/***************************************************************
 * Name:      EDID_class.h
 * Purpose:   EDID classes and field handlers
 * Author:    Tomasz Pawlak (tomasz.pawlak@wp.eu)
 * Created:   2014-03-18
 * Copyright: Tomasz Pawlak (C) 2014-2020
 * License:   GPLv3
 **************************************************************/

#ifndef EDID_CLASS_H
#define EDID_CLASS_H 1

#include <wx/string.h>
#include <wx/treectrl.h>
#include <wx/menu.h>
#include <wx/dynarray.h>
#include <wx/arrimpl.cpp>

#include "EDID.h"
#include "CEA_EXT.h"
#include "CEA.h"
#include "def_types.h"

#include "returncode/rcode.h"

#include "guilog.h"

typedef unsigned int uint;
typedef unsigned long ulong;

enum { //block IDs
   //EDID base:
   ID_BED      = 0x00000001,
   ID_VID      = 0x00000002,
   ID_BDD      = 0x00000003,
   ID_SPF      = 0x00000004,
   ID_CXY      = 0x00000005,
   ID_ETM      = 0x00000006,
   ID_STI      = 0x00000007,
   ID_DTD      = 0x00000008,
   ID_MRL      = 0x00000009,
   ID_MSN      = 0x0000000A,
   ID_MND      = 0x0000000B,
   ID_WPD      = 0x0000000C,
   ID_AST      = 0x0000000D,
   ID_UST      = 0x0000000E,
   ID_UNK      = 0x0000000F,
   //CEA-DBC:
   ID_CHD      = 0x00000100,
   ID_ADB      = 0x00000200,
   ID_VDB      = 0x00000300,
   ID_SAD      = 0x00000400,
   ID_SVD      = 0x00000500,
   ID_VSD      = 0x00000600,
   ID_VDTC     = 0x00000700,
   ID_CEA_UTC  = 0x00000800, //unknown Tag Code
   //CEA-DBC-EXT:
   ID_VCDB     = 0x00010000,
   ID_VSVD     = 0x00020000,
   ID_VSAD     = 0x00030000,
   ID_VDDD     = 0x00040000,
   ID_VDDD_IPF = 0x00050000,
   ID_VDDD_CPT = 0x00060000,
   ID_VDDD_AUD = 0x00070000,
   ID_VDDD_DPR = 0x00080000,
   ID_VDDD_CXY = 0x00090000,
   ID_RSV4     = 0x000A0000,
   ID_CLDB     = 0x000B0000,
   ID_HDRS     = 0x000C0000,
   ID_HDRD     = 0x000D0000,
   ID_HDRD_MTD = 0x000E0000,
   ID_VFPD     = 0x000F0000,
   ID_Y42V     = 0x00100000,
   ID_Y42C     = 0x00110000,
   ID_RMCD     = 0x00120000,
   ID_RMCD_HDR = 0x00130000,
   ID_RMCD_SPM = 0x00140000,
   ID_RMCD_SPD = 0x00150000,
   ID_RMCD_DPC = 0x00160000,
   ID_SLDB     = 0x00170000,
   ID_SLDB_SLD = 0x00180000,
   ID_IFDB     = 0x00190000,
   ID_IFDB_IPD = 0x001A0000,
   ID_IFDB_SIF = 0x001B0000,
   ID_IFDB_VSD = 0x001C0000,
   ID_CEA_UETC = 0x001D0000 //unknown Extended Tag Code
};

enum { //EDID field flags
   //byte0: property flags
   EF_PRCNT  = 6,
   EF_PRSHFT = 0,
   EF_PRMASK = 0x3F,
   EF_RD     = 0x00000001, //read-only
   EF_NI     = 0x00000002, //field value can't be provided as int (for write)
   EF_NU     = 0x00000004, //field is not used i.e. it is marked as unused in the EDID
   EF_FGR    = 0x00000008, //force group refresh after editing a field with this flag set
   EF_GPD    = 0x00000010, //display group description for this field
   EF_VS     = 0x00000020, //use value selector / values map present
   //byte1: val type
   EF_TPCNT  = 8,
   EF_TPSHFT = 8,
   EF_TPMASK = 0xFF,
   EF_BIT    = 0x00000100, //flag field, single bit
   EF_BFLD   = 0x00000200, //bitfield
   EF_BYTE   = 0x00000400, //
   EF_INT    = 0x00000800, //uint
   EF_FLT    = 0x00001000, //float
   EF_HEX    = 0x00002000, //display as hex
   EF_STR    = 0x00004000, //text/byte string
   EF_LE     = 0x00008000, //Little Endian byte order : reverse the byte r/w order for strings
   //byte2: val unit
   EF_UNCNT  = 9,
   EF_UNSHFT = 16,
   EF_UNMASK = 0x1FF,
   EF_PIX    = 0x00010000, //pixels
   EF_MM     = 0x00020000, //
   EF_CM     = 0x00040000, //
   EF_DM     = 0x00080000, //decimeters
   EF_HZ     = 0x00100000, //
   EF_KHZ    = 0x00200000, //
   EF_MHZ    = 0x00400000, //
   EF_MLS    = 0x00800000, //milliseconds
   EF_PCT    = 0x01000000  //percent
};

enum { //handler operating modes
   OP_READ  = 0x01,
   OP_WRSTR = 0x02,
   OP_WRINT = 0x04
};

enum { //EDID blocks
   EDI_BLK_SIZE = 128,
   EDI_BASE_IDX = 0,
   EDI_EXT0_IDX,
   EDI_EXT1_IDX,
   EDI_EXT2_IDX
};

typedef u8_t ediblk_t[EDI_BLK_SIZE];

typedef struct __attribute__ ((packed)) { //edi_s
   edid_t   base;
   ediblk_t ext0;
   ediblk_t ext1;
   ediblk_t ext2;
} edi_t;

typedef union __attribute__ ((packed)) { //edi_buff_u
   u8_t     buff[4* sizeof(edi_t)];
   ediblk_t blk[4];
   edi_t    edi;
} edi_buf_t;

//value map : single entry
typedef struct { // vname_map_t
   const u32_t val;
   const char *name;
} vname_map_t;

typedef struct { // vmap_t
   const u32_t        resvd; //reserved 0
   const u32_t        nval;
   const vname_map_t *vmap;
} vmap_t;

class  EDID_cl;
struct edi_dynfld_s;
//field - handler fn ptr
typedef rcode (EDID_cl::*field_fn)(u32_t, wxString&, u32_t&, edi_dynfld_s*);

typedef struct __attribute__ ((packed)) { //edi_field_s
         field_fn  handlerfn;
   const vmap_t   *vmap;
         u32_t     offs;    //offset in data struct - generic/multi-instance functions
         u32_t     fldoffs; //offset in bits
         u32_t     fldsize; //len in bytes/bits -> depends on flags
         u32_t     flags;
         u32_t     minv;
         u32_t     maxv;
   const char     *name;
   const char     *desc;
} edi_field_t;

typedef struct __attribute__ ((packed)) edi_dynfld_s { //edi_dynfld_s
   edi_field_t  field;
   u8_t        *base;     //data pointer (instance) - generic handlers
   wxMenu      *selector;
} edi_dynfld_t;


WX_DECLARE_OBJARRAY(edi_dynfld_t*, GpFieldA_t);

class edi_grp_cl;
WX_DECLARE_OBJARRAY(edi_grp_cl*, GroupsA_t);

class edi_grp_cl : public wxTreeItemData {
   protected:
      static const size_t edi_fld_sz = sizeof(edi_field_t);

      u8_t   *instance;
      u32_t   type_id;
      u32_t   abs_offs;
      u32_t   rel_offs;

      rcode   init_fields(const edi_field_t* field_arr, u8_t* inst, u32_t fcount, u32_t orflags,
                          const char *pname = NULL, const char *pdesc = NULL);
      rcode   create_selector(edi_dynfld_t *pfld);
      void    clear_fields();

   public:
      GpFieldA_t      FieldsA;

      wxString        GroupName;
      wxString        GroupDesc;

      inline u32_t    getTypeID () {return type_id;};
      inline u32_t    getAbsOffs() {return (abs_offs);};
      inline u32_t    getRelOffs() {return (rel_offs);};
      inline void     setAbsOffs(u32_t offs) {abs_offs = offs;};
      inline void     setRelOffs(u32_t offs) {rel_offs = offs;};

      virtual rcode   init(u8_t* inst, u32_t orflags) =0;
      virtual rcode   ForcedGroupRefresh() =0;
      virtual u32_t   getSubGrpCount() {return 0;};

      virtual edi_grp_cl* getSubGroup(u32_t ) {return NULL;};

      edi_grp_cl() : type_id(0), abs_offs(0), rel_offs(0)  {};

      ~edi_grp_cl() {
         clear_fields();
         FieldsA.Clear();
      };
};


class EDID_cl {
   private:
      edi_buf_t    EDID_buff;

      guilog_cl   *pGLog;

      u32_t        num_valid_blocks;
      bool         RD_ignore;
      bool         ERR_ignore;
      wxString     tmps;

   protected:
      static const wxString val_unit_name[];
      static const wxString val_type_name[];
      static const wxString prop_flag_name[];

      edi_grp_cl* ParseDetDtor(dsctor_u* pdsc, rcode& retU);

      //Common handlers: helpers
      inline u32_t    calcGroupOffs (void *inst);
      inline u8_t*    getInstancePtr(edi_dynfld_t* p_field);
             u8_t*    getValPtr     (edi_dynfld_t* p_field);

      rcode getStrUint  (wxString& sval, int base, u32_t minv, u32_t maxv, ulong& val);
      rcode getStrDouble(wxString& sval, const double minv, const double maxv, double& val);
      rcode rdByteStr   (wxString& sval, u8_t* pstrb, u32_t slen);
      rcode rdByteStrLE (wxString& sval, u8_t* pstrb, u32_t slen);
      rcode wrByteStr   (wxString& sval, u8_t* pstrb, u32_t slen);
      rcode wrByteStrLE (wxString& sval, u8_t* pstrb, u32_t slen);

   public:
      GroupsA_t* BlkGroupsA[4];
      GroupsA_t  EDI_BaseGrpA;
      GroupsA_t  EDI_Ext0GrpA;
      GroupsA_t  EDI_Ext1GrpA;
      GroupsA_t  EDI_Ext2GrpA;

      inline  edi_buf_t* getEDID() {return &EDID_buff;};
      inline  void       SetGuiLogPtr(guilog_cl *p_glog) {pGLog = p_glog;};
      inline  void       SetERRignore(bool errign) {ERR_ignore = errign;};
      inline  bool       GetERRignore() {return ERR_ignore;};
      inline  void       SetRDignore(bool rd) {RD_ignore = rd;};
      inline  bool       GetRDignore() {return RD_ignore;};
      inline  uint       getNumValidBlocks() {return num_valid_blocks;};
      inline  void       ForceNumValidBlocks(uint nblk) {num_valid_blocks = (nblk%5);}; //max 4

      u32_t genChksum(u32_t block);
      bool  VerifyChksum(u32_t block);
      void  ClearBuffer();
      rcode ParseEDID_Base(u32_t& n_extblk);
      rcode ParseEDID_CEA();
      rcode ParseCEA_DBC(u8_t *pinst, ethdr_t ethdr);

      //field flags
      rcode getValUnitName (wxString& sval, const u32_t flags);
      rcode getValTypeName (wxString& sval, const u32_t flags);
      rcode getValFlagsName(wxString& sval, const u32_t flags);

      //Common handlers
      rcode FldPadString(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode ByteStr     (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode ByteVal     (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode BitF8Val    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode BitVal      (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode Gamma       (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);

      //Base Data handlers
      rcode MfcId (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode ProdSN(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode ProdWk(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode ProdYr(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //input type : no dedicated handlers
      //basic display description (old) : no dedicated handlers
      //Supported features : no dedicated handlers
      //Chromacity coords
      rcode ChrXY_getWriteVal(u32_t op, wxString& sval, u32_t& ival);
      rcode CHredX    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode CHredY    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode CHgrnX    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode CHgrnY    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode CHbluX    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode CHbluY    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode CHwhtX    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode CHwhtY    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //Resolution map : no dedicated handlers
      //std timing descriptors
      rcode StdTxres8 (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode StdTvsync (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //DTD: Detailed Timing Descriptor
      rcode DTD_PixClk(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_HApix (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_HBpix (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_VAlin (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_VBlin (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_HOsync(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_VOsync(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_VsyncW(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_HsyncW(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_Hsize (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode DTD_Vsize (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //MRL handlers
      rcode MRL_Hdr      (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode MRL_GTFM     (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode MRL_MaxPixClk(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //WPD handlers
      rcode WPD_pad      (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //CEA:ADB:SAD
      rcode SAD_LPCM_MC  (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //CEA:VSD
      rcode VSD_ltncy    (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode VSD_MaxTMDS  (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //CEA-Ext: VDDD
      rcode VDDD_IF_MaxF   (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode VDDD_HVpix_cnt (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode VDDD_AspRatio  (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode VDDD_HVpx_pitch(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      rcode VDDD_AudioDelay(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);
      //CEA-Ext: HDRD
      rcode HDRD_mtd_type  (u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field);


      EDID_cl() : num_valid_blocks(0), RD_ignore(false), ERR_ignore(false) {
         BlkGroupsA[0] = &EDI_BaseGrpA;
         BlkGroupsA[1] = &EDI_Ext0GrpA;
         BlkGroupsA[2] = &EDI_Ext1GrpA;
         BlkGroupsA[3] = &EDI_Ext2GrpA;
      };

      ~EDID_cl() {
         EDI_BaseGrpA.Clear();
         EDI_Ext0GrpA.Clear();
         EDI_Ext1GrpA.Clear();
         EDI_Ext2GrpA.Clear();
      };
};

u32_t EDID_cl::calcGroupOffs(void *inst) {
   return (reinterpret_cast <u8_t*> (inst) - EDID_buff.buff);
};

u8_t* EDID_cl::getInstancePtr(edi_dynfld_t* p_field) {
   if (p_field == NULL) return NULL; //reinterpret_cast <u8_t*> (field);
   return p_field->base;
};


//BED: Base EDID data
class edibase_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      //edibase_cl() {};
};
//VID: Video Input Descriptor
class vindsc_cl : public edi_grp_cl {
   private:
      //num of fields per block
      static const u32_t in_analog_fcnt;
      static const u32_t in_digital_fcnt;

      static const u32_t max_fcnt; //for pre-allocating array of fields

      //block data layouts
      static const edi_field_t in_analog[];
      static const edi_field_t in_digital[];

      rcode  gen_data_layout(u8_t* inst);

   protected:
      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh();

       vindsc_cl() {};
      ~vindsc_cl() { free(fields); };
};
//BDD: basic display descriptior
class bddcs_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//SFT: Supported features
class sft_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//Chromacity coords
class chromxy_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//ETM: Estabilished Timings Map
class resmap_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//STI: Std Timing Information Descriptor
class sttd_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//DTD fields indexes in array
enum {
   DTD_IDX_PIXCLK = 0,
   DTD_IDX_HAPIX,
   DTD_IDX_HBPIX,
   DTD_IDX_VALIN,
   DTD_IDX_VBLIN,
   DTD_IDX_HSOFFS,
   DTD_IDX_HSWIDTH,
   DTD_IDX_VSOFFS,
   DTD_IDX_VSWIDTH,
   DTD_IDX_HSIZE,
   DTD_IDX_VSIZE,
   DTD_IDX_HBORD,
   DTD_IDX_VBORD,
   DTD_IDX_IL2W,
   DTD_IDX_HSTYPE,
   DTD_IDX_VSTYPE,
   DTD_IDX_SYNCTYP,
   DTD_IDX_STEREOMD,
   DTD_IDX_ILACE
};
//DTD : detailed timing descriptor
class dtd_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//MRL : Monitor Range Limits Descriptor
class mrl_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char *Desc;
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//WPD : White Point Descriptor
class wpt_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char *Desc;
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//MND: Monitor Name Descriptor
class mnd_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char *Desc;
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//MSN: Monitor Serial Number Descriptor
class msn_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char *Desc;
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//UTX: UnSpecified Text
class utx_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char *Desc;
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//AST: Additional Standard Timing identifiers
class ast_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char *Desc;
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};
//UNK: Unknown Descriptor (type != 0xFA-0xFF)
class unk_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char *Desc;
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };
};


//----------------- CEA/CTA-861 extension

//CHD: CEA Header
class cea_hdr_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_hdr_cl() {};
};
//ADB: Audio Data Block
class cea_adb_cl : public edi_grp_cl {
   private:
      GroupsA_t    subgroups; //ADB SADs

   protected:

      static const char  Name[];
      static const char  Desc[];

   public:
      edi_grp_cl*  getSubGroup(u32_t idx) {return subgroups.Item(idx);};
      u32_t        getSubGrpCount() {return subgroups.GetCount();};
      rcode        init(u8_t* inst, u32_t orflags);
      rcode        ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_adb_cl() {};
};
//ADB: Audio Data Block ->
//SAD: Short Audio Descriptor
class cea_sad_cl : public edi_grp_cl {
   private:
      //num of fields per block
      static const u32_t byte0_afc1_14_fcnt;
      static const u32_t byte0_afc15_ace11_12_fcnt;
      static const u32_t byte0_afc15_ace13_fcnt;

      static const u32_t byte1_afc1_14_ace11_fcnt;
      static const u32_t byte1_afc15_ace456810_fcnt;
      static const u32_t byte1_afc15_ace12_fsmp_fcnt;
      static const u32_t byte1_afc15_ace13_fcnt;

      static const u32_t byte2_afc1_fcnt;
      static const u32_t byte2_afc2_8_fcnt;
      static const u32_t byte2_afc9_13_fcnt;
      static const u32_t byte2_afc14_fcnt;
      static const u32_t byte2_afc15_ace456_fcnt;
      static const u32_t byte2_afc15_ace8_10_fcnt;
      static const u32_t byte2_afc15_ace11_12_fcnt;
      static const u32_t byte2_afc15_ace13_fcnt;

      static const u32_t byte_unk_fcnt;

      static const u32_t max_fcnt; //for pre-allocating array of fields

      //block data layouts
      static const edi_field_t byte0_afc1_14[];
      static const edi_field_t byte0_afc15_ace11_12[];
      static const edi_field_t byte0_afc15_ace13[];

      static const edi_field_t byte1_afc1_14_ace11[];
      static const edi_field_t byte1_afc15_ace456810[];
      static const edi_field_t byte1_afc15_ace12_fsmp[];
      static const edi_field_t byte1_afc15_ace13[];

      static const edi_field_t byte2_afc1[];
      static const edi_field_t byte2_afc2_8[];
      static const edi_field_t byte2_afc9_13[];
      static const edi_field_t byte2_afc14[];
      static const edi_field_t byte2_afc15_ace456[];
      static const edi_field_t byte2_afc15_ace8_10[];
      static const edi_field_t byte2_afc15_ace11_12[];
      static const edi_field_t byte2_afc15_ace13[];

      //SAD unknown/invalid byte:
      static const edi_field_t byte_unk[];

      //SAD needs custom constructor for field array
      rcode  gen_data_layout(u8_t* inst);

   protected:
      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh();

       cea_sad_cl() {};
      ~cea_sad_cl() { free(fields); };
};
//VDB: Video Data Block
class cea_vdb_cl : public edi_grp_cl {
   private:
      GroupsA_t    subgroups; //VDB SVDs

   protected:

      static const char  Name[];
      static const char  Desc[];

   public:
      //GroupsA_t*   getSubGroupA() {return &subgroups;};
      edi_grp_cl*  getSubGroup(u32_t idx) {return subgroups.Item(idx);};
      u32_t        getSubGrpCount() {return subgroups.GetCount();};
      rcode        init(u8_t* inst, u32_t orflags);
      rcode        ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_vdb_cl() {};
};
//VDB: Video Data Block ->
//SVD: Short Video Descriptor
class cea_svd_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_svd_cl() {};
};
//VSD: Vendor Specific Data Block
class cea_vsd_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_vsd_cl() {};
};
//SAB: Speaker Allocation Data Block
class cea_sab_cl : public edi_grp_cl {
   private:
             const edi_field_t *fields;

   protected:
      static const char  Name[];
                   wxString Desc;
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_sab_cl() {};
};
//VTC: VESA Display Transfer Characteristic Data Block (gamma)
class cea_vdtc_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:

      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_vdtc_cl() {};
};
//UNK-TC: Unknown Data Block (Tag Code)
class cea_unktc_cl : public edi_grp_cl {
   private:

   protected:

      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;


   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

       cea_unktc_cl() {};
      ~cea_unktc_cl() { free(fields); };
};

//-------------------------------------------------------------------------------------------------
//DBC Extended Tag Codes

//VCDB: Video Capability Data Block (DBC_EXT_VCDB = 0)
class cea_vcdb_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:

      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_vcdb_cl() {};
};
//VSVD: Vendor-Specific Video Data Block (DBC_EXT_VSVD = 1)
class cea_vsvd_cl : public edi_grp_cl {
   private:
      //base block layout
      static const u32_t       blkhdr_fcnt;
      static const edi_field_t blkhdr[];

      rcode gen_data_layout(u8_t* inst);

   protected:

      static const char  Name[];
      static const char  Desc[];

       //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

       cea_vsvd_cl() {};
      ~cea_vsvd_cl() { free(fields); };
};
//VDDD: VESA Display Device Data Block (DBC_EXT_VDDD = 2)
class cea_vddd_cl : public edi_grp_cl {
   private:
      GroupsA_t    subgroups;

   protected:

      static const char  Name[];
      static const char  Desc[];

   public:
      edi_grp_cl*  getSubGroup(u32_t idx) {return subgroups.Item(idx);};
      u32_t        getSubGrpCount() {return subgroups.GetCount();};
      rcode        init(u8_t* inst, u32_t orflags);
      rcode        ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_vddd_cl() {};
};
//VDDD: IFP: Interface
class vddd_iface_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      vddd_iface_cl() {};
};
//VDDD: CPT: Content Protection
class vddd_cprot_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      vddd_cprot_cl() {};
};
//VDDD: AUD: Audio properties
class vddd_audio_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      vddd_audio_cl() {};
};
//VDDD: DPR: Display properties
class vddd_disp_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      vddd_disp_cl() {};
};
//VDDD: CXY: Additional Chromaticity coords
class vddd_cxy_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      vddd_cxy_cl() {};
};

//VESA Video Timing Block Extension (DBC_EXT_VVTB = 3) BUG!

//CLDB: Colorimetry Data Block (DBC_EXT_CLDB = 5)
class cea_cldb_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_cldb_cl() {};
};
//HDRS: HDR Static Metadata Data Block (DBC_EXT_HDRS = 6)
class cea_hdrs_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_hdrs_cl() {};
};
//HDRD: HDR Dynamic Metadata Data Block (DBC_EXT_HDRD = 7)
class cea_hdrd_cl : public edi_grp_cl {
   private:
      GroupsA_t    subgroups;

   protected:

      static const char  Name[];
      static const char  Desc[];

   public:
      edi_grp_cl*  getSubGroup(u32_t idx) {return subgroups.Item(idx);};
      u32_t        getSubGrpCount() {return subgroups.GetCount();};
      rcode        init(u8_t* inst, u32_t orflags);
      rcode        ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_hdrd_cl() {};
};
//HDRD: HDR Dynamic Metadata sub-group
class hdrd_mtd_cl : public edi_grp_cl {
   private:
      static const u32_t       mtd_hdr_fcnt3;
      static const u32_t       mtd_hdr_fcnt124;
      static const edi_field_t mtd_hdr[];

      static const u32_t max_fcnt; //for pre-allocating array of fields

      //metadata needs custom constructor for field array
      rcode  gen_data_layout(u8_t* inst);

   protected:
      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh();

       hdrd_mtd_cl() {};
      ~hdrd_mtd_cl() { free(fields); };
};
//VFPD: Video Format Preference Data Block (DBC_EXT_VFPD = 13)
class cea_vfpd_cl : public edi_grp_cl {
   private:

      static const edi_field_t SVR_code_fld;

      //variable field array
      rcode  gen_data_layout(u8_t* inst);

   protected:
      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh();

       cea_vfpd_cl() {};
      ~cea_vfpd_cl() { free(fields); };
};
//Y42V: YCBCR 4:2:0 Video Data Block (DBC_EXT_Y42V = 14)
class cea_y42v_cl : public edi_grp_cl {
   private:
      GroupsA_t    subgroups; //SVDs

   protected:

      static const char  Name[];
      static const char  Desc[];

   public:
      //GroupsA_t*   getSubGroupA() {return &subgroups;};
      edi_grp_cl*  getSubGroup(u32_t idx) {return subgroups.Item(idx);};
      u32_t        getSubGrpCount() {return subgroups.GetCount();};
      rcode        init(u8_t* inst, u32_t orflags);
      rcode        ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_y42v_cl() {};
};
//Y42C: YCBCR 4:2:0 Capability Map Data Block (DBC_EXT_Y42C = 15)
class cea_y42c_cl : public edi_grp_cl {
   private:

      static const edi_field_t bitmap_fld;

      //variable field array
      rcode  gen_data_layout(u8_t* inst);

   protected:
      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      y42c_svdn_t *svdn_ar; //array of strings with SVD nr
      edi_field_t *fields;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh();

       cea_y42c_cl() {};
      ~cea_y42c_cl() { free(fields); free(svdn_ar); };
};
//VSAD: Vendor-Specific Audio Data Block (DBC_EXT_VSAD = 17)
class cea_vsad_cl : public edi_grp_cl {
   private:
      //base block layout
      static const u32_t       blkhdr_fcnt;
      static const edi_field_t blkhdr[];

      rcode gen_data_layout(u8_t* inst);

   protected:

      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

       cea_vsad_cl() {};
      ~cea_vsad_cl() { free(fields); };
};
//RMCD: Room Configuration Data Block (DBC_EXT_RMCD = 19)
class cea_rmcd_cl : public edi_grp_cl {
   private:
      GroupsA_t    subgroups;

   protected:

      static const char  Name[];
      static const char  Desc[];

   public:
      edi_grp_cl*  getSubGroup(u32_t idx) {return subgroups.Item(idx);};
      u32_t        getSubGrpCount() {return subgroups.GetCount();};
      rcode        init(u8_t* inst, u32_t orflags);
      rcode        ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_rmcd_cl() {};
};
//RMCD: DHDR: Data Header
class rmcd_hdr_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      rmcd_hdr_cl() {};
};
//RMCD: SPM: Speaker Mask
class rmcd_spm_cl : public edi_grp_cl {
   private:

   protected:
      static const char  Name[];
             const char *Desc;
      static const u32_t fcount;

      //field descriptors are shared with SAB
      edi_field_t *fields;


   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      rmcd_spm_cl() {};
};
//RMCD: SPKD: Speaker Distance
class rmcd_spkd_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      rmcd_spkd_cl() {};
};
//RMCD: DSPC: Display Coordinates
class rmcd_dspc_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      rmcd_dspc_cl() {};
};
//SLDB: Speaker Location Data Block (DBC_EXT_SLDB = 20)
class cea_sldb_cl : public edi_grp_cl {
   private:
      GroupsA_t    subgroups;

   protected:
      static const char  Name[];
      static const char  Desc[];

   public:
      edi_grp_cl*  getSubGroup(u32_t idx) {return subgroups.Item(idx);};
      u32_t        getSubGrpCount() {return subgroups.GetCount();};
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_sldb_cl() {};
};
//SLDB: SLOCD: Speaker Location Descriptor
class slocd_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      slocd_cl() {};
};
//IFDB: InfoFrame Data Block (DBC_EXT_IFDB = 32)
class cea_ifdb_cl : public edi_grp_cl {
   private:
      GroupsA_t    subgroups;

   protected:

      static const char  Name[];
      static const char  Desc[];

   public:
      edi_grp_cl*  getSubGroup(u32_t idx) {return subgroups.Item(idx);};
      u32_t        getSubGrpCount() {return subgroups.GetCount();};
      rcode        init(u8_t* inst, u32_t orflags);
      rcode        ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      cea_ifdb_cl() {};
};
//IFDB: IFPD: InfoFrame Processing Descriptor
class ifdb_ifpdh_cl : public edi_grp_cl {
   private:
      static const edi_field_t fields[];

   protected:
      static const char  Name[];
      static const char  Desc[];
      static const u32_t fcount;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

      ifdb_ifpdh_cl() {};
};
//IFDB: SIFD: Short InfoFrame Descriptor, InfoFrame Type Code != 0x00, 0x01
class ifdb_sifd_cl : public edi_grp_cl {
   private:
      static const edi_field_t blkhdr[];
      static const u32_t       blkhdr_fcnt;

   protected:
      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;


   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

       ifdb_sifd_cl() {};
      ~ifdb_sifd_cl() { free(fields); };
};
//IFDB: VSIFD: Short Vendor-Specific InfoFrame Descriptor, InfoFrame Type Code = 0x01
class ifdb_vsifd_cl : public edi_grp_cl {
   private:
      static const edi_field_t blkhdr[];
      static const u32_t       blkhdr_fcnt;

   protected:
      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;

   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

       ifdb_vsifd_cl() {};
      ~ifdb_vsifd_cl() { free(fields); };
};

//UNK-ET: Unknown Data Block (Extended Tag Code)
class cea_unket_cl : public edi_grp_cl {
   private:

   protected:

      static const char  Name[];
      static const char  Desc[];

      //dynamic, depends on data layout
      u32_t        fcount;
      edi_field_t *fields;


   public:
      rcode  init(u8_t* inst, u32_t orflags);
      rcode  ForcedGroupRefresh() {rcode retU; RCD_RETURN_OK(retU); };

       cea_unket_cl() {};
      ~cea_unket_cl() { free(fields); };
};

#endif /* EDID_CLASS_H */
