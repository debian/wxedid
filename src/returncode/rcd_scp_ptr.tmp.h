/* template file: rcd_scp_ptr.tmp.h

   This file is part of the rcode project.

   Copyright (C) 2019 Tomasz Pawlak,
   e-mail: tomasz.pawlak@wp.eu

   The rcd_scp_ptr.tmp.h is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   The rcd_scp_ptr.tmp.h is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
   License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with the rcode project; see the file COPYING. If not,
   see <http://www.gnu.org/licenses/>.
*/

#ifndef RCD_THIS_SCP_PTR_H
#define RCD_THIS_SCP_PTR_H 1

#ifndef _RCD_AUTOGEN // not active when rcd_autogen is running
   #ifdef _USE_RCD_AUTOGEN

      #include "%rcd_bname%_rcd_scope.h"

      #undef RCD_THIS_SCOPE_PTR
      #define RCD_THIS_SCOPE_PTR %rcd_bname%_RCD_GET_SCOPE_PTR()

   #endif
#endif

#else
   //Note: the template file "rcd_this_scp.tmp.h" is used to generate ($base_name)_scope_ptr.h
   #error %rcd_bname%_scope_ptr.h: multiple inclusion in the same translation unit.
#endif /* RCD_THIS_SCP_PTR_H */
