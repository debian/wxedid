/* template file: rcd_fn_bm.tmp.c

   This file is part of the rcode project.

   Copyright (C) 2019 Tomasz Pawlak,
   e-mail: tomasz.pawlak@wp.eu

   The rcd_fn_bm.tmp.c is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   The rcd_fn_bm.tmp.c is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
   License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with the rcode project; see the file COPYING. If not,
   see <www.gnu.org/licenses/>.
*/

#include "%path%rcode.h"

#include <sys/cdefs.h>
__BEGIN_DECLS

// rcd_autogen: basic/dummy mode

#pragma GCC diagnostic ignored "-Wunused-parameter"

int
%rcd_bname%_RCD_GET_MSG(const rcd_scope_t *const scp, rcode rcd, char *buf, int bsz) {
   return RCD_PRINT_BUF(rcd, buf, bsz);
}

int
%rcd_bname%_RCD_PRINT_MSG(const rcd_scope_t *const scp, rcode retU, FILE *strm) {
   RCD_PRINT(retU, strm); return 0;
}

void
__%rcd_bname%_RCD_SET_VMSG(rcode rcd, int32_t nargs, const char *fmt, ... ) {
   return;
}

#pragma GCC diagnostic warning "-Wunused-parameter"

__END_DECLS
