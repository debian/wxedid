/* template file: rcd_scp.tmp.c

   This file is part of the rcode project.

   Copyright (C) 2019 Tomasz Pawlak,
   e-mail: tomasz.pawlak@wp.eu

   The rcd_scp.tmp.c is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   The rcd_scp.tmp.c is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
   License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with the rcode project; see the file COPYING. If not,
   see <www.gnu.org/licenses/>.
*/

#include "%path%rcode.h"

#include <sys/cdefs.h>
__BEGIN_DECLS

// rcd_autogen: full/basic mode

// "volatile"/variable message:
//  valid only right after the rcode is returned
static __thread rcd_vmsg_t vmsg;

const rcd_scope_t*
%rcd_bname%_RCD_GET_SCOPE_PTR() {
	%rcd_bname%_units.vmsg = &vmsg; //TLS pointer
	return (const rcd_scope_t*) &%rcd_bname%_units;
}

size_t
%rcd_bname%_RCD_GET_MIN_BUFF_SZ() {
	return (size_t) %rcd_bname%_units.min_bufsz + RCD_VMSG_MAX_SZ;
}

__END_DECLS
