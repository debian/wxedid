/* template file: rcd_fn.tmp.h

   This file is part of the rcode project.

   Copyright (C) 2019 Tomasz Pawlak,
   e-mail: tomasz.pawlak@wp.eu

   The rcd_fn.tmp.h is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   The rcd_fn.tmp.h is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
   License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with the rcode project; see the file COPYING. If not,
   see <http://www.gnu.org/licenses/>.
*/


#ifndef %rcd_bname%_RCD_FUNC_H
#define %rcd_bname%_RCD_FUNC_H 1

#include "%path%rcode.h"

#include <sys/cdefs.h>

__BEGIN_DECLS

// Assembles null-terminated rcode description in the *buff,
// returns length of the string, incl. null byte, -1 for error.

__nonnull((3))
extern int
%rcd_bname%_RCD_GET_MSG(const rcd_scope_t* const, rcode, char*, int);

// Writes rcode description + newline to strm,
// returns -1 for error, zero otherwise.

__nonnull((3))
extern int
%rcd_bname%_RCD_PRINT_MSG(const rcd_scope_t* const, rcode, FILE*);

// internal fn, use:
// ($base_name)_RCD_RETURN_FAULT_VMSG() or ($base_name)_RCD_SET_FAULT_VMSG()
extern
void
__%rcd_bname%_RCD_SET_VMSG(rcode, const char *fmt, ... );


#define %rcd_bname%_RCD_RETURN_FAULT_VMSG( _rcd, _fmt, ... ) \
   { \
      _rcd = RCD_SET_VAL( RCD_UNIT, RCD_FVMSG, __LINE__ ); \
      __%rcd_bname%_RCD_SET_VMSG(_rcd, _fmt, ##__VA_ARGS__); \
      return (_rcd); \
   }

//same as ($base_name)_RCD_RETURN_FAULT_VMSG() - but it doesn't return immediately.
//NOTE: changing the rcode before returning invalidates the message.
#define %rcd_bname%_RCD_SET_FAULT_VMSG( _rcd, _fmt, ... ) \
   { \
      _rcd = RCD_SET_VAL( RCD_UNIT, RCD_FVMSG, __LINE__ ); \
      __%rcd_bname%_RCD_SET_VMSG(_rcd, _fmt, ##__VA_ARGS__); \
   }


__END_DECLS

#endif /* %rcd_bname%_RCD_FUNC_H */
