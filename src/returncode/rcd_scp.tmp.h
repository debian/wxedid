/* template file: rcd_scp.tmp.h

   This file is part of the rcode project.

   Copyright (C) 2019 Tomasz Pawlak,
   e-mail: tomasz.pawlak@wp.eu

   The rcd_scp.tmp.h is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   The rcd_scp.tmp.h is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
   License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with the rcode project; see the file COPYING. If not,
   see <http://www.gnu.org/licenses/>.
*/

#ifndef %rcd_bname%_RCD_SCP_FUNC_H
#define %rcd_bname%_RCD_SCP_FUNC_H 1

#include <sys/cdefs.h>
__BEGIN_DECLS

extern
const rcd_scope_t*
%rcd_bname%_RCD_GET_SCOPE_PTR()
__attribute__((pure));

extern
size_t
%rcd_bname%_RCD_GET_MIN_BUFF_SZ()
__attribute__((pure));

__END_DECLS

#endif /* %rcd_bname%_RCD_SCP_FUNC_H */
