/* template file: rcd_fn.tmp.c

   This file is part of the rcode project.

   Copyright (C) 2019 Tomasz Pawlak,
   e-mail: tomasz.pawlak@wp.eu

   The rcd_fn.tmp.c is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   The rcd_fn.tmp.c is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
   License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with the rcode project; see the file COPYING. If not,
   see <www.gnu.org/licenses/>.
*/

#include "%path%rcode.h"

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "%rcd_bname%_rcd_scope.h"

#include <sys/cdefs.h>
__BEGIN_DECLS

// rcd_autogen: full mode

#define offsetof(type, member) __builtin_offsetof (type, member)

#define __RCD_ALIAS(__fn, __alias) \
   extern __typeof(__fn) __alias __attribute__((alias (#__fn), visibility ("hidden") ))

static char* __rcd_cnv_s(char* buf, char* const pend, uintptr_t val, int sign, uint32_t base) {
// internal fn: args are checked by the caller

// min target buffer sizes (in bytes) :
// 64bit val, base16: 16 (unsigned) + NULL = 17 (+1 signed)
// 64bit val, base10: 20 (signed)   + NULL = 21
// 64bit val, base2 : 64 (unsigned) + NULL = 65 (+1 signed)
// 32bit val, base16: 8  (unsigned) + NULL = 9  (+1 signed)
// 32bit val, base10: 11 (signed)   + NULL = 12
// 32bit val, base2 : 32 (unsigned) + NULL = 33 (+1 signed)

//   mode:
//   sign:  1= signed, 0= unsigned

   static const char cset[] = "0123456789ABCDEF";
   char   cbuf[64];
   char  *pcbuf = cbuf + sizeof(cbuf);

   {
      int   neg;
      neg = (0 != sign);
      neg = neg && (0 > (intptr_t) val);
      if (neg) { val *= -1; *buf++ = '-';}
   }

   do {
      intptr_t utmp = val;
      val /= base;
      *--pcbuf = cset[utmp - (val * base)];
   } while (val);

   /* copy buffer */
   while (pcbuf < (cbuf + sizeof(cbuf)) ) {
      *buf++ = *pcbuf++;
      if (buf >= pend) break;
   }
   return buf;
}

static const void* __rcd_bin_search(const char *arr, int32_t arsz, uint32_t itemsz, uint32_t voffs, uint32_t val) {
   const char *pitem;
   uint32_t    mval;
   uint32_t    jmp_sz;
   uint32_t    s_cnt;
    int32_t    idx;

   jmp_sz   = arsz;
   jmp_sz >>= 1;
   idx      = jmp_sz;
   s_cnt    = 0;
   arr     += voffs; //jumping directly between target fields

loop:
   pitem = arr + (idx * itemsz);
   mval  = *((uint16_t*) pitem);
   if (val == mval) { return (pitem - voffs); }
   if (jmp_sz >= 2) {
      jmp_sz >>=1;
   } else { //2 steps left
      if (s_cnt >= 2) goto not_found;
      s_cnt ++ ;
      idx = (val > mval) ? (idx + 1) : (idx - 1);
      if ((idx < 0) || (idx > arsz)) goto not_found;
      goto loop;
   }
   idx = (val > mval) ? (idx + jmp_sz) : (idx - jmp_sz);
   goto loop;

not_found:
   return NULL;
}

static char* __rcd_assemble_msg(char* buf, int32_t bsz, rcode retU,
                                const rcd_scope_t *const rscp,
                                const rcd_unit_t *p_un,
                                const char* msg, uint32_t mlen) {
   //min. buff size checked by the caller
   //format: "%s[%u]: %s%s.%u [%d] %s": +7 to min_buf size-> autogen::F_APPEND_SORT_UNITS()
   char     *pend;
   uint32_t  len;

   pend  = buf;
   pend += bsz;
   len = rscp->bname_slen;
   memcpy(buf, rscp->base_name, (size_t) len); //base name
   buf += len;
   *buf = '['; buf ++ ;
   buf  = __rcd_cnv_s(buf, pend, RCD_GET_UNIT(retU), 0, 10); //unit number
   *buf = ']'; buf ++ ;
   *buf = ':'; buf ++ ;
   *buf = ' '; buf ++ ;
   len  = p_un->dir_slen;
   memcpy(buf, p_un->un_dir, (size_t) len); //unit dir
   buf += len;
   len  = p_un->file_slen;
   memcpy(buf, p_un->un_file, (size_t) len); //file name
   buf += len;
   *buf = '.'; buf ++ ;
   buf  = __rcd_cnv_s(buf, pend, RCD_GET_DATA(retU), 0, 10); //ln num
   *buf = ' '; buf ++ ;
   *buf = '['; buf ++ ;
   { //rcode
      int rcd;
      rcd = RCD_GET_RCODE(retU); // -2 .. +1
      if (rcd < 0) {
         *buf = '-'; buf ++ ;
         rcd *= -1;
      }
      rcd += '0';
      *buf = rcd; buf ++ ;
   }
   *buf = ']'; buf ++ ;
   *buf = ' '; buf ++ ;
   if (NULL == msg) goto end;
   len  = (pend - buf);
   if (mlen > len) mlen = len;
   memcpy(buf, msg, (size_t) mlen); //message
   buf += mlen;

end:
   *buf = 0;
   return buf;
}

int %rcd_bname%_RCD_GET_MSG(const rcd_scope_t *const rscp, rcode retU, char* buf, int bsz) {
   const rcd_unit_t *p_un;
   const char       *msg;
   int32_t           itmp;

   if (NULL == rscp) goto fallback;
   if (0 == RCD_GET_UNIT(retU)) goto fallback; //rcode.detail.unit == 0 -> special case, reserved value.
   if (bsz < rscp->min_bufsz) goto fallback; //at least rscp->min_bufsz, +RCD_VMSG_MAX_SZ for formatted msg.

   itmp = rscp->un_cnt;
   p_un = (rcd_unit_t*) &rscp->unit_ar[0];
   //search units
   p_un = (rcd_unit_t*) __rcd_bin_search((char*) p_un, itmp,
                                     sizeof(rcd_unit_t),
                                     offsetof(rcd_unit_t, un_id),
                                     RCD_GET_UNIT(retU) );
   if ( NULL == p_un ) goto fallback; //unit not found
   //get message
   msg = NULL;
   if ( RCD_GET_RCODE(retU) != RCD_FVMSG ) {
      rcd_msg_t *pmsg;
      //search messages
      itmp = p_un->msg_cnt;
      pmsg = (rcd_msg_t*) &p_un->msg_ar[0];
      if ( NULL == pmsg ) goto no_msg; //no msg in the unit

      pmsg = (rcd_msg_t*) __rcd_bin_search((char*) pmsg, itmp,
                                         sizeof(rcd_msg_t),
                                         offsetof(rcd_msg_t, lnum),
                                         RCD_GET_DATA(retU) );
      if ( NULL == pmsg ) goto no_msg; //no msg for a given line, itmp=msg_len has no meaning in such case.
      msg  = pmsg->msg;
      itmp = pmsg->msg_len;

   } else { //RCD_FVMSG
      rcd_vmsg_t *vmsg;
      //NOTE: vmsg is initialized by calling  $(base_name)_RCD_GET_SCOPE_PTR()
      vmsg = rscp->vmsg;
      //check if vmsg matches *this* rcode
      if (retU.value == vmsg->retU.value) {
         msg  = vmsg->msg_buf;
         itmp = vmsg->msg_len;
      }
   }

no_msg:
   {
      char *pend;

      pend = __rcd_assemble_msg( buf, bsz, retU, rscp, p_un, msg, itmp);

      itmp = (pend - buf);
      return itmp;
   }
fallback:
   return RCD_PRINT_BUF(retU, buf, bsz);
}

__RCD_ALIAS(%rcd_bname%_RCD_GET_MSG, __alias_get_msg);

int %rcd_bname%_RCD_PRINT_MSG(const rcd_scope_t *const rscp, rcode retU, FILE* strm) {
   if (NULL == rscp) {
      RCD_PRINT(retU, strm);
      return 0;
   }
   {
      ssize_t strsz, wrsz;
      int     buf_sz = rscp->min_bufsz + RCD_VMSG_MAX_SZ;
      char    buf[buf_sz];

      strsz = __alias_get_msg(rscp, retU, buf, buf_sz);
      if (strsz <= 0) return strsz;
      buf[strsz++] = '\n';

      wrsz = fwrite(buf, 1, strsz, strm);
      if (wrsz != strsz) return -1;
      return wrsz;
   }
}

void __%rcd_bname%_RCD_SET_VMSG(rcode rcd, const char *fmt, ... ) {
   rcd_vmsg_t *vmsg;
   int         len;
   va_list     argp;
   //*this* unit
   vmsg = %rcd_bname%_RCD_GET_SCOPE_PTR()->vmsg;

   va_start(argp, fmt);

   len = vsnprintf(vmsg->msg_buf, RCD_VMSG_MAX_SZ, fmt, argp );

   va_end(argp);

   vmsg->msg_len  = len;
   vmsg->retU     = rcd;
}

__END_DECLS
