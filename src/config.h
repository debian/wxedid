/* src/config.h.  Generated from config.h.in by configure.  */
/* src/config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "wxedid"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "tomasz.pawlak@wp.eu"

/* Define to the full name of this package. */
#define PACKAGE_NAME "wxEDID"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "wxEDID 0.0.21"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "wxedid"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.0.21"

/* Version number of package */
#define VERSION "0.0.21"
