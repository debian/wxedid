/***************************************************************
 * Name:      wxEDID_Main.cpp
 * Purpose:   Code for Application Frame
 * Author:    Tomasz Pawlak (tomasz.pawlak@wp.eu)
 * Created:   2014-03-18
 * Copyright: Tomasz Pawlak (C) 2014-2020
 * License:   GPLv3
 **************************************************************/

#include "debug.h"
#include "rcdunits.h"
#ifndef idMAIN
   #error "wxEDID_Main.cpp: missing unit ID"
#endif
#define RCD_UNIT idMAIN
#include "returncode/rcode.h"

#include "wxedid_rcd_func.h"
#include "wxedid_rcd_scope_ptr.h"

RCD_AUTOGEN_DEFINE_UNIT

#include <wx/event.h>

#include "wxEDID_Main.h"
#include <wx/msgdlg.h>

#include <wx/filedlg.h>
#include <wx/file.h>
#include <string.h>

//(*InternalHeaders(wxEDID_Frame)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format) {
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//config vars
config_t config;

//common messages / strings
static const wxString ver = _(VERSION);

static const char wxEDID_About[] =
"wxEDID v" VERSION "\n\n"
"Extended Display Identification Data (EDID)\n"
"structure editor and parser.\n"
"Supported structures:\n"
"EDID v1.3+\n"
"CEA/CTA-861-G\n\n"
"Author: Tomasz Pawlak\n"
"e-mail: tomasz.pawlak@wp.eu\n"
"License: GPLv3\n\n";

static const char Hlp_Flg_Type[] =
"Types:\n"
"Bit: single bit value\n"
"BitFld: bit field\n"
"Byte: byte\n"
"Int: integer\n"
"Float: real/floating point value\n"
"Hex: value expressed in hexadecimal format\n"
"String: string of bytes\n"
"LE: little endian\n\n"
"Flags:\n"
"RD: read-only\n"
"NU: field not used\n"
"FR: forced refresh of block data\n"
"VS: value selector menu available\n"
"GD: group descriptor: no dedicated description\n"
"NI: internal checks: value is not an integer\n";

static const wxString wxLF   = _("\n");
static const wxString wxTAB  = _("\t");
static const wxString wxIDNT = _("  ");
static const wxString wxSP   = _(" ");

static const wxString txt_WARNING = _("WARNING");
static const wxString txt_REMARK = _("Remark");
static const wxString msgF_LOAD = _(
"Failed to load EDID.\n"
"You may use Options->\"Ignore EDID Errors\" and then \"Reparse EDID buffer\" to view the data anyway."
);

static const char AuiEDID_DefLayout[] =
"layout2|"
"name=TreeDataCtl;caption=Block Tree;state=5892;dir=4;layer=0;row=0;pos=0;"
"prop=100000;bestw=150;besth=80;minw=150;minh=-1;maxw=-1;maxh=-1;"
"floatx=-1;floaty=-1;floatw=-1;floath=-1|"

"name=GridDataCtl;caption=Block Data;state=5896;dir=5;layer=0;row=0;pos=0;"
"prop=137878;bestw=82;besth=16;minw=-1;minh=-1;maxw=-1;maxh=-1;"
"floatx=-1;floaty=-1;floatw=-1;floath=-1|"

"name=InfoCtl;caption=Info;state=5892;dir=5;layer=0;row=0;pos=1;"
"prop=62122;bestw=80;besth=100;minw=-1;minh=100;maxw=-1;maxh=-1;"
"floatx=-1;floaty=-1;floatw=-1;floath=-1|"

"dock_size(4,0,0)=215|dock_size(5,0,0)=10|";


//(*IdInit(wxEDID_Frame)
const long wxEDID_Frame::id_tree_edid = wxNewId();
const long wxEDID_Frame::id_edi_fgrid = wxNewId();
const long wxEDID_Frame::id_txc_edid_info = wxNewId();
const long wxEDID_Frame::ID_PANEL1 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT5 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT6 = wxNewId();
const long wxEDID_Frame::id_sct_pixclk = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT1 = wxNewId();
const long wxEDID_Frame::id_txc_vrefresh = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT2 = wxNewId();
const long wxEDID_Frame::id_dtd_screen = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT12 = wxNewId();
const long wxEDID_Frame::id_sct_xres = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT11 = wxNewId();
const long wxEDID_Frame::id_txres = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT33 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT14 = wxNewId();
const long wxEDID_Frame::id_sct_hborder = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT13 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT7 = wxNewId();
const long wxEDID_Frame::id_sct_hblank = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT3 = wxNewId();
const long wxEDID_Frame::id_txc_thblank = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT29 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT8 = wxNewId();
const long wxEDID_Frame::id_sct_hsoffs = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT4 = wxNewId();
const long wxEDID_Frame::id_txc_thsoffs = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT30 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT9 = wxNewId();
const long wxEDID_Frame::id_sct_hswidth = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT10 = wxNewId();
const long wxEDID_Frame::id_thswidth = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT31 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT27 = wxNewId();
const long wxEDID_Frame::id_txc_htotal = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT32 = wxNewId();
const long wxEDID_Frame::id_txc_thtotal = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT28 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT25 = wxNewId();
const long wxEDID_Frame::id_txc_hfreq = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT26 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT15 = wxNewId();
const long wxEDID_Frame::id_sct_vres = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT16 = wxNewId();
const long wxEDID_Frame::id_txc_tvres = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT34 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT17 = wxNewId();
const long wxEDID_Frame::id_sct_vborder = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT18 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT19 = wxNewId();
const long wxEDID_Frame::id_sct_vblank = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT20 = wxNewId();
const long wxEDID_Frame::id_txc_tvblank = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT35 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT21 = wxNewId();
const long wxEDID_Frame::id_sct_vsoffs = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT22 = wxNewId();
const long wxEDID_Frame::id_txc_tvsoffs = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT36 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT23 = wxNewId();
const long wxEDID_Frame::id_sct_vswidth = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT24 = wxNewId();
const long wxEDID_Frame::id_txc_vswidth = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT37 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT38 = wxNewId();
const long wxEDID_Frame::id_txc_vtotal = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT39 = wxNewId();
const long wxEDID_Frame::id_txc_tvtotal = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT40 = wxNewId();
const long wxEDID_Frame::ID_STATICTEXT41 = wxNewId();
const long wxEDID_Frame::id_txc_modeline = wxNewId();
const long wxEDID_Frame::id_panel_dtd = wxNewId();
const long wxEDID_Frame::id_ntbook = wxNewId();
const long wxEDID_Frame::id_mnu_imphex = wxNewId();
const long wxEDID_Frame::id_mnu_exphex = wxNewId();
const long wxEDID_Frame::id_mnu_parse = wxNewId();
const long wxEDID_Frame::id_mnu_chksum = wxNewId();
const long wxEDID_Frame::id_mnu_ignerr = wxNewId();
const long wxEDID_Frame::id_mnu_allwr = wxNewId();
const long wxEDID_Frame::id_mnu_dtd_asp = wxNewId();
const long wxEDID_Frame::id_mnu_logw = wxNewId();
const long wxEDID_Frame::id_mnu_flags = wxNewId();
const long wxEDID_Frame::id_win_stat_bar = wxNewId();
//*)

wxDECLARE_EVENT(wxEVT_DEFERRED, wxCommandEvent);
wxDEFINE_EVENT (wxEVT_DEFERRED, wxCommandEvent);

wxBEGIN_EVENT_TABLE(wxEDID_Frame, wxFrame)
    //(*EventTable(wxEDID_Frame)
    //*)
    EVT_SIZE                   (                wxEDID_Frame::evt_frame_size        )
    EVT_GRID_SELECT_CELL       (                wxEDID_Frame::evt_gridcell_select   )
  //EVT_GRID_CELL_CHANGED      (                wxEDID_Frame::evt_gridcell_write    ) //dynamic
    EVT_GRID_CMD_EDITOR_SHOWN  (id_edi_fgrid  , wxEDID_Frame::evt_gridcell_vsel     )
    EVT_GRID_CMD_EDITOR_HIDDEN (id_edi_fgrid  , wxEDID_Frame::evt_gridcell_edit_hide)
    EVT_TREE_SEL_CHANGED       (id_tree_edid  , wxEDID_Frame::evt_blktree_sel       )

    EVT_AUINOTEBOOK_PAGE_CHANGING(id_ntbook   , wxEDID_Frame::evt_ntbook_page       )
    EVT_COMMAND                (wxID_ANY      , wxEVT_DEFERRED, wxEDID_Frame::evt_Deferred)

    EVT_MENU                   (wxID_OPEN     , wxEDID_Frame::evt_open_edid_bin     )
    EVT_MENU                   (id_mnu_imphex , wxEDID_Frame::evt_import_hex        )
    EVT_MENU                   (wxID_SAVE     , wxEDID_Frame::evt_save_edid_bin     )
    EVT_MENU                   (wxID_SAVEAS   , wxEDID_Frame::evt_save_report       )
    EVT_MENU                   (id_mnu_exphex , wxEDID_Frame::evt_export_hex        )
    EVT_MENU                   (wxID_EXIT     , wxEDID_Frame::evt_Quit              )
    EVT_MENU                   (wxID_ABOUT    , wxEDID_Frame::evt_About             )
    EVT_MENU                   (id_mnu_flags  , wxEDID_Frame::evt_Flags             )
    EVT_MENU                   (id_mnu_logw   , wxEDID_Frame::evt_log_win           )
    EVT_MENU                   (id_mnu_allwr  , wxEDID_Frame::evt_ignore_rd         )
    EVT_MENU                   (id_mnu_ignerr , wxEDID_Frame::evt_ignore_err        )
    EVT_MENU                   (id_mnu_parse  , wxEDID_Frame::evt_reparse           )
    EVT_MENU                   (id_mnu_chksum , wxEDID_Frame::evt_recalc_chksum     )
    EVT_MENU                   (id_mnu_dtd_asp, wxEDID_Frame::evt_dtd_asp           )
wxEND_EVENT_TABLE()

#pragma GCC diagnostic ignored "-Wunused-parameter"

// Workaround for gcc v8.x :
// Warnings of type mismatch for event handlers cast using "wxObjectEventFunction"
#if wxCHECK_GCC_VERSION(8, 0)
   #pragma GCC diagnostic ignored "-Wcast-function-type"
#endif

wxEDID_Frame::wxEDID_Frame(wxWindow* parent,wxWindowID id) :
   grid_typeRGB_int   (wxColour(0xFF, 0xFF, 0xFF)),
   grid_typeRGB_bit   (wxColour(0xFF, 0xFF, 0xCC)),
   grid_typeRGB_float (wxColour(0xFF, 0xDD, 0xFF)),
   grid_typeRGB_hex   (wxColour(0xDD, 0xDD, 0xFF)),
   b_dtd_keep_aspect  (config.b_dtd_keep_aspect  )
{
   SetEvtHandlerEnabled(false);

    //(* Initialize(wxEDID_Frame)
    wxBoxSizer* BoxSizer5;
    wxMenu* Menu1;
    wxMenu* Menu2;
    wxMenuBar* MenuBar1;
    wxMenuItem* mnu_about;
    wxMenuItem* mnu_quit;

    Create(parent, wxID_ANY, _("wxEDID"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
    SetMinSize(wxSize(640,440));
    AuiMgrMain = new wxAuiManager(this, 0);
    ntbook = new wxAuiNotebook(this, id_ntbook, wxDefaultPosition, wxDefaultSize, 0);
    edid_panel = new wxPanel(ntbook, ID_PANEL1, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL1"));
    AuiMgrEDID = new wxAuiManager(edid_panel, wxAUI_MGR_RECTANGLE_HINT);
    tree_edid = new wxTreeCtrl(edid_panel, id_tree_edid, wxDefaultPosition, wxDefaultSize, wxTR_HAS_BUTTONS|wxTR_SINGLE|wxTAB_TRAVERSAL, wxDefaultValidator, _T("id_tree_edid"));
    tree_edid->SetMinSize(wxSize(150,-1));
    AuiMgrEDID->AddPane(tree_edid, wxAuiPaneInfo().Name(_T("TreeDataCtl")).DefaultPane().Caption(_("Block Tree")).CloseButton(false).Left().TopDockable(false).BottomDockable(false).RightDockable(false).Floatable(false).MinSize(wxSize(150,-1)).Movable(false).DestroyOnClose());
    edi_fgrid = new fgrid_cl(edid_panel, id_edi_fgrid, wxDefaultPosition, wxDefaultSize, 0, _T("id_edi_fgrid"));
    edi_fgrid->CreateGrid(8,5);
    edi_fgrid->Disable();
    edi_fgrid->EnableEditing(true);
    edi_fgrid->EnableGridLines(true);
    edi_fgrid->SetColLabelSize(22);
    edi_fgrid->SetRowLabelSize(28);
    edi_fgrid->SetDefaultRowSize(22, true);
    edi_fgrid->SetDefaultColSize(64, true);
    edi_fgrid->SetColLabelValue(0, _("Name"));
    edi_fgrid->SetColLabelValue(1, _("Type"));
    edi_fgrid->SetColLabelValue(2, _("Value"));
    edi_fgrid->SetColLabelValue(3, _("Unit"));
    edi_fgrid->SetColLabelValue(4, _("Flags"));
    edi_fgrid->SetDefaultCellFont( edi_fgrid->GetFont() );
    edi_fgrid->SetDefaultCellTextColour( edi_fgrid->GetForegroundColour() );
    AuiMgrEDID->AddPane(edi_fgrid, wxAuiPaneInfo().Name(_T("GridDataCtl")).DefaultPane().Caption(_("Block Data")).CloseButton(false).Center().TopDockable(false).BottomDockable(false).LeftDockable(false).Floatable(false).Movable(false).DestroyOnClose());
    txc_edid_info = new wxTextCtrl(edid_panel, id_txc_edid_info, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY|wxTE_WORDWRAP|wxVSCROLL, wxDefaultValidator, _T("id_txc_edid_info"));
    txc_edid_info->SetMinSize(wxSize(-1,100));
    txc_edid_info->SetForegroundColour(wxColour(0,0,0));
    txc_edid_info->SetBackgroundColour(wxColour(255,255,255));
    AuiMgrEDID->AddPane(txc_edid_info, wxAuiPaneInfo().Name(_T("InfoCtl")).DefaultPane().Caption(_("Info")).CloseButton(false).Position(1).Center().TopDockable(false).BottomDockable(false).RightDockable(false).Floatable(false).MinSize(wxSize(-1,100)).Movable(false).DestroyOnClose());
    AuiMgrEDID->Update();
    dtd_panel = new wxPanel(ntbook, id_panel_dtd, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("id_panel_dtd"));
    bs_dtd_main = new wxBoxSizer(wxVERTICAL);
    fgs_dtd = new wxFlexGridSizer(3, 2, 0, 0);
    fgs_dtd_top = new wxFlexGridSizer(4, 4, 0, 0);
    StaticText5 = new wxStaticText(dtd_panel, ID_STATICTEXT5, _("Pixel Clock"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT5"));
    fgs_dtd_top->Add(StaticText5, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd_top->Add(-1,-1,1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 8);
    StaticText6 = new wxStaticText(dtd_panel, ID_STATICTEXT6, _("V-Refresh"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT6"));
    fgs_dtd_top->Add(StaticText6, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd_top->Add(-1,-1,1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    sct_pixclk = new dtd_sct_cl(dtd_panel, id_sct_pixclk, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 1, 655350, 0, _T("id_sct_pixclk"));
    sct_pixclk->SetValue(_T("0"));
    sct_pixclk->SetMinSize(wxSize(60,-1));
    fgs_dtd_top->Add(sct_pixclk, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText1 = new wxStaticText(dtd_panel, ID_STATICTEXT1, _("x10kHz"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
    fgs_dtd_top->Add(StaticText1, 2, wxRIGHT|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 8);
    txc_vrefresh = new wxTextCtrl(dtd_panel, id_txc_vrefresh, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_vrefresh"));
    txc_vrefresh->SetMinSize(wxSize(60,-1));
    txc_vrefresh->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_top->Add(txc_vrefresh, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText2 = new wxStaticText(dtd_panel, ID_STATICTEXT2, _("Hz"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
    fgs_dtd_top->Add(StaticText2, 2, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd->Add(fgs_dtd_top, 1, wxTOP|wxBOTTOM|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 6);
    fgs_dtd->Add(-1,-1,1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    dtd_screen = new dtd_screen_cl(dtd_panel, id_dtd_screen, wxDefaultPosition, wxDefaultSize, wxCLIP_CHILDREN|wxFULL_REPAINT_ON_RESIZE, _T("id_dtd_screen"));
    fgs_dtd->Add(dtd_screen, 1, wxEXPAND, 4);
    fgs_dtd_right = new wxFlexGridSizer(7, 5, 1, 0);
    StaticText12 = new wxStaticText(dtd_panel, ID_STATICTEXT12, _("X-res"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT12"));
    fgs_dtd_right->Add(StaticText12, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_xres = new dtd_sct_cl(dtd_panel, id_sct_xres, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 1, 4095, 0, _T("id_sct_xres"));
    sct_xres->SetValue(_T("0"));
    sct_xres->SetMinSize(wxSize(60,-1));
    fgs_dtd_right->Add(sct_xres, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText11 = new wxStaticText(dtd_panel, ID_STATICTEXT11, _("pix"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT11"));
    fgs_dtd_right->Add(StaticText11, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_txres = new wxTextCtrl(dtd_panel, id_txres, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txres"));
    txc_txres->SetMinSize(wxSize(60,-1));
    txc_txres->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_right->Add(txc_txres, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText33 = new wxStaticText(dtd_panel, ID_STATICTEXT33, _("us"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT33"));
    fgs_dtd_right->Add(StaticText33, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText14 = new wxStaticText(dtd_panel, ID_STATICTEXT14, _("H-Border"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT14"));
    fgs_dtd_right->Add(StaticText14, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_hborder = new dtd_sct_cl(dtd_panel, id_sct_hborder, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 0, 255, 0, _T("id_sct_hborder"));
    sct_hborder->SetValue(_T("0"));
    sct_hborder->SetMinSize(wxSize(60,-1));
    fgs_dtd_right->Add(sct_hborder, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText13 = new wxStaticText(dtd_panel, ID_STATICTEXT13, _("pix"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT13"));
    fgs_dtd_right->Add(StaticText13, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd_right->Add(-1,-1,1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd_right->Add(-1,-1,1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText7 = new wxStaticText(dtd_panel, ID_STATICTEXT7, _("H-Blank"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT7"));
    fgs_dtd_right->Add(StaticText7, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_hblank = new dtd_sct_cl(dtd_panel, id_sct_hblank, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 1, 4095, 0, _T("id_sct_hblank"));
    sct_hblank->SetValue(_T("0"));
    sct_hblank->SetMinSize(wxSize(60,-1));
    fgs_dtd_right->Add(sct_hblank, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText3 = new wxStaticText(dtd_panel, ID_STATICTEXT3, _("pix"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
    fgs_dtd_right->Add(StaticText3, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_thblank = new wxTextCtrl(dtd_panel, id_txc_thblank, _("0"), wxDefaultPosition, wxSize(50,22), wxTE_READONLY, wxDefaultValidator, _T("id_txc_thblank"));
    txc_thblank->SetMinSize(wxSize(60,-1));
    txc_thblank->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_right->Add(txc_thblank, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText29 = new wxStaticText(dtd_panel, ID_STATICTEXT29, _("us"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT29"));
    fgs_dtd_right->Add(StaticText29, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText8 = new wxStaticText(dtd_panel, ID_STATICTEXT8, _("H-Sync offs."), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT8"));
    fgs_dtd_right->Add(StaticText8, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_hsoffs = new dtd_sct_cl(dtd_panel, id_sct_hsoffs, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 0, 1023, 0, _T("id_sct_hsoffs"));
    sct_hsoffs->SetValue(_T("0"));
    sct_hsoffs->SetMinSize(wxSize(60,-1));
    fgs_dtd_right->Add(sct_hsoffs, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText4 = new wxStaticText(dtd_panel, ID_STATICTEXT4, _("pix"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT4"));
    fgs_dtd_right->Add(StaticText4, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_thsoffs = new wxTextCtrl(dtd_panel, id_txc_thsoffs, _("0"), wxDefaultPosition, wxSize(50,22), wxTE_READONLY, wxDefaultValidator, _T("id_txc_thsoffs"));
    txc_thsoffs->SetMinSize(wxSize(60,-1));
    txc_thsoffs->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_right->Add(txc_thsoffs, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText30 = new wxStaticText(dtd_panel, ID_STATICTEXT30, _("us"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT30"));
    fgs_dtd_right->Add(StaticText30, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText9 = new wxStaticText(dtd_panel, ID_STATICTEXT9, _("H-Sync width"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT9"));
    fgs_dtd_right->Add(StaticText9, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_hswidth = new dtd_sct_cl(dtd_panel, id_sct_hswidth, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 1, 1023, 0, _T("id_sct_hswidth"));
    sct_hswidth->SetValue(_T("0"));
    sct_hswidth->SetMinSize(wxSize(60,-1));
    fgs_dtd_right->Add(sct_hswidth, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText10 = new wxStaticText(dtd_panel, ID_STATICTEXT10, _("pix"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT10"));
    fgs_dtd_right->Add(StaticText10, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_thswidth = new wxTextCtrl(dtd_panel, id_thswidth, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_thswidth"));
    txc_thswidth->SetMinSize(wxSize(60,-1));
    txc_thswidth->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_right->Add(txc_thswidth, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText31 = new wxStaticText(dtd_panel, ID_STATICTEXT31, _("us"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT31"));
    fgs_dtd_right->Add(StaticText31, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText27 = new wxStaticText(dtd_panel, ID_STATICTEXT27, _("H total"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT27"));
    fgs_dtd_right->Add(StaticText27, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_htotal = new wxTextCtrl(dtd_panel, id_txc_htotal, _("0"), wxDefaultPosition, wxSize(60,22), wxTE_READONLY, wxDefaultValidator, _T("id_txc_htotal"));
    txc_htotal->SetMinSize(wxSize(60,-1));
    txc_htotal->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_right->Add(txc_htotal, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText32 = new wxStaticText(dtd_panel, ID_STATICTEXT32, _("pix"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT32"));
    fgs_dtd_right->Add(StaticText32, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_thtotal = new wxTextCtrl(dtd_panel, id_txc_thtotal, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_thtotal"));
    txc_thtotal->SetMinSize(wxSize(60,-1));
    txc_thtotal->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_right->Add(txc_thtotal, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText28 = new wxStaticText(dtd_panel, ID_STATICTEXT28, _("us"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT28"));
    fgs_dtd_right->Add(StaticText28, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText25 = new wxStaticText(dtd_panel, ID_STATICTEXT25, _("H-Freq"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT25"));
    fgs_dtd_right->Add(StaticText25, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_hfreq = new wxTextCtrl(dtd_panel, id_txc_hfreq, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_hfreq"));
    txc_hfreq->SetMinSize(wxSize(60,-1));
    txc_hfreq->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_right->Add(txc_hfreq, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText26 = new wxStaticText(dtd_panel, ID_STATICTEXT26, _("kHz"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT26"));
    fgs_dtd_right->Add(StaticText26, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd_right->Add(-1,-1,1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd_right->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd->Add(fgs_dtd_right, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    fgs_dtd_bottom = new wxFlexGridSizer(6, 5, 1, 0);
    StaticText15 = new wxStaticText(dtd_panel, ID_STATICTEXT15, _("V-res"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT15"));
    fgs_dtd_bottom->Add(StaticText15, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_vres = new dtd_sct_cl(dtd_panel, id_sct_vres, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 1, 4095, 0, _T("id_sct_vres"));
    sct_vres->SetValue(_T("0"));
    sct_vres->SetMinSize(wxSize(60,-1));
    fgs_dtd_bottom->Add(sct_vres, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText16 = new wxStaticText(dtd_panel, ID_STATICTEXT16, _("pix"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT16"));
    fgs_dtd_bottom->Add(StaticText16, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_tvres = new wxTextCtrl(dtd_panel, id_txc_tvres, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_tvres"));
    txc_tvres->SetMinSize(wxSize(60,-1));
    txc_tvres->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_bottom->Add(txc_tvres, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText34 = new wxStaticText(dtd_panel, ID_STATICTEXT34, _("ms"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT34"));
    fgs_dtd_bottom->Add(StaticText34, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText17 = new wxStaticText(dtd_panel, ID_STATICTEXT17, _("V-Border"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT17"));
    fgs_dtd_bottom->Add(StaticText17, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_vborder = new dtd_sct_cl(dtd_panel, id_sct_vborder, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 0, 255, 0, _T("id_sct_vborder"));
    sct_vborder->SetValue(_T("0"));
    sct_vborder->SetMinSize(wxSize(60,-1));
    fgs_dtd_bottom->Add(sct_vborder, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText18 = new wxStaticText(dtd_panel, ID_STATICTEXT18, _("lines"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT18"));
    fgs_dtd_bottom->Add(StaticText18, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd_bottom->Add(-1,-1,1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd_bottom->Add(-1,-1,1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText19 = new wxStaticText(dtd_panel, ID_STATICTEXT19, _("V-Blank"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT19"));
    fgs_dtd_bottom->Add(StaticText19, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_vblank = new dtd_sct_cl(dtd_panel, id_sct_vblank, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 1, 4095, 0, _T("id_sct_vblank"));
    sct_vblank->SetValue(_T("0"));
    sct_vblank->SetMinSize(wxSize(60,-1));
    fgs_dtd_bottom->Add(sct_vblank, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText20 = new wxStaticText(dtd_panel, ID_STATICTEXT20, _("lines"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT20"));
    fgs_dtd_bottom->Add(StaticText20, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_tvblank = new wxTextCtrl(dtd_panel, id_txc_tvblank, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_tvblank"));
    txc_tvblank->SetMinSize(wxSize(60,-1));
    txc_tvblank->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_bottom->Add(txc_tvblank, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText35 = new wxStaticText(dtd_panel, ID_STATICTEXT35, _("ms"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT35"));
    fgs_dtd_bottom->Add(StaticText35, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText21 = new wxStaticText(dtd_panel, ID_STATICTEXT21, _("V-Sync offs."), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT21"));
    fgs_dtd_bottom->Add(StaticText21, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_vsoffs = new dtd_sct_cl(dtd_panel, id_sct_vsoffs, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 0, 1023, 0, _T("id_sct_vsoffs"));
    sct_vsoffs->SetValue(_T("0"));
    sct_vsoffs->SetMinSize(wxSize(60,-1));
    fgs_dtd_bottom->Add(sct_vsoffs, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText22 = new wxStaticText(dtd_panel, ID_STATICTEXT22, _("lines"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT22"));
    fgs_dtd_bottom->Add(StaticText22, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_tvsoffs = new wxTextCtrl(dtd_panel, id_txc_tvsoffs, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_tvsoffs"));
    txc_tvsoffs->SetMinSize(wxSize(60,-1));
    txc_tvsoffs->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_bottom->Add(txc_tvsoffs, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText36 = new wxStaticText(dtd_panel, ID_STATICTEXT36, _("ms"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT36"));
    fgs_dtd_bottom->Add(StaticText36, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText23 = new wxStaticText(dtd_panel, ID_STATICTEXT23, _("V-Sync width"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT23"));
    fgs_dtd_bottom->Add(StaticText23, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    sct_vswidth = new dtd_sct_cl(dtd_panel, id_sct_vswidth, _T("0"), wxDefaultPosition, wxDefaultSize, 0, 1, 1023, 0, _T("id_sct_vswidth"));
    sct_vswidth->SetValue(_T("0"));
    sct_vswidth->SetMinSize(wxSize(60,-1));
    fgs_dtd_bottom->Add(sct_vswidth, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText24 = new wxStaticText(dtd_panel, ID_STATICTEXT24, _("lines"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT24"));
    fgs_dtd_bottom->Add(StaticText24, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_tvswidth = new wxTextCtrl(dtd_panel, id_txc_vswidth, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_vswidth"));
    txc_tvswidth->SetMinSize(wxSize(60,-1));
    txc_tvswidth->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_bottom->Add(txc_tvswidth, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText37 = new wxStaticText(dtd_panel, ID_STATICTEXT37, _("ms"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT37"));
    fgs_dtd_bottom->Add(StaticText37, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText38 = new wxStaticText(dtd_panel, ID_STATICTEXT38, _("V total"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT38"));
    fgs_dtd_bottom->Add(StaticText38, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_vtotal = new wxTextCtrl(dtd_panel, id_txc_vtotal, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_vtotal"));
    txc_vtotal->SetMinSize(wxSize(60,-1));
    txc_vtotal->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_bottom->Add(txc_vtotal, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText39 = new wxStaticText(dtd_panel, ID_STATICTEXT39, _("lines"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT39"));
    fgs_dtd_bottom->Add(StaticText39, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    txc_tvtotal = new wxTextCtrl(dtd_panel, id_txc_tvtotal, _("0"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_tvtotal"));
    txc_tvtotal->SetMinSize(wxSize(60,-1));
    txc_tvtotal->SetBackgroundColour(wxColour(224,224,224));
    fgs_dtd_bottom->Add(txc_tvtotal, 1, wxLEFT|wxRIGHT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText40 = new wxStaticText(dtd_panel, ID_STATICTEXT40, _("ms"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT40"));
    fgs_dtd_bottom->Add(StaticText40, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    fgs_dtd->Add(fgs_dtd_bottom, 1, wxTOP|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    bs_dtd_main->Add(fgs_dtd, 1, wxLEFT|wxEXPAND, 16);
    BoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
    StaticText41 = new wxStaticText(dtd_panel, ID_STATICTEXT41, _("X11 ModeLine"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT41"));
    BoxSizer5->Add(StaticText41, 4, wxLEFT|wxRIGHT|wxALIGN_CENTER_VERTICAL, 5);
    txc_modeline = new wxTextCtrl(dtd_panel, id_txc_modeline, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY, wxDefaultValidator, _T("id_txc_modeline"));
    txc_modeline->SetBackgroundColour(wxColour(224,224,224));
    BoxSizer5->Add(txc_modeline, 19, wxLEFT|wxRIGHT|wxEXPAND, 4);
    bs_dtd_main->Add(BoxSizer5, 0, wxTOP|wxBOTTOM|wxEXPAND, 4);
    dtd_panel->SetSizer(bs_dtd_main);
    bs_dtd_main->Fit(dtd_panel);
    bs_dtd_main->SetSizeHints(dtd_panel);
    ntbook->AddPage(edid_panel, _("EDID"));
    ntbook->AddPage(dtd_panel, _("DTD Constructor"));
    AuiMgrMain->AddPane(ntbook, wxAuiPaneInfo().Name(_T("AUInbook")).DefaultPane().Caption(_("Nbook")).CaptionVisible(false).CloseButton(false).Center().Floatable(false).Movable(false).PaneBorder(false));
    AuiMgrMain->Update();
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    mnu_open_edi = new wxMenuItem(Menu1, wxID_OPEN, _("Open EDID binary\tctrl-O"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(mnu_open_edi);
    mnu_save_edi = new wxMenuItem(Menu1, wxID_SAVE, _("Save EDID binary\tctrl-S"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(mnu_save_edi);
    mnu_save_edi->Enable(false);
    mnu_save_text = new wxMenuItem(Menu1, wxID_SAVEAS, _("Save as text"), _("Saves EDID as human readable text file"), wxITEM_NORMAL);
    Menu1->Append(mnu_save_text);
    mnu_save_text->Enable(false);
    mnu_imphex = new wxMenuItem(Menu1, id_mnu_imphex, _("Import EDID from HEX (ASCII)"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(mnu_imphex);
    mnu_exphex = new wxMenuItem(Menu1, id_mnu_exphex, _("Export EDID as HEX (ASCII)"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(mnu_exphex);
    mnu_exphex->Enable(false);
    mnu_quit = new wxMenuItem(Menu1, wxID_EXIT, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(mnu_quit);
    MenuBar1->Append(Menu1, _("&File"));
    Menu3 = new wxMenu();
    mnu_reparse = new wxMenuItem(Menu3, id_mnu_parse, _("Reparse EDID buffer"), _("Reinterpret EDID data"), wxITEM_NORMAL);
    Menu3->Append(mnu_reparse);
    mnu_chksum = new wxMenuItem(Menu3, id_mnu_chksum, _("Recalc Checksum"), wxEmptyString, wxITEM_NORMAL);
    Menu3->Append(mnu_chksum);
    mnu_ignore_err = new wxMenuItem(Menu3, id_mnu_ignerr, _("Ignore EDID Errors"), wxEmptyString, wxITEM_CHECK);
    Menu3->Append(mnu_ignore_err);
    mnu_allwritable = new wxMenuItem(Menu3, id_mnu_allwr, _("Ignore Read-Only flags"), wxEmptyString, wxITEM_CHECK);
    Menu3->Append(mnu_allwritable);
    mnu_dtd_aspect = new wxMenuItem(Menu3, id_mnu_dtd_asp, _("DTD preview: keep aspect ratio"), wxEmptyString, wxITEM_CHECK);
    Menu3->Append(mnu_dtd_aspect);
    mnu_dtd_aspect->Check(true);
    mnu_logw = new wxMenuItem(Menu3, id_mnu_logw, _("Log Window\tctrl-L"), wxEmptyString, wxITEM_NORMAL);
    Menu3->Append(mnu_logw);
    MenuBar1->Append(Menu3, _("Options"));
    Menu2 = new wxMenu();
    mnu_about = new wxMenuItem(Menu2, wxID_ABOUT, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(mnu_about);
    MenuItem1 = new wxMenuItem(Menu2, id_mnu_flags, _("Flags & Types\tF3"), wxEmptyString, wxITEM_NORMAL);
    Menu2->Append(MenuItem1);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    win_stat_bar = new wxStatusBar(this, id_win_stat_bar, wxST_SIZEGRIP, _T("id_win_stat_bar"));
    int __wxStatusBarWidths_1[2] = { -300, 220 };
    int __wxStatusBarStyles_1[2] = { wxSB_NORMAL, wxSB_NORMAL };
    win_stat_bar->SetFieldsCount(2,__wxStatusBarWidths_1);
    win_stat_bar->SetStatusStyles(2,__wxStatusBarStyles_1);
    SetStatusBar(win_stat_bar);
    Center();
    //*)

    Connect(wxID_ANY, wxEVT_GRID_CELL_CHANGED,
            (wxObjectEventFunction) &wxEDID_Frame::evt_gridcell_write);

    row_sel = -1;
    edigrp_sel = NULL;

    GLog.Create(this);

    EDID.SetGuiLogPtr(&GLog);

    dtd_panel->Enable(false);
    dtd_screen->Enable(false);
    dtd_screen->SetParentFrame(this); //needed to have a pointer to a DTD values

    mnu_dtd_aspect->Check(b_dtd_keep_aspect);


    flags.edigridblk_ok = 0;
    flags.ctrl_enabled = 0;
    flags.edi_grp_rfsh = 0;
    edid_file_name = _("EDID");

    SetEvtHandlerEnabled(true);

    {  //calc win size based on DTD panel.
       wxSize minsz, actsz, winsz, delta;

       winsz    = GetSize();
       actsz    = dtd_panel->GetSize();
       minsz    = bs_dtd_main->ComputeFittingWindowSize(this);

       delta.x  = actsz.x - minsz.x;
       delta.x  = (delta.x > 0) ? 0 : -delta.x;
       delta.x += 8; //magic: compensation for dtd_screen border
       delta.y  = actsz.y - minsz.y;
       delta.y  = (delta.y > 0) ? 0 : -delta.y;

       winsz   += delta;

       SetMinSize(winsz);
    }

    //EDID panel, default layout
    tmps = wxString::FromAscii(AuiEDID_DefLayout);
    AuiMgrEDID->LoadPerspective(tmps);

    //load/import file from cmd line arg?
    if (config.b_cmdln_bin_file) {
       wxCommandEvent evt(wxEVT_DEFERRED, wxID_OPEN);
       AddPendingEvent(evt);
       return;
    }
    if (config.b_cmdln_txt_file) {
       wxCommandEvent evt(wxEVT_DEFERRED, id_mnu_imphex);
       AddPendingEvent(evt);
    }
}
#if wxCHECK_GCC_VERSION(8, 0)
   #pragma GCC diagnostic warning "-Wcast-function-type"
#endif
#pragma GCC diagnostic warning "-Wunused-parameter"

wxEDID_Frame::~wxEDID_Frame() {
    //(*Destroy(wxEDID_Frame)
    //*)
    AuiMgrEDID->UnInit();
    AuiMgrMain->UnInit();

    delete AuiMgrEDID;
    delete AuiMgrMain;
}

void wxEDID_Frame::evt_frame_size(wxSizeEvent& evt) {
   wxSize NewSize;

   NewSize.y  = fgs_dtd->GetSize().y;
   NewSize.y -= fgs_dtd_top->GetSize().y;
   NewSize.y -= fgs_dtd_bottom->GetSize().y;

   NewSize.x  = fgs_dtd->GetSize().x;
   NewSize.x -= fgs_dtd_right->GetSize().x;

   NewSize.DecBy(10, 16); //borders

   dtd_screen->SetMinSize(NewSize);
   bs_dtd_main->Layout();

   evt.Skip(true);
}

void wxEDID_Frame::evt_gridcell_select(wxGridEvent& evt) {
   rcode retU;
   row_sel = evt.GetRow();
   edi_fgrid->SelectRow(row_sel);
   retU = SetFieldDesc(row_sel);
   evt.Skip(true);
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
void wxEDID_Frame::evt_gridcell_vsel(wxGridEvent& evt) {
   //EVT_GRID_CMD_EDITOR_SHOWN: show value selector menu for fields with EF_VS
   rcode  retU;

   if (edigrp_sel == NULL) {
      RCD_SET_FAULT(retU);
      GLog.PrintRcode(retU);
      //evt.Skip(true);
      GLog.Show();
      return;
   }

   //row_sel = evt.GetRow(); //grp field idx
   edi_dynfld_t *p_field = edigrp_sel->FieldsA.Item(row_sel);

   if (p_field == NULL) {
      RCD_SET_FAULT(retU);
      GLog.PrintRcode(retU);
      GLog.Show();
      //evt.Skip(true);
      return;
   }

   if ((p_field->field.flags & EF_VS) != 0) {
      if(p_field->selector != NULL) edi_fgrid->PopupMenu(p_field->selector);
   }

}

void wxEDID_Frame::evt_gridcell_edit_hide(wxGridEvent& evt) {
   //keep row selected after CellEditControl is removed
   edi_fgrid->SelectRow(row_sel);
}


#if wxCHECK_GCC_VERSION(8, 0)
   #pragma GCC diagnostic ignored "-Wcast-function-type"
#endif
void wxEDID_Frame::evt_gridcell_write(wxGridEvent& evt) {
   static const wxString fmsgU = _("FAULT: UpdateEDID_grid()");
   static const wxString fmsgW = _("FAULT: WriteField()");
   rcode retU;

   GLog.DoLog(_("-> evt_gridcell_write"));

   row_op = evt.GetRow();
   //Call field handler(write), then re-read the value to verify it
   retU = WriteField();
   //refresh the cell value: (fn name is misleading, but it works)
   edi_fgrid->ShowCellEditControl();
   edi_fgrid->HideCellEditControl();

   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(fmsgW);
      GLog.PrintRcode(retU);
      GLog.Show();
   }

   //check Group Refresh flag:
   if (flags.edi_grp_rfsh != 0) {

      flags.edi_grp_rfsh = 0;
      GLog.DoLog(_("Group Refresh forced."));
      // prevent evt nesting
      Disconnect(wxID_ANY, wxEVT_GRID_CELL_CHANGED,
                 (wxObjectEventFunction) &wxEDID_Frame::evt_gridcell_write);

      retU = UpdateEDID_grid(edigrp_sel);
      if (!RCD_IS_OK(retU)) {
         GLog.DoLog(fmsgU);
         GLog.PrintRcode(retU);
         GLog.Show();
      }

      //edi_fgrid->ForceRefresh();
      Connect(wxID_ANY, wxEVT_GRID_CELL_CHANGED,
              (wxObjectEventFunction) &wxEDID_Frame::evt_gridcell_write);
   }
}

void wxEDID_Frame::evt_blktree_sel(wxTreeEvent& evt) {
   rcode retU;
   wxTreeItemId itemsel = evt.GetItem();
   edi_grp_cl *group = reinterpret_cast <edi_grp_cl*> (tree_edid->GetItemData(itemsel));

   //on block sel. change always hide cell editor and clear grid selection:
   edi_fgrid->HideCellEditControl();
   edi_fgrid->ClearSelection();

   if ( (!itemsel.IsOk()) || (group == NULL)) {
      txc_edid_info->Clear();
      edi_fgrid->Enable(false);
      tmps.Empty();
      win_stat_bar->SetStatusText(tmps, SBAR_GRPOFFS);
      //evt.Skip(true);
      return;
   }
   edi_fgrid->Enable(flags.ctrl_enabled);
   edigrp_sel = group;
   //display group info
   txc_edid_info->SetValue(group->GroupDesc);
   {
      uint offs = group->getAbsOffs();
      tmps.Printf(_("Group offset=%d (0x%04X)"), offs, offs);
      win_stat_bar->SetStatusText(tmps, SBAR_GRPOFFS);
   }

   retU = UpdateEDID_grid(group);
   if (!RCD_IS_OK(retU)) {
      GLog.PrintRcode(retU);
      GLog.Show();
   }

   {
      bool enb = (group->getTypeID() == ID_DTD);
      if (enb) {
         Connect(wxID_ANY, wxEVT_COMMAND_SPINCTRL_UPDATED,
                 (wxObjectEventFunction) &wxEDID_Frame::evt_dtdctor_sct);
      } else {
         Disconnect(wxID_ANY, wxEVT_COMMAND_SPINCTRL_UPDATED,
                    (wxObjectEventFunction) &wxEDID_Frame::evt_dtdctor_sct);
      }
      dtd_panel->Enable(enb);
      dtd_screen->Enable(enb);
   }
}
#if wxCHECK_GCC_VERSION(8, 0)
   #pragma GCC diagnostic warning "-Wcast-function-type"
#endif

void wxEDID_Frame::evt_open_edid_bin(wxCommandEvent& evt) {
   rcode retU;
   retU = OpenEDID();
   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(_("OpenEDID() FAILED."));
      GLog.PrintRcode(retU);
      GLog.Show();
      tmps = msgF_LOAD;
      wxMessageBox(tmps, txt_REMARK, wxOK|wxCENTRE|wxICON_INFORMATION);
      return;
   }
}

void wxEDID_Frame::evt_save_edid_bin(wxCommandEvent& evt) {
   rcode retU;

   retU = SaveEDID();
   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(_("SaveEDID() FAILED."));
      GLog.PrintRcode(retU);
      GLog.Show();
   }

   evt.Skip(true);
}

void wxEDID_Frame::evt_save_report(wxCommandEvent& evt) {
   rcode retU;

   retU = SaveReport();
   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(_("SaveReport() FAILED."));
      GLog.PrintRcode(retU);
      GLog.Show();
   }
}

void wxEDID_Frame::evt_export_hex(wxCommandEvent& evt) {
   rcode retU;

   retU = ExportEDID_hex();
   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(_("ExportEDID_hex() FAILED."));
      GLog.PrintRcode(retU);
      GLog.Show();
   }
}

void wxEDID_Frame::evt_import_hex(wxCommandEvent& evt) {
   rcode retU;

   retU = ImportEDID_hex();
   if (!RCD_IS_OK(retU) &! EDID.GetERRignore()) {
      GLog.DoLog(_("ImportEDID_hex() FAILED."));
      GLog.PrintRcode(retU);
      GLog.Show();
      tmps = msgF_LOAD;
      wxMessageBox(tmps, txt_REMARK, wxOK|wxCENTRE|wxICON_INFORMATION);
      return;
   }

   //EnableControl(true); see Reparse()
}

void wxEDID_Frame::evt_ignore_rd(wxCommandEvent& evt) {

   EDID.SetRDignore(mnu_allwritable->IsChecked());

   if (mnu_allwritable->IsChecked()) {
      tmps = _("Write protection for ALL fields is disabled.");
      wxMessageBox(tmps, txt_WARNING, wxOK|wxCENTRE|wxICON_EXCLAMATION);
      UpdateEDID_grid(edigrp_sel);
   } else {
      tmps = _("Write protection is enabled for fields with RD flag set.");
      wxMessageBox(tmps, txt_REMARK, wxOK|wxCENTRE|wxICON_INFORMATION);
      UpdateEDID_grid(edigrp_sel);
   }
}

void wxEDID_Frame::evt_ignore_err(wxCommandEvent& evt) {

   EDID.SetERRignore(mnu_ignore_err->IsChecked());

   if (mnu_ignore_err->IsChecked()) {
      tmps = _("EDID errors will be IGNORED.\n"
               "This allows to bypass minor bugs in loaded files, "
               "but it won't help if the file contains garbage.");
      wxMessageBox(tmps, txt_WARNING, wxOK|wxCENTRE|wxICON_EXCLAMATION);
      GLog.DoLog(_("Ignoring of EDID errors ENABLED."));
   } else {
      tmps = _("Critical EDID error will disable the editor.");
      wxMessageBox(tmps, txt_REMARK, wxOK|wxCENTRE|wxICON_INFORMATION);
      GLog.DoLog(_("Ignoring of EDID errors DISABLED."));
   }
}

void wxEDID_Frame::evt_dtdctor_sct(wxSpinEvent& evt) {
   float  pixclk;
   int    limit, val;
   bool   brecalc     = true;
   bool   updt_htotal = false;
   bool   updt_vtotal = false;
   int    evtid       = evt.GetId();

   //invalidate grid data -> refresh
   if (evtid != wxID_ANY) flags.edigridblk_ok = 0;

   if (evtid == id_sct_pixclk) {
      sct_pixclk->data = sct_pixclk->GetValue();
      if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_pixclk))) return;
      evtid = wxID_ANY;
      //DTD_Ctor_Recalc();
      //return;
   }
   pixclk  = (sct_pixclk->data * 10000);

   if ((evtid == wxID_ANY) || (evtid == id_sct_xres)) {
      sct_xres->data = sct_xres->GetValue();
      if (evtid != wxID_ANY) {
         if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_xres))) return;
      }
      tmps.Printf(_("%.03f"), ((sct_xres->data * 1000000.0) / pixclk));
      txc_txres->SetValue(tmps);
      updt_htotal = updt_vtotal = true;
      evtid = wxID_ANY;
   }
   if ((evtid == wxID_ANY) || (evtid == id_sct_hblank)) {
      limit = (sct_hsoffs->data + sct_hswidth->data);
      val   = sct_hblank->GetValue();
      if (val < limit) {
         val = limit;
         sct_hblank->SetValue(val);
      }
      sct_hblank->data = val;
      if (evtid != wxID_ANY) {
         if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_hblank))) return;
      }
      tmps.Printf(_("%.04f"), ((val * 1000000.0) / pixclk));
      txc_thblank->SetValue(tmps);
      updt_htotal = updt_vtotal = true;
   }
   if ((evtid == wxID_ANY) || (evtid == id_sct_hsoffs)) {
      limit = (sct_hblank->data - sct_hswidth->data);
      val   = sct_hsoffs->GetValue();
      if (val > limit) {
         val = limit;
         sct_hsoffs->SetValue(val);
      }
      sct_hsoffs->data = val;
      if (evtid != wxID_ANY) {
         if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_hsoffs))) return;
      }
      tmps.Printf(_("%.04f"), ((val * 1000000.0) / pixclk));
      txc_thsoffs->SetValue(tmps);
      brecalc = false;
   }
   if ((evtid == wxID_ANY) || (evtid == id_sct_hswidth)) {
      limit = (sct_hblank->data - sct_hsoffs->data);
      val   = sct_hswidth->GetValue();
      if (val > limit) {
         val = limit;
         sct_hswidth->SetValue(val);
      }
      sct_hswidth->data = val;
      if (evtid != wxID_ANY) {
         if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_hswidth))) return;
      }
      tmps.Printf(_("%.04f"), ((val * 1000000.0) / pixclk));
      txc_thswidth->SetValue(tmps);
      brecalc = false;
   }
   if (evtid == id_sct_hborder) {
      sct_hborder->data = sct_hborder->GetValue();
      if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_hborder))) return;
      brecalc = false;
   }

   dtd_Htotal = (sct_hblank->data + sct_xres->data);
   if ((evtid == wxID_ANY) || (evtid == id_sct_vres)) {
      sct_vres->data = sct_vres->GetValue();
      if (evtid != wxID_ANY) {
         if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_vres))) return;
      }
      tmps.Printf(_("%.03f"), ((sct_vres->data * dtd_Htotal * 1000.0) / pixclk));
      txc_tvres->SetValue(tmps);
      updt_vtotal = true;
   }
   if ((evtid == wxID_ANY) || (evtid == id_sct_vblank)) {
      limit = (sct_vsoffs->data + sct_vswidth->data);
      val   = sct_vblank->GetValue();
      if (val < limit) {
         val = limit;
         sct_vblank->SetValue(val);
      }
      sct_vblank->data = val;
      if (evtid != wxID_ANY) {
         if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_vblank))) return;
      }
      tmps.Printf(_("%.04f"), ((val * dtd_Htotal * 1000.0) / pixclk));
      txc_tvblank->SetValue(tmps);
      updt_vtotal = true;
   }
   if ((evtid == wxID_ANY) || (evtid == id_sct_vsoffs)) {
      limit = (sct_vblank->data - sct_vswidth->data);
      val   = sct_vsoffs->GetValue();
      if (val > limit) {
         val = limit;
         sct_vsoffs->SetValue(val);
      }
      sct_vsoffs->data = val;
      if (evtid != wxID_ANY) {
         if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_vsoffs))) return;
      }
      tmps.Printf(_("%.04f"), ((val * dtd_Htotal * 1000.0) / pixclk));
      txc_tvsoffs->SetValue(tmps);
      brecalc = false;
   }
   if ((evtid == wxID_ANY) || (evtid == id_sct_vswidth)) {
      limit = (sct_vblank->data - sct_vsoffs->data);
      val   = sct_vswidth->GetValue();
      if (val > limit) {
         val = limit;
         sct_vswidth->SetValue(val);
      }
      sct_vswidth->data = val;
      if (evtid != wxID_ANY) {
         if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_vswidth))) return;
      }
      tmps.Printf(_("%.04f"), ((val * dtd_Htotal * 1000.0) / pixclk));
      txc_tvswidth->SetValue(tmps);
      brecalc = false;
   }
   if (evtid == id_sct_vborder) {
      sct_vborder->data = sct_vborder->GetValue();
      if (!RCD_IS_OK(DTD_Ctor_WriteInt(*sct_vborder))) return;
      brecalc = false;
   }

   if (updt_htotal) {
      tmps.Printf(_("%d"), dtd_Htotal);
      txc_htotal->SetValue(tmps);
      tmps.Printf(_("%.03f"), ((dtd_Htotal * 1000000.0) / pixclk));
      txc_thtotal->SetValue(tmps);
   }

   if (updt_vtotal) {
      dtd_Vtotal = (sct_vres->data + sct_vblank->data);
      tmps.Printf(_("%d"), dtd_Vtotal);
      txc_vtotal->SetValue(tmps);
      tmps.Printf(_("%.03f"), ((dtd_Htotal * 1000.0 * dtd_Vtotal) / pixclk));
      txc_tvtotal->SetValue(tmps);
   }

   if (brecalc || (evtid == wxID_ANY)) DTD_Ctor_Recalc();

   DTD_Ctor_Modeline();

   dtd_screen->Refresh();
}

void wxEDID_Frame::evt_ntbook_page(wxAuiNotebookEvent& evt) {
   rcode retU;
   int page = evt.GetSelection();

   if (page == 0) {
      //update edi block data grid after switching from DTD Ctor panel
      if ( !flags.edigridblk_ok && (edigrp_sel != NULL)) {
         retU = UpdateEDID_grid(edigrp_sel);
         if (!RCD_IS_OK(retU)) {
            GLog.DoLog(_("UpdateEDID_grid() FAILED."));
            GLog.PrintRcode(retU);
            GLog.Show();
         }
      }
      //prevent tree item from stealing focus
      edid_panel->SetFocus();
      return;
   }
   if (!dtd_panel->IsEnabled()) return;

   retU = DTD_Ctor_read_all();

   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(_("DTD_Ctor_read_all() FAILED."));
      GLog.PrintRcode(retU);
      GLog.Show();
   }

   //push evt to refresh all the spin controls
   wxSpinEvent evtsp(wxEVT_COMMAND_SPINCTRL_UPDATED, wxID_ANY);
   ProcessEvent(evtsp);

   evt.Skip(true);
}

void wxEDID_Frame::evt_reparse(wxCommandEvent& evt) {
   rcode retU;

   retU = Reparse();
   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(_("Reparse() FAILED."));
      GLog.PrintRcode(retU);
      GLog.Show();
   }
}

void wxEDID_Frame::evt_recalc_chksum(wxCommandEvent& evt) {
   rcode retU;

   GLog.DoLog(_("recalc_chksum()"));
   retU = CalcVerifyChksum(EDI_BASE_IDX);

   if (RCD_IS_OK(retU)) {
      //do not use EDID.num_valid_blocks, as this can be changed by user
      uint idx = EDID.getEDID()->edi.base.num_extblk;
      if (idx <= EDI_EXT2_IDX) {
         for (uint itb=0; itb<idx;) {
            retU = CalcVerifyChksum(++itb);
            if (!RCD_IS_OK(retU)) break;
         }

      } else RCD_SET_FAULT(retU);
   }

   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(_("recalc_chksum() FAILED."));
      GLog.PrintRcode(retU);
      GLog.Show();
   }
   return;
}

void wxEDID_Frame::evt_log_win(wxCommandEvent& evt) {
   GLog.ShowHide();
   this->Raise();
   this->SetFocus();
}

void wxEDID_Frame::evt_dtd_asp(wxCommandEvent& evt) {

   b_dtd_keep_aspect        = mnu_dtd_aspect->IsChecked();
   config.b_dtd_keep_aspect = b_dtd_keep_aspect;

   mnu_dtd_aspect->Check(b_dtd_keep_aspect);

   if (dtd_panel->IsShown() && dtd_panel->IsEnabled()) {
      //don't post refresh evt if the dtd panel is not active
      dtd_panel->Refresh();
   }
}

void wxEDID_Frame::evt_Quit(wxCommandEvent& evt) {

   Close();
}

void wxEDID_Frame::evt_About(wxCommandEvent& evt) {
   const wxString wxLF = _("\n");
   tmps = wxString::FromAscii(wxEDID_About);
   tmps << wxbuildinfo(long_f) << wxLF;
   tmps << wxLF << _("build date: ") << wxString::FromAscii(__DATE__);
   tmps << wxLF << _("build time: ") << wxString::FromAscii(__TIME__);
   wxMessageBox(tmps, _("wxEDID::About"));
}

void wxEDID_Frame::evt_Flags(wxCommandEvent& evt) {
   tmps = wxString::FromAscii(Hlp_Flg_Type);
   wxMessageBox(tmps, _("Help::Flags & Types"));
}
#pragma GCC diagnostic warning "-Wunused-parameter"

void wxEDID_Frame::evt_Deferred(wxCommandEvent& evt) {

   if (evt.GetId() == wxID_OPEN) {
      evt_open_edid_bin(evt);
   }

   if (evt.GetId() == id_mnu_imphex) {
      evt_import_hex(evt);
   }
}


rcode wxEDID_Frame::OpenEDID() {
   rcode  retU;
   wxFile file;

   GLog.DoLog(_("OpenEDID()"));

   if (! config.b_cmdln_bin_file) { //menu event: open EDID binary

      wxFileDialog dlg_open(this, _("Open EDID binary file"), _(""), _(""),
                            _("Binary (*.bin)|*.bin|All (*.*)|*"), wxFD_DEFAULT_STYLE);
      //set last used path
      if (config.b_have_last_fpath) {
         dlg_open.SetPath(config.last_used_fpath);
      }

      if (dlg_open.ShowModal() != wxID_OK) {
         GLog.DoLog(_("OpenEDID() canceled by user."));
         RCD_RETURN_OK(retU);
      }
      edid_file_name = dlg_open.GetFilename();
      tmps           = dlg_open.GetPath();

   } else {
      //open EDID binary file provided as cmd line arg
      edid_file_name = config.last_used_fname;
      tmps           = config.cmd_open_file_path;
      config.b_cmdln_bin_file = false; //single shot
   }

   if (! file.Open(tmps, wxFile::read) ) RCD_RETURN_FAULT(retU);

   //remember last used file path
   config.last_used_fpath   = tmps;
   config.b_have_last_fpath = true;

   GLog.slog = _("File opened:\n ");
   GLog.slog << tmps;
   GLog.DoLog();

   RCD_SET_OK(retU);
   ClearAll();
   EDID.ClearBuffer();

   u32_t idx = EDI_BASE_IDX;
   u32_t idxmax = 0;
   ediblk_t *pblk = EDID.getEDID()->blk;
   do {
      if ( EDI_BLK_SIZE != file.Read(&pblk[idx], EDI_BLK_SIZE) ) {
         RCD_SET_FAULT(retU);
         GLog.slog.Printf(_("Failed to load EDID block[%d]."), idx);
         GLog.DoLog();
      } else if (! EDID.VerifyChksum(idx)) {
         RCD_SET_FAULT(retU);
      }
      if (!RCD_IS_OK(retU)) break;
      if (idx == 0) idxmax = EDID.getEDID()->edi.base.num_extblk;
      idx++ ;
   } while (idx <= idxmax);

   file.Close();
   if (!RCD_IS_OK(retU)) return retU;

   GLog.slog.Printf(_("Loaded %u EDID block(s)\n "), idx);
   GLog.DoLog();

   retU = Reparse();

   return retU;
}

rcode wxEDID_Frame::SaveEDID() {
   rcode retU;
   RCD_SET_OK(retU);

   GLog.DoLog(_("SaveEDID()"));

   uint dtalen = (EDID.getNumValidBlocks() * EDI_BLK_SIZE);
   if ((dtalen == 0) || (dtalen > sizeof(edi_buf_t)) ) {
      RCD_RETURN_FAULT(retU);
   }

   for (u32_t itB=0; itB<EDID.getNumValidBlocks(); itB++) {
      retU = CalcVerifyChksum(itB);
      if (!RCD_IS_OK(retU)) return retU;
   }

   wxFileDialog dlg_open(this, _("Save EDID binary file"), _(""), _(""),
                         _("Binary (*.bin)|*.bin|All (*.*)|*.*"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
   //set last used path
   if (config.b_have_last_fpath) {
      dlg_open.SetPath(config.last_used_fpath);
   }

   if (dlg_open.ShowModal() != wxID_OK) {
      GLog.DoLog(_("SaveEDID() cancelled by user."));
      RCD_RETURN_OK(retU);
   }

   wxFile file;
   tmps = dlg_open.GetPath();
   if (! file.Open(tmps, wxFile::write) ) {
      RCD_RETURN_FAULT(retU);
   }
   //save edid_t - no extensions
   if ( dtalen != file.Write(EDID.getEDID()->buff, dtalen) ) {
      RCD_SET_FAULT(retU);
   }
   file.Close();
   if (RCD_IS_OK(retU)) {
      GLog.slog = _("File saved:\n ");
      GLog.slog << tmps;
      GLog.DoLog();
   }

   return retU;
}

rcode wxEDID_Frame::SaveRep_SubGrps(edi_grp_cl *pgrp, wxString& reps) {
   rcode      retU;
   static int rcdepth = -1;

   RCD_SET_OK(retU);

   if (pgrp == NULL) {
      RCD_RETURN_FAULT(retU);
   }

   rcdepth++ ;
   if (rcdepth > 1) { //hardcoded nesting depth limit
      rcdepth-- ;
      RCD_RETURN_FAULT(retU);
   }

   wxString sval;

   //indentation based on nesting depth
   wxString grpINDENT;
   for (int nsub=0; nsub<rcdepth; nsub++) {
      grpINDENT << wxIDNT;
   }

   sval.Printf(_("0x%04X: "), pgrp->getAbsOffs());
   reps << wxLF << grpINDENT << sval << pgrp->GroupName << wxLF;

   for (u32_t itf=0; itf<(pgrp->FieldsA.GetCount()); itf++) {
      u32_t tmpi, ival;
      edi_dynfld_t* p_field = pgrp->FieldsA.Item(itf);
      if (p_field == NULL) {
         RCD_SET_FAULT(retU); break;
      }
      reps << wxIDNT << grpINDENT << wxString::FromAscii(p_field->field.name);
      //align values:
      tmpi = (16 - strlen(p_field->field.name));
      if (tmpi > 16) tmpi = 1;
      sval.Empty();
      sval.Pad(tmpi, wxSP[0]);
      reps << sval;
      //p_field value
      sval.Empty();
      retU = (EDID.*p_field->field.handlerfn)(OP_READ, sval, ival, p_field);
      if (!RCD_IS_OK(retU)) break;
      reps << sval;
      //align units / vmap strings
      tmpi = (6 - sval.Len());
      if (tmpi > 6) tmpi = 1;
      sval.Empty();
      sval.Pad(tmpi, wxSP[0]);
      reps << sval;
      //value interpretation (mapped)
      if ( ((p_field->field.flags & EF_VS) != 0) && (p_field->field.vmap != NULL)) {
         if (ival < p_field->field.vmap->nval) sval = wxString::FromAscii(p_field->field.vmap->vmap[ival].name);
         reps << sval;
      }
      //value unit
      retU = EDID.getValUnitName(sval, p_field->field.flags);
      if (!RCD_IS_OK(retU)) break;

      reps << sval;
      if ((p_field->field.flags & EF_NU) != 0) reps << wxTAB << _("<unused>");
      reps << wxLF;

   }
   if (! RCD_IS_OK(retU)) {
      /* this can happen if edid field definition contains bad combination
         of flags and handler type */
      reps << _("\n< internal error! >\n");
      rcdepth-- ;
      return retU;
   }

   //subgroups/subfields
   if (pgrp->getSubGrpCount() > 0) {

      for (u32_t sgp=0; sgp<pgrp->getSubGrpCount(); sgp++ ) {
         edi_grp_cl *subgrp;
         subgrp = pgrp->getSubGroup(sgp);

         retU = SaveRep_SubGrps(subgrp, reps);
         if (!RCD_IS_OK(retU)) break;
      }
   }

   rcdepth-- ;

   return retU;
}

rcode wxEDID_Frame::SaveReport() {
   rcode  retU;
   RCD_SET_OK(retU);

   GLog.DoLog(_("SaveReport()"));

   for (u32_t itB=0; itB<EDID.getNumValidBlocks(); itB++) {
      retU = CalcVerifyChksum(itB);
      if (!RCD_IS_OK(retU)) return retU;
   }

   wxString* reps = new wxString();
   if (reps == NULL) {
      RCD_RETURN_FAULT(retU);
   }
   reps->Alloc(16*1024);

   *reps = _("wxEDID v");
   *reps << ver << _("\nEDID structure and data:\nSource file: \"");
   *reps << edid_file_name << _("\"");

   if (EDID.getNumValidBlocks() == 0) {
      delete reps;
      RCD_RETURN_FAULT(retU);
   }

   for (u32_t blk=0; blk<EDID.getNumValidBlocks(); blk++ ) {
      tmps.Printf(_("\n\n----| EDID block [%u] |----\n"), blk);
      *reps << tmps;
      GroupsA_t* pBlockA = EDID.BlkGroupsA[blk];
      u32_t     grpcnt = EDID.BlkGroupsA[blk]->GetCount();

      for (u32_t itg=0; itg<grpcnt; itg++) {
         edi_grp_cl *pgrp = pBlockA->Item(itg);
         if (pgrp == NULL) {
            RCD_SET_FAULT(retU); break;
         }

         //group fields / subgroups / subfields
         retU = SaveRep_SubGrps(pgrp, *reps);
         if (!RCD_IS_OK(retU)) break;
      }
      if (!RCD_IS_OK(retU)) break;
   }

   if (!RCD_IS_OK(retU)) return retU;

   *reps << _("\n\n----| END |----\n");

   wxFileDialog dlg_open(this, _("Save EDID report"), _(""), _(""),
                         _("text (*.txt)|*.txt|All (*.*)|*.*"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
   //set last used path
   if (config.b_have_last_fpath) {
      dlg_open.SetPath(config.last_used_fpath);
   }

   if (dlg_open.ShowModal() != wxID_OK) {
      GLog.DoLog(_("SaveReport() cancelled by user."));
      delete reps;
      RCD_RETURN_OK(retU);
   }

   wxFile file;
   tmps = dlg_open.GetPath();
   if (! file.Open(tmps, wxFile::write) ) {
      delete reps;
      RCD_RETURN_FAULT(retU);
   }

   //save EDID - no extensions
   if ( reps->Len() != file.Write(reps->ToAscii(), reps->Len()) ) {
      RCD_SET_FAULT(retU);
   } else {
      GLog.slog = _("File saved:\n ");
      GLog.slog << tmps;
      GLog.DoLog();
   }

   file.Close();

   delete reps;
   return retU;
}

rcode wxEDID_Frame::ExportEDID_hex() {
   rcode retU;
   RCD_SET_OK(retU);

   GLog.DoLog(_("ExportEDID_hex()"));

   uint dtalen = (EDID.getNumValidBlocks() * EDI_BLK_SIZE);
   if ((dtalen == 0) || (dtalen > sizeof(edi_buf_t)) ) {
      RCD_RETURN_FAULT(retU);
   }

   for (u32_t itB=0; itB<EDID.getNumValidBlocks(); itB++) {
      retU = CalcVerifyChksum(itB);
      if (!RCD_IS_OK(retU)) return retU;
   }

   wxFileDialog dlg_open(this, _("Export EDID as text (hex)"), _(""), _(""),
                         _("text (*.txt)|*.txt|All (*.*)|*.*"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
   //set last used path
   if (config.b_have_last_fpath) {
      dlg_open.SetPath(config.last_used_fpath);
   }

   if (dlg_open.ShowModal() != wxID_OK) {
      GLog.DoLog(_("ExportEDID_hex() cancelled by user."));
      RCD_RETURN_OK(retU);
   }

   //gen hex
   wxString* shex = new wxString();
   if (shex == NULL) RCD_RETURN_FAULT(retU);

   shex->Alloc(4000);
   tmps.Empty();
   u8_t *edi = reinterpret_cast <u8_t*> (EDID.getEDID());

   for (u32_t itb=0; itb<dtalen; itb++) {
      tmps.Printf(_("%02X"), edi[itb]);
      *shex << tmps;
      if ((itb & 0x0F) == 0x0F) *shex << _("\n");
      if ((itb & 0x7F) == 0x7F) *shex << _("\n");
   }
   *shex << _("\nwxEDID v") << ver << _("\nEDID data in hex format.\nSource file:\n");
   *shex << edid_file_name << wxLF;

   wxFile file;
   tmps = dlg_open.GetPath();
   if (! file.Open(tmps, wxFile::write) ) {
      delete shex;
      RCD_RETURN_FAULT(retU);
   }
   //save edid_t - no extensions
   if ( shex->Len() != file.Write(shex->ToAscii(), shex->Len()) ) {
      RCD_SET_FAULT(retU);
   }
   file.Close();
   if (RCD_IS_OK(retU)) {
      GLog.slog = _("Export as Hex: File saved:\n ");
      GLog.slog << tmps;
      GLog.DoLog();
   }

   delete shex;
   return retU;
}

rcode wxEDID_Frame::ImportEDID_hex() {
   rcode    retU;
   char    *buff8 = NULL;
   u32_t    itb;
   ssize_t  itc, len;
   wxFile   file;

   RCD_SET_OK(retU);

   GLog.DoLog(_("ImportEDID_hex()"));

   if (! config.b_cmdln_txt_file) { //menu event: import EDID text file

      wxFileDialog dlg_open(this, _("Import EDID from text (hex)"), _(""), _(""),
                            _("text (*.txt)|*.txt|All (*.*)|*"), wxFD_DEFAULT_STYLE);
      //set last used path
      if (config.b_have_last_fpath) {
         dlg_open.SetPath(config.last_used_fpath);
      }

      if (dlg_open.ShowModal() != wxID_OK) {
         GLog.DoLog(_("ImportEDID_hex() canceled by user."));
         RCD_RETURN_OK(retU);
      }
      edid_file_name = dlg_open.GetFilename();
      tmps           = dlg_open.GetPath();

   } else {
      //open EDID text file provided as cmd line arg
      edid_file_name = config.last_used_fname;
      tmps           = config.cmd_open_file_path;
      config.b_cmdln_txt_file = false; //single shot
   }

   if (! file.Open(tmps, wxFile::read) ) RCD_RETURN_FAULT(retU);

   //remember last used file path
   config.last_used_fpath   = tmps;
   config.b_have_last_fpath = true;

   len = file.Length();
   if ( (len < (128*2)) || (len > (1024*4)) ) {
      /* reject files which are too small
         but also prevent importing essays :)
      */
      GLog.DoLog(_("Bad file. Allowed size is 256...4096 bytes."));
      file.Close();
      RCD_RETURN_FAULT(retU);
   }

   buff8 = new char[len+1];
   if (buff8 == NULL) {
      file.Close();
      RCD_RETURN_FAULT(retU);
   }
   //read text
   if ( len != file.Read(buff8, len) ) {
      RCD_SET_FAULT(retU);
   }
   file.Close();
   if (!RCD_IS_OK(retU)) {
      delete [] buff8;
      return retU;
   }

   GLog.slog = _("File imported:\n ");
   GLog.slog << tmps;
   GLog.DoLog();

   ClearAll();
   EDID.ClearBuffer();
   //hex to bin
   itb = 0;
   u8_t *edi = EDID.getEDID()->buff;
   for (itc=0; itc<len; itc++) {
      ulong chr = buff8[itc];
      if ( (chr < '0') || ((chr > '9') && (chr < 'A')) || \
          ((chr > 'F') && (chr < 'a')) || (chr > 'f') ) continue;
      //Not UTF8, just take 2 chars in one call;
      tmps = wxString::FromUTF8(&buff8[itc], 2); itc++;
      if (! tmps.ToULong(&chr, 16)) {
         RCD_SET_FAULT(retU);
         break;
      }
      edi[itb] = static_cast <u8_t> (chr);
      if (itb == offsetof(edid_t, num_extblk)) {
         /*don't read extensions if they are not claimed,
           otherwise read until end of the buffer or file */
         if (chr == 0) len = (itc+1);
      }
      if (itb > sizeof(edi_buf_t)) break;
      itb++ ;
   }
   delete [] buff8;

   retU = Reparse();

   return retU;
}

rcode wxEDID_Frame::UpdateEDID_tree() {
   rcode        retU;
   wxTreeItemId Root, Block;

   tree_edid->DeleteAllItems();

   if (EDID.EDI_BaseGrpA.GetCount() < 1) RCD_RETURN_FAULT(retU);

   Root = tree_edid->AddRoot(edid_file_name);

   Block = tree_edid->AppendItem(Root, _("EDID: Main"), -1, -1, NULL);
   for (u32_t itg=0; itg<EDID.EDI_BaseGrpA.GetCount(); itg++) {
      wxTreeItemId  item;
      edi_grp_cl   *group;
      group = EDID.EDI_BaseGrpA.Item(itg);
      tmps = group->GroupName;
      item = tree_edid->AppendItem(Block, tmps, -1, -1, NULL);
      tree_edid->SetItemData(item, group);
   }

   if (EDID.EDI_Ext0GrpA.GetCount() < 1) {
      tree_edid->ExpandAllChildren(Root);
      RCD_RETURN_OK(retU);
   }

   Block = tree_edid->AppendItem(Root, _("EDID: CEA-861"), -1, -1, NULL);
   for (u32_t itg=0; itg<EDID.EDI_Ext0GrpA.GetCount(); itg++) {
      wxTreeItemId  item;
      edi_grp_cl   *group;
      group = EDID.EDI_Ext0GrpA.Item(itg);
      tmps = group->GroupName;
      item = tree_edid->AppendItem(Block, tmps, -1, -1, NULL);
      tree_edid->SetItemData(item, group);
      //sub groups
      if (group->getSubGrpCount() > 0) {
         for (u32_t idx=0; idx<group->getSubGrpCount(); idx++ ) {
            wxTreeItemId  subitem;
            edi_grp_cl   *subgroup;
            subgroup = group->getSubGroup(idx);
            tmps = subgroup->GroupName;
            subitem = tree_edid->AppendItem(item, tmps, -1, -1, NULL);
            tree_edid->SetItemData(subitem, subgroup);
         }
      }
   }

   //auto expand after creating
   tree_edid->ExpandAllChildren(Root);

   RCD_RETURN_OK(retU);
}

rcode wxEDID_Frame::EDID_updt_Grid_Row(int nrow, edi_dynfld_t *p_field) {
   rcode        retU;
   u32_t        tmpi;
   static const wxString fmsg = _("FAULT:EDID_updt_Grid_Row()");

   if (p_field == NULL) {
      RCD_SET_FAULT(retU);
      GLog.DoLog(fmsg);
      GLog.PrintRcode(retU);
      return retU;
   }
   //p_field name
   tmps = wxString::FromAscii(p_field->field.name);
   edi_fgrid->SetCellValue(nrow, EDI_GRCOL_NAME, tmps);
   edi_fgrid->SetReadOnly(nrow, EDI_GRCOL_NAME, true);
   //p_field value
   tmps.Empty();
   retU = (EDID.*p_field->field.handlerfn)(OP_READ, tmps, tmpi, p_field);
   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(fmsg);
      GLog.PrintRcode(retU);
      //return retU;
      tmps = _("<error!>");
   }
   edi_fgrid->SetCellValue(nrow, EDI_GRCOL_VAL, tmps);
   //is read-only
   if (EDID.GetRDignore()) {
      edi_fgrid->SetReadOnly(nrow, EDI_GRCOL_VAL, false);
   } else {
      edi_fgrid->SetReadOnly(nrow, EDI_GRCOL_VAL, (p_field->field.flags & EF_RD));
   }
   //get types
   retU = EDID.getValTypeName(tmps, p_field->field.flags);
   if (!RCD_IS_OK(retU)) return retU;
   edi_fgrid->SetCellValue(nrow, EDI_GRCOL_TYPE, tmps);
   edi_fgrid->SetReadOnly(nrow, EDI_GRCOL_TYPE, true);
   //get units
   retU = EDID.getValUnitName(tmps, p_field->field.flags);
   if (!RCD_IS_OK(retU)) return retU;
   if (tmps.IsEmpty()) tmps = _("--");
   edi_fgrid->SetCellValue(nrow, EDI_GRCOL_UNIT, tmps);
   edi_fgrid->SetReadOnly(nrow, EDI_GRCOL_UNIT, true);
   //get flags
   retU = EDID.getValFlagsName(tmps, p_field->field.flags);
   if (!RCD_IS_OK(retU)) return retU;
   edi_fgrid->SetCellValue(nrow, EDI_GRCOL_FLG, tmps);
   edi_fgrid->SetReadOnly(nrow, EDI_GRCOL_FLG, true);

   //set row color for type
   RCD_SET_OK(retU);

   wxGridCellAttr *RowAttr = edi_fgrid->GetOrCreateCellAttr(nrow, EDI_GRCOL_NAME);
   RowAttr->SetTextColour(*wxBLACK);
   RowAttr->SetBackgroundColour(*wxWHITE);

   if (p_field->field.flags & EF_FLT) {
      RowAttr->SetBackgroundColour(grid_typeRGB_float);
      edi_fgrid->SetRowAttr(nrow, RowAttr);
      return retU;
   }
   if (p_field->field.flags & EF_HEX) {
      RowAttr->SetBackgroundColour(grid_typeRGB_hex);
      edi_fgrid->SetRowAttr(nrow, RowAttr);
      return retU;
   }
   if (p_field->field.flags & EF_BIT) {
      RowAttr->SetBackgroundColour(grid_typeRGB_bit);
      edi_fgrid->SetRowAttr(nrow, RowAttr);
      return retU;
   }
   //default color
   edi_fgrid->SetRowAttr(nrow, RowAttr);

   return retU;
}

rcode wxEDID_Frame::UpdateEDID_grid(edi_grp_cl* edigrp) {
   rcode     retU;

   if (edigrp == NULL) RCD_RETURN_FAULT(retU);

   int NumR  = edi_fgrid->GetNumberRows();
   int nGrpF = edigrp->FieldsA.GetCount();

   if (NumR < nGrpF) {
      edi_fgrid->AppendRows(nGrpF-NumR);
   } else if (NumR > nGrpF) {
      edi_fgrid->DeleteRows(0, (NumR-nGrpF));
   }

   for (int itf=0; itf<nGrpF; itf++) {
      edi_dynfld_t *field = edigrp->FieldsA.Item(itf);
      if (field == NULL) RCD_RETURN_FAULT(retU);
      retU = EDID_updt_Grid_Row(itf, field);
   }

   edi_fgrid->AutoSizeColumns(false);
   edi_fgrid->FitInside();

   flags.edigridblk_ok = 1;

   RCD_RETURN_OK(retU);
}

rcode wxEDID_Frame::SetFieldDesc(int row) {
   rcode retU;
   RCD_SET_OK(retU);
   //show field describtion
   if (edigrp_sel != NULL) {
      edi_dynfld_t *p_field = edigrp_sel->FieldsA.Item(row);
      if (p_field == NULL) {
         RCD_SET_FAULT(retU);
         GLog.DoLog(_("FAULT: SetFieldDesc()"));
         GLog.PrintRcode(retU);
         return retU;
      }
      if (p_field->field.flags & EF_GPD) {
         txc_edid_info->SetValue(edigrp_sel->GroupDesc);
      } else {
         txc_edid_info->SetValue(wxString::FromUTF8(p_field->field.desc));
      }
   }
   return retU;
}

rcode wxEDID_Frame::DTD_Ctor_Modeline() {
   rcode   retU;
   int       tmpi = sct_xres->data;

   //pix clk tmp
   GLog.slog.Printf(_("%.02f"), static_cast <double> (sct_pixclk->data / 100.0));

   tmps.Printf(_("\"%dx%dx"), tmpi, sct_vres->data);
   tmps << txc_vrefresh->GetValue() << _("\" ");
   tmps << GLog.slog;
   //xres
   tmps << wxSP << tmpi << wxSP;
   //hsoffs - front porch
   tmpi += sct_hsoffs->data; tmps << tmpi << wxSP;
   //hsync width
   tmpi += sct_hswidth->data; tmps << tmpi << wxSP;
   //hsync end - back porch
   tmps << txc_htotal->GetValue() << wxSP;
   //vres
   tmpi = sct_vres->data; tmps << tmpi << wxSP;
   //vsync - front porch
   tmpi += sct_vsoffs->data; tmps << tmpi << wxSP;
   //vsync width
   tmpi += sct_vswidth->data; tmps << tmpi << wxSP;
   //vsync - back porch
   tmps << txc_vtotal->GetValue();

   txc_modeline->SetValue(tmps);

   RCD_RETURN_OK(retU);
}

rcode wxEDID_Frame::DTD_Ctor_WriteInt(dtd_sct_cl& sct) {
                rcode   retU, retU2;
   static       wxString  sval;
   static const wxString  fmsg  = _("FAULT: DTD_Ctor_WriteInt()");
            edi_dynfld_t *p_field = sct.field;
                uint      val;

   if (p_field == NULL) {
      RCD_SET_FAULT(retU);
      GLog.DoLog(fmsg);
      GLog.PrintRcode(retU);
      return retU;
   }
   //if (edigrp_sel == NULL) : not checking, because the evt is disabled in such case

   val = sct.data;

   tmps = edigrp_sel->GroupName;
   tmps << _(" (") << win_stat_bar->GetStatusText(SBAR_GRPOFFS) << _(") \n");
   tmps << _("p_field changed: ") << wxString::FromAscii(p_field->field.name) << _("\n");
   //GLog.DoLog(tmps);

   sval.Empty();
   retU = (EDID.*p_field->field.handlerfn)(OP_WRINT, sval, val, p_field);
   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(fmsg);
      GLog.PrintRcode(retU);
   }
   tmps << _("new value: ") << val << _("\n"); //GLog.DoLog(tmps);

   //re-read p_field -> immediately check the value/revert to last correct value
   sval.Empty();
   retU2 = (EDID.*p_field->field.handlerfn)(OP_READ, sval, val, p_field);
   if (!RCD_IS_OK(retU2)) {
      GLog.DoLog(fmsg);
      GLog.PrintRcode(retU2);
   }
   sct.SetValue(val);
   tmps << _("verified value: ") << val << _("\n");
   GLog.DoLog(tmps);

   if (!RCD_IS_OK(retU)) {
      GLog.DoLog(fmsg);
      GLog.PrintRcode(retU);
   }

   return retU;
}

rcode wxEDID_Frame::DTD_Ctor_Recalc() {
   rcode retU;
   double linew, pixclk, tline, totlies, framet;

   linew   = (sct_hblank->data + sct_xres->data);
   pixclk  = (sct_pixclk->data * 10000);
   tline   = (linew / pixclk);
   totlies = (sct_vblank->data + sct_vres->data);
   framet  = (totlies * tline);
   tmps.Printf(_("%.02f"), (1.0/framet)); //Hz
   txc_vrefresh->SetValue(tmps);
   tmps.Printf(_("%.03f"), ((1.0/tline)/1000.0)); //kHz
   txc_hfreq->SetValue(tmps);

   RCD_RETURN_OK(retU);
}

rcode wxEDID_Frame::DTD_Ctor_read_field(dtd_sct_cl& sct, const edi_grp_cl& group, const u32_t idx_field) {
   rcode         retU;
   uint          val   = 0;
   edi_dynfld_t *p_field = NULL;

   tmps.Empty();
   p_field = group.FieldsA.Item(idx_field);
   if (p_field == NULL) RCD_RETURN_FAULT(retU);

   retU = (EDID.*p_field->field.handlerfn)(OP_READ, tmps, val, p_field);
   sct.SetValue(val);
   if (! RCD_IS_OK(retU)) {
      GLog.DoLog(_("DTD_Ctor_read_field() FAILED."));
      GLog.PrintRcode(retU);
   }
   sct.field = p_field;
   sct.data = val;
   return retU;
}

rcode wxEDID_Frame::DTD_Ctor_read_all() {
   rcode retU;

   if (edigrp_sel == NULL) RCD_RETURN_FAULT(retU);

   edi_grp_cl& group = *edigrp_sel;
   if (group.getTypeID() != ID_DTD) RCD_RETURN_FAULT(retU);

   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_pixclk , group, DTD_IDX_PIXCLK ) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_xres   , group, DTD_IDX_HAPIX  ) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_hblank , group, DTD_IDX_HBPIX  ) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_vres   , group, DTD_IDX_VALIN  ) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_vblank , group, DTD_IDX_VBLIN  ) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_hsoffs , group, DTD_IDX_HSOFFS ) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_hswidth, group, DTD_IDX_HSWIDTH) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_vsoffs , group, DTD_IDX_VSOFFS ) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_vswidth, group, DTD_IDX_VSWIDTH) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_hborder, group, DTD_IDX_HBORD  ) ) ) RCD_RETURN_FAULT(retU);
   if (! RCD_IS_OK( DTD_Ctor_read_field(*sct_vborder, group, DTD_IDX_VBORD  ) ) ) RCD_RETURN_FAULT(retU);

   retU = DTD_Ctor_Recalc();
   return retU;
}

rcode wxEDID_Frame::Reparse() {
   rcode retU;
   u32_t n_extblk;

   ntbook->SetSelection(0); //go to tree view
   bool err_ignore = EDID.GetERRignore();
   edigrp_sel = NULL;
   dtd_panel->Enable(false);
   edi_fgrid->Enable(false);
   ClearAll(false); //do not clear grid when called directly from menu

   GLog.DoLog(_("Reparse()"));

   retU = VerifyChksum(EDI_BASE_IDX);
   if (!RCD_IS_OK(retU)) {
      if (! err_ignore) return retU;
      GLog.PrintRcode(retU);
   }

   retU = EDID.ParseEDID_Base(n_extblk);
   if (!RCD_IS_OK(retU)) {
      if (! err_ignore) return retU;
      GLog.PrintRcode(retU);
   }

   if (n_extblk > 0) {
      retU = VerifyChksum(EDI_EXT0_IDX);
      if (!RCD_IS_OK(retU)) {
         if (! err_ignore) return retU;
         GLog.PrintRcode(retU);
      }

      retU = EDID.ParseEDID_CEA();
      if (!RCD_IS_OK(retU)) {
         if (err_ignore) {
            GLog.PrintRcode(retU);
            EDID.ForceNumValidBlocks(2);
         } else
         return retU;
      }
   }

   retU = UpdateEDID_tree();
   if (!RCD_IS_OK(retU)) return retU;

   EnableControl(true);
   return retU;
}

void wxEDID_Frame::ClearAll(bool clrGrid) {
   EDID.EDI_BaseGrpA.Empty();
   EDID.EDI_Ext0GrpA.Empty();
   EDID.EDI_Ext1GrpA.Empty();
   EDID.EDI_Ext2GrpA.Empty();

   if (clrGrid) edi_fgrid->ClearGrid();
   edi_fgrid->Enable(false);
   EnableControl(false);
   txc_edid_info->Clear();
   tmps.Empty();
   win_stat_bar->SetStatusText(tmps, SBAR_GRPOFFS);
}

void wxEDID_Frame::EnableControl(bool enb) {
   mnu_save_edi ->Enable(enb);
   mnu_exphex   ->Enable(enb);
   mnu_save_text->Enable(enb);

   flags.ctrl_enabled = enb;
}

rcode wxEDID_Frame::VerifyChksum(uint block) {
   rcode retU;
   wxString schksum;

   schksum.Printf(_("0x%02X"), EDID.getEDID()->blk[block][offsetof(edid_t, chksum)]);
   tmps.Printf(_("EDID block %u checksum= "), block);
   tmps << schksum;
   if (EDID.VerifyChksum(block)) {
      tmps << _(" OK");
      RCD_SET_OK(retU);
   } else {
      tmps << _(" BAD!");
      RCD_SET_FAULT(retU);
   }
   GLog.DoLog(tmps);
   return retU;
}

rcode wxEDID_Frame::CalcVerifyChksum(uint block) {
   rcode retU;

   tmps.Printf(_("EDID block %u checksum= 0x%02X "), block, EDID.genChksum(block));
   if (EDID.VerifyChksum(block)) {
      tmps << _("OK");
      RCD_SET_OK(retU);
   } else {
      tmps << _("BAD!");
      RCD_SET_FAULT(retU);
   }
   GLog.DoLog(tmps);
   return retU;
}

rcode wxEDID_Frame::WriteField() {
   //Call field handler(write), then re-read the value to verify it
   rcode   retU, retU2;
   uint    tmpi;
   static  wxString sval;

   if (edigrp_sel == NULL) RCD_RETURN_FAULT(retU);

   tmpi = (row_op+1);
   if ( (tmpi > edigrp_sel->FieldsA.GetCount()) || (row_op < 0) ) {
      RCD_RETURN_FAULT(retU);
   }

   edi_dynfld_t *p_field = edigrp_sel->FieldsA.Item(row_op);

   if (p_field == NULL) {
      RCD_RETURN_FAULT(retU);
   }
   //write value
   tmps = edigrp_sel->GroupName;
   tmps << _(" (") << win_stat_bar->GetStatusText(SBAR_GRPOFFS) << _(") \n");
   tmps << _("field changed: ") << wxString::FromAscii(p_field->field.name) << _("\n");

   sval = edi_fgrid->GetCellValue(row_op, EDI_GRCOL_VAL);
   tmps << _("new value: ") << sval << _("\n");
   retU = (EDID.*p_field->field.handlerfn)(OP_WRSTR, sval, tmpi, p_field);
   if (! RCD_IS_OK(retU)) {
      GLog.PrintRcode(retU);
      //print error code, but continue: re-reading will revert the field value
   }

   //re-read field -> immediately check the value/revert to last correct value
   sval.Empty();
   retU2 = (EDID.*p_field->field.handlerfn)(OP_READ, sval, tmpi, p_field);
   if (!RCD_IS_OK(retU2)) {
      GLog.PrintRcode(retU2);
   }
   edi_fgrid->SetCellValue(row_op, EDI_GRCOL_VAL, sval);
   tmps << _("verified value: ") << sval << _("\n");
   GLog.DoLog(tmps);

   //changing field value can lead to change in data structure,
   //check the EF_FGR flag (group refresh).
   if ((p_field->field.flags & EF_FGR) != 0) {
      flags.edi_grp_rfsh = 1;
      //re-parse group data, possibly changing the layout
      retU = edigrp_sel->ForcedGroupRefresh();
   }

   return retU;
}

wxBEGIN_EVENT_TABLE(fgrid_cl, wxGrid)
   EVT_MENU(wxID_ANY, fgrid_cl::evt_gridcell_vmnu)
wxEND_EVENT_TABLE()

void fgrid_cl::evt_gridcell_vmnu(wxCommandEvent& evt) {
   int row = GetGridCursorRow();

   HideCellEditControl();

   //MenuItem id contains selected value:
   tmps.Empty(); tmps << evt.GetId();
   SetCellValue(row, EDI_GRCOL_VAL, tmps);

   wxGridEvent evtgr(wxID_ANY, wxEVT_GRID_CELL_CHANGED, NULL, row, EDI_GRCOL_VAL);
   AddPendingEvent(evtgr);
}


wxBEGIN_EVENT_TABLE(dtd_screen_cl, wxPanel)
    EVT_PAINT           (dtd_screen_cl::evt_paint   )
    EVT_ERASE_BACKGROUND(dtd_screen_cl::evt_erase_bg)
wxEND_EVENT_TABLE()

void dtd_screen_cl::scr_aspect_ratio(wxSize& dcsize, wxPaintDC& dc) {
   float  faspHV;
   bool   b_scaling_to_dc_Y;

   {
      wxSize HVtotal;
      float  faspDC;

      HVtotal.x = pwin->dtd_Htotal;
      HVtotal.y = pwin->dtd_Vtotal;

      //check DC aspect ratio
      faspDC  = dcsize.x;
      faspDC /= dcsize.y;
      faspHV  = HVtotal.x;
      faspHV /= HVtotal.y;

      b_scaling_to_dc_Y = (faspDC > faspHV);
   }

   {  //DC clipping & scaling
      wxSize  szClip;
      wxPoint dcXY(0, 0);

      if (b_scaling_to_dc_Y) {

         szClip.y  = dcsize.y;
         szClip.x  = dcsize.y;
         szClip.x *= faspHV;

         dcXY.x    = dcsize.x;
         dcXY.x   -= szClip.x;
         dcXY.x  >>= 1;

      } else {
         //scaling to dc X-axis
         szClip.x  = dcsize.x;
         szClip.y  = dcsize.x;
         szClip.y /= faspHV;

         dcXY.y    = dcsize.y;
         dcXY.y   -= szClip.y;
         dcXY.y  >>= 1;
      }

      dcsize = szClip;
      dc.SetDeviceOrigin(dcXY.x, dcXY.y);
   }
}

rcode dtd_screen_cl::calc_coords(wxSize& dcsize) {
   rcode  retU;
   float  ftmpA, ftmpB, fltDCsize, scale_base;

   if (pwin == NULL) RCD_RETURN_FAULT(retU);

   //V-sync pulse bar (top edge)
   rcVsync.x = 0; rcVsync.y = 0; rcVsync.width = dcsize.x;
   fltDCsize = dcsize.y;
   scale_base = (pwin->sct_vblank->data + pwin->sct_vres->data);
   if (scale_base == 0) RCD_RETURN_FAULT(retU);
   ftmpA = pwin->sct_vswidth->data;
   ftmpB = (ftmpA*fltDCsize)/scale_base;
   rcVsync.height = ftmpB;
   if (rcVsync.height == 0) rcVsync.height = 1;

   //Screen Area Y-coords rcScreen
   // Y
   ftmpA = (pwin->sct_vblank->data - pwin->sct_vsoffs->data);
   ftmpB = (ftmpA*fltDCsize)/scale_base;
   rcScreen.y = ftmpB;
   // Height
   ftmpA = pwin->sct_vres->data;
   ftmpB = (ftmpA*fltDCsize)/scale_base;
   rcScreen.height = ftmpB;

   //borders
   szVborder.y = 0;
   if (pwin->sct_vborder->data != 0) {
      ftmpA = pwin->sct_vborder->data;
      ftmpB = (ftmpA*fltDCsize)/scale_base;
      szVborder.y = ftmpB;
      if (szVborder.y == 0) szVborder.y = 1;
   }
   szHborder.y = rcScreen.height;

   //H-sync pulse bar (vertical, left edge)
   rcHsync.x = 0; rcHsync.y = rcVsync.height;
   rcHsync.height = (dcsize.y-rcVsync.height);
   fltDCsize = dcsize.x;
   scale_base = (pwin->sct_hblank->data + pwin->sct_xres->data);
   if (scale_base == 0) RCD_RETURN_FAULT(retU);
   ftmpA = pwin->sct_hswidth->data;
   ftmpB = (ftmpA*fltDCsize)/scale_base;
   rcHsync.width = ftmpB;
   if (rcHsync.width == 0) rcHsync.width = 1;

   //Screen Area X-coords rcScreen
   // X
   ftmpA = (pwin->sct_hblank->data - pwin->sct_hsoffs->data);
   ftmpB = (ftmpA*fltDCsize)/scale_base;
   rcScreen.x = ftmpB;
   // Width
   ftmpA = pwin->sct_xres->data;
   ftmpB = (ftmpA*fltDCsize)/scale_base;
   rcScreen.width = ftmpB;

   //borders
   szHborder.x = 0;
   if (pwin->sct_hborder->data != 0) {
      ftmpA = pwin->sct_hborder->data;
      ftmpB = (ftmpA*fltDCsize)/scale_base;
      szHborder.x = ftmpB;
      if (szHborder.x == 0) szHborder.x = 1;
   }
   szVborder.x = rcScreen.width;

   RCD_RETURN_OK(retU);
}

void dtd_screen_cl::scr_area_str(wxPaintDC& dc) {

   {  //active area size to string
      int  Xres, Yres, tmpi;

      tmpi   = pwin->sct_hborder->data;
      tmpi <<= 1; //exclude borders on both screen edges
      Xres   = pwin->sct_xres->data;
      Xres  -= tmpi;

      tmpi   = pwin->sct_vborder->data;
      tmpi <<= 1;
      Yres   = pwin->sct_vres->data;
      Yres  -= tmpi;

      tmps.Printf(_("%u x %u"), Xres, Yres);
   }


   {  //set the DC font
      wxFont font;

      font = dc.GetFont();
      font.MakeBold();

      dc.SetFont(font);
   }

   {  //draw the string
      wxPoint str_pos;
      wxSize  str_sz;

      str_sz = dc.GetTextExtent(tmps);

      //skip if active area is too small
      if ((str_sz.x >= rcScreen.width ) ||
          (str_sz.y >= rcScreen.height) ) return;

      str_sz.x >>= 1;
      str_sz.y >>= 1;

      str_pos.x   = rcScreen.width;
      str_pos.x >>= 1;
      str_pos.x  += rcScreen.x;
      str_pos.x  -= str_sz.x;

      str_pos.y   = rcScreen.height;
      str_pos.y >>= 1;
      str_pos.y  += rcScreen.y;
      str_pos.y  -= str_sz.y;

      dc.SetTextForeground(cResStr);
      dc.DrawText(tmps, str_pos);
   }

}

void dtd_screen_cl::evt_erase_bg(wxEraseEvent& evt) {
   evt.Skip(false);
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
void dtd_screen_cl::evt_paint(wxPaintEvent& evt){
   rcode     retU;
   wxPaintDC dc(this);
   wxSize    dcsize = dc.GetSize();
   wxBrush   bgbrush(*wxMEDIUM_GREY_BRUSH);

   if (!IsEnabled()) {
      dc.SetBrush(bgbrush);
      dc.DrawRectangle(0, 0, dcsize.x, dcsize.y);
      return;
   }

   if (pwin->b_dtd_keep_aspect) {
      scr_aspect_ratio(dcsize, dc);
   }

   retU = calc_coords(dcsize);
   if (! RCD_IS_OK(retU)) {
      pwin->GLog.DoLog(_("dtd_screen:evt_paint():calc_coords() FAILED."));
      pwin->GLog.PrintRcode(retU);
      return;
   }

   //background
   dc.SetBrush(*wxBLACK);
   dc.DrawRectangle(0, 0, dcsize.x, dcsize.y);

   dc.SetPen(*wxTRANSPARENT_PEN);

   //vsync bar
   bgbrush.SetColour(cVsync);
   dc.SetBrush(bgbrush);
   dc.DrawRectangle(rcVsync);

   //hsync bar
   bgbrush.SetColour(cHsync);
   dc.SetBrush(bgbrush);
   dc.DrawRectangle(rcHsync);

   //sandcastle area (hsync + vsync)
   bgbrush.SetColour(cSandC);
   dc.SetBrush(bgbrush);
   dc.DrawRectangle(0, 0, rcHsync.width, rcVsync.height);

   //screen area
   bgbrush.SetColour(cScrArea);
   dc.SetBrush(bgbrush);
   dc.DrawRectangle(rcScreen);

   //print effective resolution on the active screen area.
   scr_area_str(dc);

   //h-border
   if (szHborder.x != 0) {
      bgbrush.SetColour(cSandC);
      dc.SetBrush(bgbrush);
      dc.DrawRectangle(rcScreen.x, rcScreen.y, szHborder.x, szHborder.y);
      dc.DrawRectangle((rcScreen.x + rcScreen.width) - szHborder.x,
                       rcScreen.y,
                       szHborder.x,
                       szHborder.y);
   }
   //v-border
   if (szVborder.y != 0) {
      bgbrush.SetColour(cSandC);
      dc.SetBrush(bgbrush);
      dc.DrawRectangle(rcScreen.x, rcScreen.y, szVborder.x, szVborder.y);
      dc.DrawRectangle(rcScreen.x,
                       (rcScreen.y + rcScreen.height) - szVborder.y,
                       szVborder.x,
                       szVborder.y);
   }

   evt.Skip(false);
}
#pragma GCC diagnostic warning "-Wunused-parameter"

