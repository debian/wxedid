/***************************************************************
 * Name:      wxEDID_App.cpp
 * Purpose:   Code for Application Class
 * Author:    Tomasz Pawlak (tomasz.pawlak@wp.eu)
 * Created:   2014-03-18
 * Copyright: Tomasz Pawlak (C) 2014-2020
 * License:   GPLv3
 **************************************************************/

#include "wxEDID_App.h"
#include "debug.h"

//(*AppHeaders
#include "../src/wxEDID_Main.h"
#include <wx/image.h>
//*)

#include <wx/filename.h>
#include <wx/tokenzr.h>

extern config_t config;

IMPLEMENT_APP(wxEDID_App);

bool wxEDID_App::OnInit() {
   bool wxsOK;

   wxsOK = CmdLineArgs();

   if (!wxsOK) return wxsOK;

   LoadConfig();

   //(* AppInitialize

   //wxInitAllImageHandlers();
   if ( wxsOK )
   {
      wxEDID_Frame* Frame = new wxEDID_Frame(0);
      Frame->Show();
      SetTopWindow(Frame);
   }
   //*)
   return wxsOK;
}

int wxEDID_App::OnExit() {

   SaveConfig();
   return true;
}

void wxEDID_App::SaveConfig() {
   wxString   cfg_str;
   wxFile     cfg_file;
   wxFileName cfg_fname;

   cfg_fname.SetPath( wxGetHomeDir() );
   cfg_fname.SetName( _(".wxEDID_cfg") );
   cfg_str = cfg_fname.GetFullPath();

   if (! cfg_file.Open(cfg_str, wxFile::write) ) {
      ERR_PRINT("[E!] Can't open file: \'%s\'\n", static_cast<const char*> (cfg_fname.GetFullPath().c_str()) );
   }

   cfg_str.Printf(_("dtd_keep_aspect=%d\n"
                    "last_used_fpath=%s\n"),
                     config.b_dtd_keep_aspect,
                     config.last_used_fpath
                 );

   cfg_file.Write(cfg_str);
}

void wxEDID_App::LoadConfig() {
   wxString   cfg_str;
   wxFileName cfg_fname;

   cfg_fname.SetPath( wxGetHomeDir() );
   cfg_fname.SetName( _(".wxEDID_cfg") );
   cfg_str = cfg_fname.GetFullPath();

   if (! wxFileExists(cfg_str)) {
      config.b_have_last_fpath = false;
      config.b_dtd_keep_aspect = true; //default
      return;
   }

   {
      wxFile  cfg_file;

      if (! cfg_file.Open(cfg_str, wxFile::read) ) {
         ERR_PRINT("[E!] Can't open file: \'%s\'\n", static_cast<const char*> (cfg_fname.GetFullPath().c_str()) );
         config.b_have_last_fpath = false;
         config.b_dtd_keep_aspect = true; //default
         return;
      }

      cfg_file.ReadAll(&cfg_str);
   }

   {
      i32_t tkidx     = -1;
      wxString          token;
      wxStringTokenizer tokenz(cfg_str, _("=\n"));

      while ( tokenz.HasMoreTokens() ) {
         token  = tokenz.GetNextToken();
         tkidx ++ ;
         if (0 == (tkidx & 1)) continue;
         //tokens @ odd idx hold the values
         switch (tkidx) {
            case 1: //dtd aspect ratio mode
               {
                  long mode;

                  token.ToLong(&mode);
                  config.b_dtd_keep_aspect = (mode != 0) ? true : false;
                  continue;
               }
            case 3: //last path
               {
                  bool       dexist;
                  wxFileName last_dir(token);

                  cfg_str = last_dir.GetPath();
                  dexist  = wxDirExists( cfg_str );

                  if (dexist) {
                     config.last_used_fpath   = token;
                     config.b_have_last_fpath = true;
                  } else {
                     config.b_have_last_fpath = false;
                  }
               }
         }
      }
   }
}

bool wxEDID_App::CmdLineArgs() {
   wxString argstr;
   bool     fexist;

   config.b_cmdln_txt_file = false;
   config.b_cmdln_bin_file = false;

   if (wxApp::argc > 2) {

      argstr = wxApp::argv[1];

      //import EDID text file (hex ASCII)
      if ( (wxApp::argc == 3) &&
           ( (argstr == _("-t")) || (argstr == _("--text")) ) ) { //text file
         argstr = wxApp::argv[2];
         fexist = wxFileExists(argstr);

         if (! fexist) {
            ERR_PRINT("[E!] Can't open file: \'%s\'\n", static_cast<const char*> (argstr.c_str()) );
            return false;
         }
         config.cmd_open_file_path = argstr;
         config.b_cmdln_txt_file   = true;
      } else {
         ERR_PRINT("[E!] Unknown / too many arguments.\n");
         return false;
      }
   }
   //default: load binary EDID file
   else if (wxApp::argc == 2) {
      argstr = wxApp::argv[1];
      fexist = wxFileExists(argstr);
      if (! fexist) {
         ERR_PRINT("[E!] Can't open file: \'%s\'\n", static_cast<const char*> (argstr.c_str()) );
         return false;
      }
      config.cmd_open_file_path = argstr;
      config.b_cmdln_bin_file   = true;
   }

   {
      wxFileName fname(config.cmd_open_file_path);

      if (! wxIsAbsolutePath(config.cmd_open_file_path) ) { //normalize file path

         fname.Normalize(wxPATH_NORM_ALL, wxGetCwd());
         DBG0_PRINT("[i] Absolute path \'%s\'\n", static_cast<const char*> (config.cmd_open_file_path.c_str()) );
      }

      config.cmd_open_file_path = fname.GetFullPath();
      config.last_used_fname    = fname.GetFullName();
   }

   return true;
}


