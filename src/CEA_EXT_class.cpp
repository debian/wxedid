/***************************************************************
 * Name:      CEA_EXT_class.cpp
 * Purpose:   CEA/CTA-861-G Extended Tag Codes
 * Author:    Tomasz Pawlak (tomasz.pawlak@wp.eu)
 * Created:   2019-12-20
 * Copyright: Tomasz Pawlak (C) 2019-2020
 * License:   GPLv3
 **************************************************************/

#include "debug.h"
#include "rcdunits.h"
#ifndef idCEA_EXT
   #error "CEA_EXT_class.cpp: missing unit ID"
#endif
#define RCD_UNIT idCEA_EXT
#include "returncode/rcode.h"

#include "wxedid_rcd_func.h"
#include "wxedid_rcd_scope_ptr.h"

RCD_AUTOGEN_DEFINE_UNIT

#include <stddef.h>

#include "CEA_EXT.h"

#define EDID_CEA_CLASS
#include "EDID_class.h"
#undef  EDID_CEA_CLASS

//CEA header field descriptions (defined in CEA_class.cpp)
extern const char CEA_BlkHdr_dscF0[];
extern const char CEA_BlkHdr_dscF1[];
extern const char CEA_BlkHdr_dscF2[];
extern const edi_field_t CEA_BlkHdr_fields[];
extern const u32_t CEA_BlkHdr_fcount;

//unknown/invalid byte field (defined in CEA_class.cpp)
extern const edi_field_t unknown_byte_fld;
extern void insert_unk_byte(edi_field_t *p_fld, u32_t len, u32_t s_offs);

//SPM Speaker Presence Mask: shared description (defined in CEA_class.cpp)
extern const char SPM_Desc[];
//SPM Speaker Presence Mask: shared field descriptors (SAB: defined in CEA_class.cpp)
extern const edi_field_t SAB_SPM_fields[];

//-------------------------------------------------------------------------------------------------
//CEA-DBC Extended Tag Codes

//VCDB: Video Capability Data Block (DBC_EXT_VCDB = 0)
const char  cea_vcdb_cl::Name[] = "VCDB: Video Capability Data Block";
const char  cea_vcdb_cl::Desc[] =
"VCDB is used to declare display capability to overscan/underscan and the quantization range.\n";
const u32_t cea_vcdb_cl::fcount = 8;

const edi_field_t cea_vcdb_cl::fields[] = {
   //DBC extended header
   {&EDID_cl::BitF8Val, NULL, offsetof(vcdb_t, ethdr), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
    CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(vcdb_t, ethdr), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
    CEA_BlkHdr_dscF1 },
   {&EDID_cl::ByteVal, NULL, offsetof(vcdb_t, ethdr)+1, 1, 1, EF_BYTE|EF_INT|EF_RD|EF_FGR, 0, 0xFF, "ext_tag",
    CEA_BlkHdr_dscF2 },
   //VCDB data
   {&EDID_cl::BitF8Val, NULL, offsetof(vcdb_t, vdbc0), 0, 2, EF_BFLD|EF_INT, 0, 3, "S_CE01",
   "CE overscan/underscan:\n0= not supported\n1= always overscan\n2= always underscan\n3= both supported" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vcdb_t, vdbc0), 2, 2, EF_BFLD|EF_INT, 0, 3, "S_IT01",
   "IT overscan/underscan:\n0= not supported\n1= always overscan\n2= always underscan\n3= both supported" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vcdb_t, vdbc0), 4, 2, EF_BFLD|EF_INT, 0, 3, "S_PT01",
   "PT overscan/underscan:\n0= not supported\n1= always overscan\n2= always underscan\n3= both supported" },
   {&EDID_cl::BitVal, NULL, offsetof(vcdb_t, vdbc0), 6, 1, EF_BIT|EF_INT, 0, 3, "QS",
   "Quantization Range Selectable:\n1= selectable via AVI Q (RGB only), 0= no data" },
   {&EDID_cl::BitVal, NULL, offsetof(vcdb_t, vdbc0), 7, 1, EF_BIT|EF_INT, 0, 3, "QY",
   "Quantization Range:\n1= selectable via AVI YQ (YCC only), 0= no data" }
};

rcode cea_vcdb_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;
   u32_t  dlen;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if (dlen < 2) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "VCDB");

   type_id   = ID_VCDB;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//VSVD: Vendor-Specific Video Data Block (DBC_EXT_VSVD =  1)
const char  cea_vsvd_cl::Name[] = "VSVD: Vendor-Specific Video Data Block";
const char  cea_vsvd_cl::Desc[] = "Vendor-Specific Video Data Block: undefined data.";

const u32_t       cea_vsvd_cl::blkhdr_fcnt = 4;
const edi_field_t cea_vsvd_cl::blkhdr[] = {
   //DBC extended header
   {&EDID_cl::BitF8Val, NULL, offsetof(vs_vadb_t, ethdr), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
    CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(vs_vadb_t, ethdr), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
    CEA_BlkHdr_dscF1 },
   {&EDID_cl::ByteVal, NULL, offsetof(vs_vadb_t, ethdr)+1, 1, 1, EF_BYTE|EF_INT|EF_RD|EF_FGR, 0, 0xFF, "ext_tag",
    CEA_BlkHdr_dscF2 },
   //VSVD data
   {&EDID_cl::ByteStr, NULL, offsetof(vs_vadb_t, ieee_id), 0, 3, EF_STR|EF_HEX|EF_LE|EF_RD, 0, 0xFFFFFF, "ieee_id",
   "IEEE OUI (Organizationally Unique Identifier)" }
};

rcode cea_vsvd_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;
   rcode retU2;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_VSVD;

   //pre-alloc buffer for array of fields: max 31 + 1 (hdr)
   fields = (edi_field_t*) malloc( 32 * sizeof(edi_field_t) );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   retU = gen_data_layout(inst);

   retU2 = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode cea_vsvd_cl::gen_data_layout(u8_t* inst) {
   rcode        retU;
   u32_t        dlen;
   u32_t        d_offs;
   edi_field_t *p_fld;

   p_fld   = fields;

   memcpy( (void*) p_fld, blkhdr, (blkhdr_fcnt * edi_fld_sz) );
   p_fld  += blkhdr_fcnt;
   fcount  = blkhdr_fcnt;

   dlen = reinterpret_cast <ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if (dlen < 5) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "VSVD");

   dlen   -= 4; //hdr size
   fcount += dlen;
   d_offs  = 5;

   //payload data interpreted as unknown
   insert_unk_byte(p_fld, dlen, d_offs );

   RCD_RETURN_OK(retU);
}

//VDDD: VESA Display Device Data Block (DBC_EXT_VDDD = 2)
//VDDD: IFP: Interface
const char  vddd_iface_cl::Name[] = "IFP: Interface properties";
const char  vddd_iface_cl::Desc[] = "Interface properties";
const u32_t vddd_iface_cl::fcount = 6;

const edi_field_t vddd_iface_cl::fields[] = {
   //if_type
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, if_type), 4, 4, EF_BFLD|EF_INT, 0, 0xC, "if_type",
   "Interface Type Code: (num of channels/sub-type)\n"
   "0x0= analog (sub-type in 'n_lanes' field)\n"
   "0x1= LVDS (any)\n"
   "0x2= RSDS (any)\n"
   "0x3= DVI-D (1 or 2)\n"
   "0x4= DVI-I, analog section (0)\n"
   "0x5= DVI-I, digital section (1 or 2)\n"
   "0x6= HDMI-A (1)\n"
   "0x7= HDMI-B (2)\n"
   "0x8= MDDI (1 or 2)\n"
   "0x9= DisplayPort (1,2 or 4)\n"
   "0xA= IEEE-1394 (0)\n"
   "0xB= M1, analog (0)\n"
   "0xC= M1, digital (1 or 2)\n" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, if_type), 0, 4, EF_BFLD|EF_INT, 0, 4, "n_lanes",
   "Number of lanes/channels provided by the interface.\n"
   "If interface type is 'analog' (0), then 'n_lanes' is interpreted as follows:\n"
   "0= 15HD/VGA (VESA EDDC std. pinout)\n"
   "1= VESA NAVI-V (15HD)\n"
   "2= VESA NAVI-D" },
   //iface standard version and release
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, if_version), 4, 4, EF_BFLD|EF_INT, 0, 0xF, "version",
   "Interface version number" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, if_version), 0, 4, EF_BFLD|EF_INT, 0, 0xF, "release",
   "Interface release number" },
   //min/max clock frequency for each interface link/channel
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, clkfrng), 2, 6, EF_BFLD|EF_INT|EF_MHZ, 0, 63, "if_fmin",
   "Min clock frequency for each interface link/channel" },
   {&EDID_cl::VDDD_IF_MaxF, NULL, offsetof(vdddb_t, clkfrng), 0, 2, EF_BFLD|EF_INT|EF_MHZ, 0, 63, "if_fmax",
   "Max clock frequency for each interface link/channel" }
};

rcode vddd_iface_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_VDDD_IPF;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//VDDD: CPT: Content Protection
const char  vddd_cprot_cl::Name[] = "CPT: Content Protection";
const char  vddd_cprot_cl::Desc[] = "Content Protection";
const u32_t vddd_cprot_cl::fcount = 1;

const edi_field_t vddd_cprot_cl::fields[] = {
   //Content Protection support method
   {&EDID_cl::ByteVal, NULL, offsetof(vdddb_t, cont_prot), 0, 1, EF_BYTE|EF_INT, 0, 3, "ContProt",
   "Content Protection supported method:\n"
   "0= none supported\n"
   "1= HDCP\n"
   "2= DTCP\n"
   "3= DPCP (DisplayPort)" }
};

rcode vddd_cprot_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_VDDD_CPT;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//VDDD: AUD: Audio properties
const char  vddd_audio_cl::Name[] = "AUD: Audio properties";
const char  vddd_audio_cl::Desc[] = "Audio properties";
const u32_t vddd_audio_cl::fcount = 6;

const edi_field_t vddd_audio_cl::fields[] = {
   //Audio flags
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, audioflg), 0, 5, EF_BFLD|EF_RD, 0, 0x7, "resvd04",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(vdddb_t, audioflg), 5, 1, EF_BIT, 0, 1, "audio_ovr",
   "audio input override, 1= automatically override other audio inputs" },
   {&EDID_cl::BitVal, NULL, offsetof(vdddb_t, audioflg), 6, 1, EF_BIT, 0, 1, "sep_achn",
   "separate audio channel inputs: not via video interface" },
   {&EDID_cl::BitVal, NULL, offsetof(vdddb_t, audioflg), 7, 1, EF_BIT, 0, 1, "vid_achn",
   "1= audio support on video interface" },
   //Audio delay
   {&EDID_cl::VDDD_AudioDelay, NULL, offsetof(vdddb_t, audiodly), 0, 7, EF_BFLD|EF_INT|EF_MLS, 0, 254, "delay",
   "Audio delay in 2ms resolution.\n"
   "If dly> 254ms, the stored value is 254/2=127, i.e. 254ms is max,\n"
   "0= no delay compensation" },
   {&EDID_cl::BitVal, NULL, offsetof(vdddb_t, audiodly), 7, 1, EF_BIT, 0, 1, "dly_sign",
   "Audio delay sign: 1= '+', 0= '-'" }
};

rcode vddd_audio_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_VDDD_AUD;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//VDDD: DPR: Display properties
const char  vddd_disp_cl::Name[] = "DPR: Display properties";
const char  vddd_disp_cl::Desc[] = "Display properties";
const u32_t vddd_disp_cl::fcount = 24;

const edi_field_t vddd_disp_cl::fields[] = {
   //H/V pixel count
   {&EDID_cl::VDDD_HVpix_cnt, NULL, offsetof(vdddb_t, Hpix_cnt), 2, 6, EF_INT|EF_PIX, 1, 0x10000, "Hpix_cnt",
   "Count of pixels in horizontal direction: 1-65536" },
   {&EDID_cl::VDDD_HVpix_cnt, NULL, offsetof(vdddb_t, Vpix_cnt), 0, 2, EF_INT|EF_PIX, 1, 0x10000, "Vpix_cnt",
   "Count of pixels in vertical direction: 1-65536" },
   //Aspect Ratio
   {&EDID_cl::VDDD_AspRatio, NULL, offsetof(vdddb_t, aspect), 0, 1, EF_FLT|EF_NI, 0, 0xFF, "AspRatio",
   "Aspect ratio: stored value: 100*((long_axis/short_axis)-1)" },
   //Scan Direction, Position of "zero" pixel, Display Rotation, Orientation
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, scanrot), 0, 2, EF_BFLD|EF_INT, 0, 3, "scan_dir",
   "Scan Direction:\n"
   "0= undefined\n"
   "1= 'fast' along long axis, 'slow' along short axis\n"
   "2= 'fast' along short axis, 'slow' along long axis\n"
   "3= reserved" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, scanrot), 2, 2, EF_BFLD|EF_INT, 0, 3, "pix0_pos",
   "Position of 'zero' pixel:\n"
   "0= upper left corner\n"
   "1= upper right corner\n"
   "2= lower left corner\n"
   "3= lower right corner" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, scanrot), 4, 2, EF_BFLD|EF_INT, 0, 3, "rotation",
   "Possible display rotation:\n"
   "0= not possible\n"
   "1= 90 degrees clockwise\n"
   "2= 90 degrees counterclockwise\n"
   "3= both 1 & 2 possible" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, scanrot), 6, 2, EF_BFLD|EF_INT, 0, 3, "orientation",
   "Display Orientation:\n"
   "0= landscape\n"
   "1= portrait\n"
   "2= not fixed (can be rotated)\n"
   "3= undefined" },
   //sub-pixel layout code
   {&EDID_cl::ByteVal, NULL, offsetof(vdddb_t, cont_prot), 0, 1, EF_BYTE|EF_INT, 0, 0xC, "subpixlc",
   "sub-pixel layout code:\n"
   "0x0= undefined\n"
   "0x1= RGB V-stripes\n"
   "0x2= RGB H-stripes\n"
   "0x3= RGB V-stripes, primary ordering matches chromaticity info in base EDID\n"
   "0x4= RGB H-stripes, primary ordering matches chromaticity info in base EDID\n"
   "0x5= 2x2 quad: red@top-left, blue@btm-right, remaining 2pix are green\n"
   "0x6= 2x2 quad: red@btm-left, blue@top-right, remaining 2pix are green\n"
   "0x7= delta (triad)\n"
   "0x8= mosaic\n"
   "0x9= 2x2 quad: 3 RGB + 1 extra color (*)\n"
   "0xA= 3 RGB + 2 extra colors below or above the RGB set (*)\n"
   "0xB= 3 RGB + 3 extra colors below or above the RGB set (*)\n"
   "0xC= Clairvoyante, Inc. PenTile Matrix(TM)\n\n"
   "(*) Additional sub-pixel's colors should be described in Additional Chromaticity Coordinates"
   "srtucture of this block (DDDB Std, section 2.16)" },
   //horizontal/vertical pixel pitch
   {&EDID_cl::VDDD_HVpx_pitch, NULL, offsetof(vdddb_t, Hpix_pitch), 0, 1, EF_FLT|EF_NI|EF_MM, 0, 0xFF, "Hpix_pitch",
   "horizontal pixel pitch in 0.01mm (0.00 - 2.55)" },
   {&EDID_cl::VDDD_HVpx_pitch, NULL, offsetof(vdddb_t, Vpix_pitch), 0, 1, EF_FLT|EF_NI|EF_MM, 0, 0xFF, "Vpix_pitch",
   "vertical pixel pitch in 0.01mm (0.00 - 2.55)" },
   //miscellaneous display caps.
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, misccaps), 0, 3, EF_BFLD|EF_RD, 0, 0x7, "resvd02",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(vdddb_t, misccaps), 3, 1, EF_BIT, 0, 1, "deinterlacing",
   "deinterlacing, 1= possible" },
   {&EDID_cl::BitVal, NULL, offsetof(vdddb_t, misccaps), 4, 1, EF_BIT, 0, 1, "ovrdrive",
   "overdrive not recommended, 1= src 'should' not overdrive the signal by default" },
   {&EDID_cl::BitVal, NULL, offsetof(vdddb_t, misccaps), 5, 1, EF_BIT, 0, 1, "d_drive",
   "direct-drive, 0= no direct-drive" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, misccaps), 6, 2, EF_BFLD, 0, 0x7, "dither",
   "dithering, 0=no, 1=spatial, 2=temporal, 3=both" },
   //Frame rate and mode conversion
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, frm_rcnv), 0, 6, EF_BFLD|EF_INT, 0, 0x3F, "frm_delta",
   "Frame rate delta range:\n"
   "max FPS deviation from default frame rate (+/- 63 FPS)" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, frm_rcnv), 6, 2, EF_BFLD|EF_INT, 0, 3, "conv_mode",
   "available conversion mode:\n"
   "0= none\n"
   "1= single buffer\n"
   "2= double buffer\n"
   "3= other, f.e. interframe interpolation" },
   {&EDID_cl::ByteVal, NULL, offsetof(vdddb_t, frm_rcnv), 1, 1, EF_BYTE|EF_INT, 0, 0xFF, "frm_rate",
   "default frame rate (FPS)" },
   //Color bit depth
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, colbitdpth), 0, 4, EF_BFLD|EF_INT, 0, 0xF, "cbd_dev",
   "color bit depth: display device: value=cbd-1 (1-16)" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, colbitdpth), 4, 4, EF_BFLD|EF_INT, 0, 0xF, "cbd_iface",
   "color bit depth: interface: value=cbd-1 (1-16)" },
   //Display response time
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, resp_time), 0, 7, EF_BFLD|EF_INT|EF_MLS, 0, 0x7F, "resp_time",
   "Display response time in milliseconds, max 126. 127 means greater than 126, but unspecified" },
   {&EDID_cl::BitVal, NULL, offsetof(vdddb_t, resp_time), 7, 1, EF_BIT, 0, 1, "resp_type",
   "Display response time type:\n"
   "1=black-to-white\n"
   "0=white-to-black" },
   //Overscan
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, overscan), 0, 4, EF_BFLD|EF_INT|EF_PCT, 0, 0xF, "V_overscan",
   "Vertical overscan, 0-15%, 0= no overscan (but doesn't mean 100% image match)" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, overscan), 4, 4, EF_BFLD|EF_INT|EF_PCT, 0, 0xF, "H_overscan",
   "Horizontal overscan, 0-15%, 0= no overscan (but doesn't mean 100% image match)" }
};

rcode vddd_disp_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_VDDD_DPR;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//VDDD: CXY: Additional Chromaticity coords
const char  vddd_cxy_cl::Name[] = "CXY: Additional Chromaticity coords";
const char  vddd_cxy_cl::Desc[] = "Chromaticity coords for additional primary color sub-pixels";
const u32_t vddd_cxy_cl::fcount = 8;

const edi_field_t vddd_cxy_cl::fields[] = {
   //Additional Chromaticity Coordinates
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, addchrxy)+1, 0, 2, EF_BFLD|EF_INT, 0, 0xF, "n_colors",
   "Number of additional chromaticity coordinates for primary colors 4-6\n"
   "Standard RGB primary colors are numbered 1-3 and described in base EDID structure" },
   {&EDID_cl::BitF8Val, NULL, offsetof(vdddb_t, addchrxy)+1, 2, 2, EF_BFLD|EF_INT, 0, 0xF, "resvd32",
   "reserved (0)" },
   {&EDID_cl::CHredX, NULL, offsetof(vdddb_t, addchrxy), 0, 10, EF_BFLD|EF_FLT|EF_NI, 0, 1, "col4_x", "" },
   {&EDID_cl::CHredY, NULL, offsetof(vdddb_t, addchrxy), 0, 10, EF_BFLD|EF_FLT|EF_NI, 0, 1, "col4_y", "" },
   {&EDID_cl::CHgrnX, NULL, offsetof(vdddb_t, addchrxy), 0, 10, EF_BFLD|EF_FLT|EF_NI, 0, 1, "col5_x", "" },
   {&EDID_cl::CHgrnY, NULL, offsetof(vdddb_t, addchrxy), 0, 10, EF_BFLD|EF_FLT|EF_NI, 0, 1, "col5_y", "" },
   {&EDID_cl::CHbluX, NULL, offsetof(vdddb_t, addchrxy), 0, 10, EF_BFLD|EF_FLT|EF_NI, 0, 1, "col6_x", "" },
   {&EDID_cl::CHbluY, NULL, offsetof(vdddb_t, addchrxy), 0, 10, EF_BFLD|EF_FLT|EF_NI, 0, 1, "col6_y", "" }
};

rcode vddd_cxy_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_VDDD_CXY;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//VDDD: VESA Display Device Data Block: base class
const char  cea_vddd_cl::Name[] = "VDDD: VESA Display Device Data Block";
const char  cea_vddd_cl::Desc[] = "Additional information about display device";

rcode cea_vddd_cl::init(u8_t* inst, u32_t orflags) {
   rcode       retU;
   u32_t       dlen;
   edi_grp_cl* pgrp;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if (dlen != 0x1F) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "VDDD");

   type_id   = ID_VDDD;

   RCD_SET_OK(retU);

   //create and init sub-groups: 5: IFP, CPT, AUD, DPR, CXY
   subgroups.Alloc(5);

   //NOTE: vdddb_t contains the block header, so the inst ptr is the same for all sub-groups

   //IFP
   pgrp = new vddd_iface_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);
   //CPT
   pgrp = new vddd_cprot_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);
   //AUD
   pgrp = new vddd_audio_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);
   //DPR
   pgrp = new vddd_disp_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);
   //ACXY
   pgrp = new vddd_cxy_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);

   retU = init_fields(&CEA_BlkHdr_fields[0], inst, CEA_BlkHdr_fcount, orflags);
   return retU;
}

rcode EDID_cl::VDDD_IF_MaxF(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode   retU;
   u8_t   *inst;

   inst = getValPtr(p_field);
   if (inst == NULL) {
      RCD_RETURN_FAULT(retU);
   }

   if (op == OP_READ) {
      u32_t  tmpv;
      tmpv   = reinterpret_cast<clkfrng_t*> (inst)->fmax_2msb;
      ival   = reinterpret_cast<clkfrng_t*> (inst)->fmax_8lsb;
      tmpv <<= 8;
      ival  |= tmpv; //2msb

      sval << ival;
      RCD_SET_OK(retU);
   } else {
      ulong  utmp;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         retU = getStrUint(sval, 10, p_field->field.minv, p_field->field.maxv, utmp);
         if (! RCD_IS_OK(retU)) return retU;
      } else if (op == OP_WRINT) {
         utmp = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }
      {
         u32_t  tmpv;
         utmp &= 0x3FF;

         tmpv  = utmp >> 8; //2msb
         utmp &= 0xFF;      //8lsb

         reinterpret_cast<clkfrng_t*> (inst)->fmax_2msb = tmpv;
         reinterpret_cast<clkfrng_t*> (inst)->fmax_8lsb = utmp;
      }
   }
   return retU;
}

rcode EDID_cl::VDDD_HVpix_cnt(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode   retU;
   u16_t  *inst;

   inst = (u16_t*) getValPtr(p_field);
   if (inst == NULL) {
      RCD_RETURN_FAULT(retU);
   }

   if (op == OP_READ) {
      ival   = *inst;
      ival  += 1; //stored value is pix_count-1

      sval << ival;
      RCD_SET_OK(retU);
   } else {
      ulong  utmp;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         retU = getStrUint(sval, 10, p_field->field.minv, p_field->field.maxv, utmp);
         if (! RCD_IS_OK(retU)) return retU;
      } else if (op == OP_WRINT) {
         utmp = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }

      utmp -= 1;
      if (utmp > 0xFFFF) RCD_RETURN_FAULT(retU); //from ival
      *inst = utmp;
   }
   return retU;
}

rcode EDID_cl::VDDD_AspRatio(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode  retU;
   u8_t  *inst;

   inst = getValPtr(p_field);
   if (inst == NULL) RCD_RETURN_FAULT(retU);

   if (op == OP_READ) {
      double  dval;
      ival  = *inst;
      dval  = ival;
      dval /= 100.0;
      dval += 1.0;

      if (sval.Printf(_("%.03f"), dval) < 0) {
         RCD_SET_FAULT(retU);
      } else {
         RCD_SET_OK(retU);
      }
   } else {
      u32_t  utmp = 0;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         double  dval;
         retU = getStrDouble(sval, 1.0, 3.55, dval);
         if (! RCD_IS_OK(retU)) return retU;
         dval -= 1.0;
         dval *= 100.0;
         ival = dval;
      } else if (op == OP_WRINT) {
         ival &= 0xFF;
         utmp  = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }

      *inst = utmp;
   }
   return retU;
}

rcode EDID_cl::VDDD_HVpx_pitch(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode  retU;
   u8_t  *inst;

   inst = getValPtr(p_field);
   if (inst == NULL) RCD_RETURN_FAULT(retU);

   if (op == OP_READ) {
      double  dval;
      ival  = *inst;
      dval  = ival;
      dval /= 100.0;

      if (sval.Printf(_("%.03f"), dval) < 0) {
         RCD_SET_FAULT(retU);
      } else {
         RCD_SET_OK(retU);
      }
   } else {
      u32_t  utmp = 0;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         double  dval;
         retU = getStrDouble(sval, 0.0, 2.55, dval);
         if (! RCD_IS_OK(retU)) return retU;
         dval *= 100.0;
         ival = dval;
      } else if (op == OP_WRINT) {
         ival &= 0xFF;
         utmp  = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }

      *inst = utmp;
   }
   return retU;
}

rcode EDID_cl::VDDD_AudioDelay(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode   retU;
   u8_t   *inst;

   inst = getValPtr(p_field);
   if (inst == NULL) {
      RCD_RETURN_FAULT(retU);
   }

   if (op == OP_READ) {
      ival   = *inst;
      ival  &= 0x7F;
      ival <<= 1;   //2ms resolution

      sval << ival;
      RCD_SET_OK(retU);
   } else {
      ulong  utmp;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         retU = getStrUint(sval, 10, p_field->field.minv, p_field->field.maxv, utmp);
         if (! RCD_IS_OK(retU)) return retU;
      } else if (op == OP_WRINT) {
         utmp = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }

      utmp >>= 1;
      utmp  &= 0x7F;
      *inst  = utmp;
   }
   return retU;
}

//VVTB: VESA Video Timing Block Extension (DBC_EXT_VVTB = 3) BUG!

//CLDB: Colorimetry Data Block (DBC_EXT_CLDB = 5)
const char  cea_cldb_cl::Name[] = "CLDB: Colorimetry Data Block";
const char  cea_cldb_cl::Desc[] = "The Colorimetry Data Block indicates support of specific extended colorimetry standards";
const u32_t cea_cldb_cl::fcount = 14;

const edi_field_t cea_cldb_cl::fields[] = {
   //DBC extended header
   {&EDID_cl::BitF8Val, NULL, offsetof(cldb_t, ethdr), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
    CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(cldb_t, ethdr), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
    CEA_BlkHdr_dscF1 },
   {&EDID_cl::ByteVal, NULL, offsetof(cldb_t, ethdr)+1, 1, 1, EF_BYTE|EF_INT|EF_RD|EF_FGR, 0, 0xFF, "ext_tag",
    CEA_BlkHdr_dscF2 },
    //byte2
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb2), 0, 1, EF_BIT, 0, 1, "xvYCC601",
   "Standard Definition Colorimetry based on IEC 61966-2-4" },
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb2), 1, 1, EF_BIT, 0, 1, "xvYCC709",
   "High Definition Colorimetry based on IEC 61966-2-4" },
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb2), 2, 1, EF_BIT, 0, 1, "sYCC601",
   "Colorimetry based on IEC 61966-2-1/Amendment 1" },
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb2), 3, 1, EF_BIT, 0, 1, "opYCC601",
   "Colorimetry based on IEC 61966-2-5, Annex A" },
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb2), 4, 1, EF_BIT, 0, 1, "opRGB",
   "Colorimetry based on IEC 61966-2-5" },
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb2), 5, 1, EF_BIT, 0, 1, "BT2020cYCC",
   "Colorimetry based on ITU-R BT.2020, Y’cC’BCC’RC" },
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb2), 6, 1, EF_BIT, 0, 1, "BT2020YCC",
   "Colorimetry based on ITU-R BT.2020, Y’C’BC’R" },
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb2), 7, 1, EF_BIT, 0, 1, "BT2020RGB",
   "Colorimetry based on ITU-R BT.2020, R’G’B’" },
   //byte3
   {&EDID_cl::BitF8Val, NULL, offsetof(cldb_t, cldb3), 0, 4, EF_BFLD|EF_INT|EF_RD, 0, 0x07, "MD03",
    "MD0-3: Designated for future gamut-related metadata."
    "As yet undefined, this metadata is carried in an interface-specific way." },
   {&EDID_cl::BitF8Val, NULL, offsetof(cldb_t, cldb3), 4, 3, EF_BFLD|EF_INT|EF_RD, 0, 0x07, "resvd46",
    "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(cldb_t, cldb3), 7, 1, EF_BIT, 0, 1, "DCI-P3",
   "Colorimetry based on DCI-P3" }
};

rcode cea_cldb_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;
   u32_t  dlen;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if (dlen != 3) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "CLDB");

   type_id   = ID_CLDB;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//HDRS: HDR Static Metadata Data Block (DBC_EXT_HDRS = 6)
const char  cea_hdrs_cl::Name[] = "HDRS: HDR Static Metadata Data Block";
const char  cea_hdrs_cl::Desc[] = "Indicates the HDR capabilities of the Sink.";
const u32_t cea_hdrs_cl::fcount = 15;

const edi_field_t cea_hdrs_cl::fields[] = {
   //DBC extended header
   {&EDID_cl::BitF8Val, NULL, offsetof(hdrs_t, ethdr), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
    CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(hdrs_t, ethdr), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
    CEA_BlkHdr_dscF1 },
   {&EDID_cl::ByteVal, NULL, offsetof(hdrs_t, ethdr)+1, 1, 1, EF_BYTE|EF_INT|EF_RD|EF_FGR, 0, 0xFF, "ext_tag",
    CEA_BlkHdr_dscF2 },
    //byte2
   {&EDID_cl::BitVal, NULL, offsetof(hdrs_t, hdrs2), 0, 1, EF_BIT, 0, 1, "ET_0",
   "Traditional gamma - SDR Luminance Range" },
   {&EDID_cl::BitVal, NULL, offsetof(hdrs_t, hdrs2), 1, 1, EF_BIT, 0, 1, "ET_1",
   "Traditional gamma - HDR Luminance Range" },
   {&EDID_cl::BitVal, NULL, offsetof(hdrs_t, hdrs2), 2, 1, EF_BIT, 0, 1, "ET_2",
   "SMPTE ST 2084" },
   {&EDID_cl::BitVal, NULL, offsetof(hdrs_t, hdrs2), 3, 1, EF_BIT, 0, 1, "ET_3",
   "Hybrid Log-Gamma (HLG) based on Recommendation ITU-R BT.2100-0" },
   {&EDID_cl::BitVal, NULL, offsetof(hdrs_t, hdrs2), 4, 1, EF_BIT|EF_RD, 0, 1, "ET_4",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(hdrs_t, hdrs2), 5, 1, EF_BIT|EF_RD, 0, 1, "ET_5",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(hdrs_t, hdrs2), 6, 2, EF_BIT|EF_RD, 0, 1, "ET6-7",
   "reserved (0)" },
   //byte3
   {&EDID_cl::BitVal, NULL, offsetof(hdrs_t, hdrs3), 0, 1, EF_BIT|EF_RD, 0, 1, "SM_0",
   "Static Metadata Type 1" },
   {&EDID_cl::BitF8Val, NULL, offsetof(hdrs_t, hdrs3), 1, 7, EF_BFLD|EF_INT|EF_RD, 0, 1, "SM1-7",
   "reserved (0)" },
   //byte4,5,6
   {&EDID_cl::ByteVal, NULL, offsetof(hdrs_t, max_lum), 0, 1, EF_BYTE|EF_INT|EF_RD, 0, 0xFF, "max_lum",
   "Desired Content Max Luminance data (8 bits)" },
   {&EDID_cl::ByteVal, NULL, offsetof(hdrs_t, avg_lum), 0, 1, EF_BYTE|EF_INT|EF_RD, 0, 0xFF, "avg_lum",
   "Desired Content Max Frame-average Luminance data (8 bits)" },
   {&EDID_cl::ByteVal, NULL, offsetof(hdrs_t, min_lum), 0, 1, EF_BYTE|EF_INT|EF_RD, 0, 0xFF, "min_lum",
   "Desired Content Min Luminance data (8 bits)" }
};

rcode cea_hdrs_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;
   u32_t  dlen;
   u32_t  fld_offs;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if ((dlen < 3) || (dlen > 6)) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "HDRS");

   type_id   = ID_HDRS;


   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   if (! RCD_IS_OK(retU)) return retU;

   //Mark unused fields:
   fld_offs  = fcount;
   fld_offs -= (6 - dlen); //for dlen==3 -> max_lum @ offs 12

   for (int fld=dlen; fld<6; ++fld) {
      edi_field_t *p_fld;
      p_fld = reinterpret_cast <edi_field_t*> ( FieldsA.Item(fld_offs) );
      p_fld->flags |= EF_NU;
      fld_offs     ++ ;
   }

   return retU;
}

//HDRD: HDR Dynamic Metadata Data Block (DBC_EXT_HDRD = 7)

//HDRD: HDR Dynamic Metadata sub-group
const char  hdrd_mtd_cl::Name[] = "MTD: HDR Dynamic Metadata";
const char  hdrd_mtd_cl::Desc[] = "HDR Dynamic Metadata";

const u32_t       hdrd_mtd_cl::mtd_hdr_fcnt124 = 4;
const u32_t       hdrd_mtd_cl::mtd_hdr_fcnt3   = 2;
const edi_field_t hdrd_mtd_cl::mtd_hdr[] = {
   {&EDID_cl::ByteVal, NULL, offsetof(hdrd_mtd_t, mtd_len), 0, 1, EF_BYTE|EF_INT|EF_RD|EF_FGR, 0, (30-3), "length",
   "Length of the metadata block" },
   {&EDID_cl::HDRD_mtd_type, NULL, offsetof(hdrd_mtd_t, mtd_type), 0, 1, EF_HEX|EF_RD|EF_FGR, 0x1, 0x4, "mtd_type",
   "Type of metadata" },
   {&EDID_cl::BitF8Val, NULL, offsetof(hdrd_mtd_t, supp_flg), 0, 4, EF_BFLD|EF_INT|EF_RD, 0, 1, "mtd_ver",
   "HDR metadata version for metadata types 1,2,4" },
   {&EDID_cl::BitF8Val, NULL, offsetof(hdrd_mtd_t, supp_flg), 4, 4, EF_BFLD|EF_INT|EF_RD, 0, 1, "resvd47",
   "reserved (0)" }
};
const u32_t hdrd_mtd_cl::max_fcnt = 30; //TODO: verify this value (for sure it's much smaller)

rcode hdrd_mtd_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;
   rcode retU2;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_HDRD_MTD;

   //pre-alloc buffer for array of fields
   fields = (edi_field_t*) malloc( max_fcnt * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   retU = gen_data_layout(inst);

   retU2 = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode hdrd_mtd_cl::ForcedGroupRefresh() {
   rcode retU;
   rcode retU2;

   //clear fields
   memset( (void*) fields, 0, ( max_fcnt * edi_fld_sz ) );

   retU  = gen_data_layout(instance);

   retU2 = init_fields(fields, instance, fcount, 0); //note: orflags are cleared.

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode hdrd_mtd_cl::gen_data_layout(u8_t* inst) {
   rcode        retU;
   u32_t        n_bytes; //length of metadata @instance: CTA-861-G, Table 87
   u32_t        m_type;
   edi_field_t *p_fld;

   p_fld   = fields;
   n_bytes = reinterpret_cast<hdrd_mtd_t*> (inst)->mtd_len;
   m_type  = reinterpret_cast<hdrd_mtd_t*> (inst)->mtd_type;

   {
      u32_t n_fld;

      n_fld = (m_type == HDR_dmtd3) ? mtd_hdr_fcnt3 : mtd_hdr_fcnt124;

      memcpy( (void*) p_fld, mtd_hdr, (n_fld * edi_fld_sz) );
      p_fld  += n_fld;
      fcount  = n_fld;
   }

   //HDR metadata type HDR_dmtd3=0x0003: header only
   if (n_bytes == 2) {
      if (m_type != HDR_dmtd3) RCD_RETURN_FAULT(retU);
      RCD_RETURN_OK(retU);
   }

   if (n_bytes < 2) RCD_RETURN_FAULT(retU);

   //all other bytes of HDR dynamic metadata, aka "Optional Fields for HDR Dynamic Metadata"
   //are displayed as "unknown"
   //TODO: add HDR metadata parser here.
   n_bytes -= 2;
   insert_unk_byte(p_fld, n_bytes, 3); //start offs: first byte after mtd type field.


   RCD_RETURN_OK(retU);
}

//HDRD: HDR Dynamic Metadata Data Block: base class
const char  cea_hdrd_cl::Name[] = "HDRD: HDR Dynamic Metadata Data Block";
const char  cea_hdrd_cl::Desc[] = "HDR Dynamic Metadata Data Block";

rcode cea_hdrd_cl::init(u8_t* inst, u32_t orflags) {
   rcode       retU;
   u32_t       dlen;
   u32_t       totlen;
   u32_t       mtdlen;
   u8_t       *p_mtd;
   edi_grp_cl *pgrp;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_HDRD;

   dlen = reinterpret_cast<ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   //1=Ext_Tag_Code + 3=min_mtd_size for type 0x0003
   if (dlen < 4) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "HDRD");

   p_mtd   = inst;
   p_mtd  += sizeof(ethdr_t);
   totlen  = 1; //1=Ext_Tag_Code

   subgroups.Alloc((dlen -1) / 3); //-1 for Ext_Tag_Code, min mtd size is 3 for type 0x0003
   RCD_SET_OK(retU);

   do {
      pgrp = new hdrd_mtd_cl;
      if (pgrp == NULL) RCD_RETURN_FAULT(retU);

      retU = pgrp->init(p_mtd, 0);
      if (! RCD_IS_OK(retU)) return retU;

      pgrp->setAbsOffs(getAbsOffs() + totlen +1);
      subgroups.Add(pgrp);

      mtdlen  = reinterpret_cast<hdrd_mtd_t*> (inst)->mtd_len;;
      p_mtd  += mtdlen;
      totlen += mtdlen;
   } while (totlen <= dlen);

   if (totlen > dlen) RCD_RETURN_FAULT(retU);

   retU = init_fields(&CEA_BlkHdr_fields[0], inst, CEA_BlkHdr_fcount, orflags);
   return retU;
}

rcode EDID_cl::HDRD_mtd_type(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode   retU;
   u16_t  *inst;

   inst = (u16_t*) getValPtr(p_field);
   if (inst == NULL) {
      RCD_RETURN_FAULT(retU);
   }

   if (op == OP_READ) {
      ival   = *inst;

      sval.Printf(_("0x%04X"), ival);
      RCD_SET_OK(retU);
   } else {
      ulong  utmp;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         retU = getStrUint(sval, 10, p_field->field.minv, p_field->field.maxv, utmp);
         if (! RCD_IS_OK(retU)) return retU;
      } else if (op == OP_WRINT) {
         utmp = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }
      *inst = utmp;
   }
   return retU;
}

//VFPD: Video Format Preference Data Block (DBC_EXT_VFPD = 13)
const char  cea_vfpd_cl::Name[] = "VFPD: Video Format Preference Data";
const char  cea_vfpd_cl::Desc[] =
"VFPD defines order of preference for selected video formats.\n"
"Preferred formats can be referenced as an index of DTD or SVD already present in the EDID or directly, as a Video ID Code (VIC)."
"The order defined here takes precedence over all other rules defined elsewhere in the standard."
"Each byte in VFPD block contains Short Video Reference (SVR) code, first SVR is the most-preferred video format."
"The SVR codes are interpreted as follows:\n"
"0\t\t: reserved\n"
"1-127\t: VIC\n"
"128\t: reserved\n"
"129-144\t: Nth DTD block, N=SVR-128 (1..16)\n"
"145-192\t: reserved\n"
"193-253\t: VIC\n"
"254,255\t: reserved";

const edi_field_t cea_vfpd_cl::SVR_code_fld =
   {&EDID_cl::ByteVal, NULL, 0, 0, 1, EF_BYTE|EF_INT|EF_GPD, 0, 0xFF, "SVR_code", "" };

rcode cea_vfpd_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;
   rcode  retU2;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_VFPD;

   //pre-alloc buffer for array of fields (max=32 including hdr + ext_tag)
   fields = (edi_field_t*) malloc( 32 * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   retU = gen_data_layout(inst);

   retU2 = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode cea_vfpd_cl::ForcedGroupRefresh() {
   rcode retU;
   rcode retU2;

   //clear fields
   memset( (void*) fields, 0, ( 32 * edi_fld_sz ) );

   retU  = gen_data_layout(instance);

   retU2 = init_fields(fields, instance, fcount, 0); //note: orflags are cleared.

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode cea_vfpd_cl::gen_data_layout(u8_t* inst) {
   rcode        retU;
   u32_t        dlen;
   u32_t        f_offs;
   edi_field_t *p_fld;

   dlen    = reinterpret_cast<ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if (dlen < 2) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "VFPD");

   dlen   -= 1;               //num of SVRs
   f_offs  = sizeof(ethdr_t); //1st SVR
   p_fld   = fields;

   fcount  = CEA_BlkHdr_fcount;
   fcount += dlen;

   //alloc fields array
   fields = (edi_field_t*) malloc( fcount * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   //CEA-DBC-EXT header fields
   memcpy( (void*) p_fld, CEA_BlkHdr_fields, (CEA_BlkHdr_fcount * edi_fld_sz) );
   p_fld  += CEA_BlkHdr_fcount;

   for (u32_t cnt=0; cnt<dlen; ++cnt) {
      memcpy( (void*) p_fld, &SVR_code_fld, edi_fld_sz );
      p_fld->offs = f_offs;
      p_fld  += 1;
      f_offs += 1;
   }

   RCD_RETURN_OK(retU);
}

//Y42V: YCBCR 4:2:0 Video Data Block (DBC_EXT_Y42V = 14)
const char  cea_y42v_cl::Name[] = "Y42V: YCBCR 4:2:0 Video Data Block";
const char  cea_y42v_cl::Desc[] =
"List of Short Video Descriptors (SVD), for which the ONLY supported sampling mode is YCBCR 4:2:0 "
"(all other sampling modes are NOT supported: RGB, YCBCR 4:4:4, or YCBCR 4:2:2)\n"
"NOTE:\nBy default, SVDs in this block shall be less preferred than all regular "
"SVDs - this can be changed using Video Format Preference Data Block (VFPD)";

rcode cea_y42v_cl::init(u8_t* inst, u32_t orflags) {
   rcode    retU;
   u32_t    dlen;
   u8_t    *pgrp_inst;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast<ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if (dlen < 2) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "Y42V");

   type_id   = ID_Y42V;

   RCD_SET_OK(retU);
   subgroups.Alloc(dlen);

   pgrp_inst  = inst;
   pgrp_inst += sizeof(ethdr_t);
   //create and init sub-groups: SVD: Short Video Descriptor
   for (u32_t idx=0; idx<dlen; idx++) {
      edi_grp_cl* pgrp = new cea_svd_cl;
      if (pgrp == NULL) {
         RCD_SET_FAULT(retU);
         break;
      }
      retU = pgrp->init(pgrp_inst, 0);
      if (! RCD_IS_OK(retU)) break;

      pgrp->setRelOffs(pgrp_inst - inst);
      pgrp->setAbsOffs(abs_offs + (pgrp_inst - inst) );

      subgroups.Add(pgrp);
      pgrp_inst += 1;
   }
   if (! RCD_IS_OK(retU)) {
      subgroups.Clear();
      return retU;
   }

   retU = init_fields(&CEA_BlkHdr_fields[0], inst, 2, orflags);
   return retU;
}

//Y42C: YCBCR 4:2:0 Capability Map Data Block (DBC_EXT_Y42C = 15)
const char  cea_y42c_cl::Name[] = "Y42C: YCBCR 4:2:0 Capability Map Data Block";
const char  cea_y42c_cl::Desc[] =
"Bytes in this block are forming a bitmap, in which a single bit indicates whether VDB:SVD format "
"supports YCBCR 4:2:0 sampling mode (in addition to other modes: RGB, YCBCR 4:4:4, or YCBCR 4:2:2)\n"
"0= SVD does NOT support YCBCR 4:2:0\n"
"1= SVD supports YCBCR 4:2:0 in addition to other modes\n\n"
"Bit 0 in 1st byte refers to 1st SVD in VDB, bit 1 refers to 2nd SVD, and so on, "
"and 9th SVD is referenced by bit 0 in 2nd byte.";

const edi_field_t cea_y42c_cl::bitmap_fld =
   {&EDID_cl::BitVal, NULL, 0, 0, 1, EF_BIT|EF_GPD, 0, 1, NULL, "" };

rcode cea_y42c_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;
   rcode retU2;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_Y42C;

   //max 30 bytes * 8 bits
   //+ hdr + ext_tag -> max = 243 fields.
   //buffer for array of fields is allocated dynamically

   retU = gen_data_layout(inst);

   retU2 = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode cea_y42c_cl::ForcedGroupRefresh() {
   rcode retU;
   rcode retU2;

   free( fields  );
   free( svdn_ar );

   retU  = gen_data_layout(instance);

   retU2 = init_fields(fields, instance, fcount, 0); //note: orflags are cleared.

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode cea_y42c_cl::gen_data_layout(u8_t* inst) {
   rcode        retU;
   u32_t        dlen;
   u32_t        f_offs;
   u32_t        svdn;
   //u32_t        m_type;
   edi_field_t *p_fld;
   y42c_svdn_t *p_svdn;

   //m_type   = reinterpret_cast<ethdr_t*> (inst)->ehdr.hdr.tag.tag_code;
   dlen = reinterpret_cast<ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;

   if (dlen < 2) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "Y42C"); //min 1 bitmap byte

   dlen    -= 1;               //num of bitmap bytes (-ext_tag_byte)
   fcount   = (dlen << 3);     //max 30 bytes * 8 bits

   //alloc array of y42c_svdn_t: dynamic field names
   svdn_ar = (y42c_svdn_t*) malloc( fcount * sizeof(y42c_svdn_t) );
   if (NULL == svdn_ar) RCD_RETURN_FAULT(retU);

   fcount  += CEA_BlkHdr_fcount;

   //alloc fields array
   fields = (edi_field_t*) malloc( fcount * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   f_offs  = sizeof(ethdr_t); //1st bitmap byte
   p_fld   = fields;
   p_svdn  = svdn_ar;

   //CEA-DBC-EXT header fields
   memcpy( (void*) p_fld, CEA_BlkHdr_fields, (CEA_BlkHdr_fcount * edi_fld_sz) );
   p_fld  += CEA_BlkHdr_fcount;
   svdn    = 0;

   //SVD map data: 1 field per bit
   for (u32_t itb=0; itb<dlen; ++itb) {
      for (u32_t bitn=0; bitn<8; ++bitn) {

         memcpy( (void*) p_fld, &bitmap_fld, edi_fld_sz );
         //dynamic field offset:
         p_fld->offs    = f_offs;
         p_fld->fldoffs = bitn;

         //dynamic field name:
         snprintf(p_svdn->svd_num, sizeof(y42c_svdn_t), "SVD_%u", svdn);
         p_svdn->svd_num[7] = 0;
         p_fld->name        = p_svdn->svd_num;

         p_fld  += 1;
         p_svdn += 1;
         svdn   += 1;
      }

      f_offs += 1;
   }

   RCD_RETURN_OK(retU);
}

//VSAD: Vendor-Specific Audio Data Block (DBC_EXT_VSAD = 17)
const char  cea_vsad_cl::Name[] = "VSAD: Vendor-Specific Audio Data Block";
const char  cea_vsad_cl::Desc[] = "Vendor-Specific Audio Data Block: undefined data.";

const u32_t       cea_vsad_cl::blkhdr_fcnt = 4;
const edi_field_t cea_vsad_cl::blkhdr[] = {
   //DBC extended header
   {&EDID_cl::BitF8Val, NULL, offsetof(vs_vadb_t, ethdr), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
    CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(vs_vadb_t, ethdr), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
    CEA_BlkHdr_dscF1 },
   {&EDID_cl::ByteVal, NULL, offsetof(vs_vadb_t, ethdr)+1, 1, 1, EF_BYTE|EF_INT|EF_RD|EF_FGR, 0, 0xFF, "ext_tag",
    CEA_BlkHdr_dscF2 },
   //VSAD data
   {&EDID_cl::ByteStr, NULL, offsetof(vs_vadb_t, ieee_id), 0, 3, EF_STR|EF_HEX|EF_LE|EF_RD, 0, 0xFFFFFF, "ieee_id",
   "IEEE OUI (Organizationally Unique Identifier)" }
};

rcode cea_vsad_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;
   rcode retU2;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_VSAD;

   //pre-alloc buffer for array of fields: max 31 + 1 (hdr)
   fields = (edi_field_t*) malloc( 32 * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   retU = gen_data_layout(inst);

   retU2 = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode cea_vsad_cl::gen_data_layout(u8_t* inst) {
   rcode        retU;
   u32_t        dlen;
   u32_t        d_offs;
   edi_field_t *p_fld;

   p_fld   = fields;

   memcpy( (void*) p_fld, blkhdr, (blkhdr_fcnt * edi_fld_sz) );
   p_fld  += blkhdr_fcnt;
   fcount  = blkhdr_fcnt;

   dlen = reinterpret_cast <vs_vadb_t*> (inst)->ethdr.ehdr.hdr.tag.blk_len;

   if (dlen < 5) RCD_RETURN_FAULT(retU); //empty block

   dlen   -= 4; //hdr size
   fcount += dlen;
   d_offs  = 5;

   //payload data interpreted as unknown
   insert_unk_byte(p_fld, dlen, d_offs );

   RCD_RETURN_OK(retU);
}


//RMCD: Room Configuration Data Block (DBC_EXT_RMCD = 19)
//RMCD: DHDR: Data Header
const char  rmcd_hdr_cl::Name[] = "DHDR: Data Header";
const char  rmcd_hdr_cl::Desc[] = "Data Header";
const u32_t rmcd_hdr_cl::fcount = 4;

const edi_field_t rmcd_hdr_cl::fields[] = {
   {&EDID_cl::BitVal, NULL, offsetof(rmcd_t, rmcd_hdr), 5, 1, EF_BIT, 0, 1, "SLD",
   "SLD: Speaker Location Descriptor, 1= fields in SPKD (Speaker Distance) are valid (used)" },
   {&EDID_cl::BitVal, NULL, offsetof(rmcd_t, rmcd_hdr), 6, 1, EF_BIT, 0, 1, "Speaker",
   "Speaker: 1= spk_cnt is valid, 0= spk_cnt is undefined" },
   {&EDID_cl::BitVal, NULL, offsetof(rmcd_t, rmcd_hdr), 7, 1, EF_BIT, 0, 1, "Display",
   "1= fields in DSPC (Display Coordinates) are valid (used)" },
   {&EDID_cl::BitF8Val, NULL, offsetof(rmcd_t, rmcd_hdr), 0, 5, EF_BFLD, 0, 0x7, "spk_cnt",
   "Speaker Count: number of LPCM_channels -1, required for SLD=1" }
};

rcode rmcd_hdr_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_RMCD_HDR;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//RMCD: SPM: Speaker Mask
//NOTE: Desc is shared with SAB: CEA_class.cpp
const char  rmcd_spm_cl::Name[] = "DHDR: SPM: Speaker Mask";
const u32_t rmcd_spm_cl::fcount = 26-2; //SAB: skip header field descriptors

rcode rmcd_spm_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   Desc      = SPM_Desc;
   type_id   = ID_RMCD_SPM;

   //SAB_SPM_fields

   //pre-alloc buffer for array of fields (max=32 including hdr + ext_tag)
   fields = (edi_field_t*) malloc( fcount * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   { //copy field descriptors & update offsets
      u32_t        fld;
      u32_t        f_offs;
      edi_field_t *p_fld;
      p_fld  = const_cast<edi_field_t*> (SAB_SPM_fields);
      p_fld += 2; //SAB: skip header field descriptors

      //copy field descriptors 2..26 (SPM part)
      memcpy( (void*) fields, p_fld, (fcount * edi_fld_sz) );

      //SAB field descriptors: the field offsets are different: update byte offsets
      fld    = 0;
      f_offs = offsetof(rmcd_t, spk_mask);

      for (; fld<8; ++fld) { //SPM byte0
         p_fld->offs  = f_offs;
         p_fld       += 1;
      }
      f_offs += 1;

      for (; fld<16; ++fld) { //SPM byte1
         p_fld->offs  = f_offs;
         p_fld       += 1;
      }
      f_offs += 1;

      for (; fld<fcount; ++fld) { //SPM byte2, fcount=24
         p_fld->offs  = f_offs;
         p_fld       += 1;
      }
   }

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//RMCD: SPKD: Speaker Distance
const char  rmcd_spkd_cl::Name[] = "SPKD: Speaker Distance";
const char  rmcd_spkd_cl::Desc[] = "Speaker Distance";
const u32_t rmcd_spkd_cl::fcount = 3;

const edi_field_t rmcd_spkd_cl::fields[] = {
   {&EDID_cl::ByteVal, NULL, offsetof(rmcd_t, spk_plpd), 0, 1, EF_BYTE|EF_INT|EF_DM, 0, 3, "Xmax",
   "X-axis distance from Primary Listening Position (PLP) in decimeters [dm]" },
   {&EDID_cl::ByteVal, NULL, offsetof(rmcd_t, spk_plpd), 1, 1, EF_BYTE|EF_INT|EF_DM, 0, 3, "Ymax",
   "Y-axis distance from Primary Listening Position (PLP) in decimeters [dm]" },
   {&EDID_cl::ByteVal, NULL, offsetof(rmcd_t, spk_plpd), 2, 1, EF_BYTE|EF_INT|EF_DM, 0, 3, "Zmax",
   "Z-axis distance from Primary Listening Position (PLP) in decimeters [dm]" }
};

rcode rmcd_spkd_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_RMCD_SPD;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//RMCD: DSPC: Display Coordinates
const char  rmcd_dspc_cl::Name[] = "DSPC: Display Coordinates";
const char  rmcd_dspc_cl::Desc[] = "Display Coordinates";
const u32_t    rmcd_dspc_cl::fcount = 3;

const edi_field_t rmcd_dspc_cl::fields[] = {
   {&EDID_cl::ByteVal, NULL, offsetof(rmcd_t, spk_plpd), 0, 1, EF_BYTE|EF_INT, 0, 3, "DisplayX",
   "X-coordinate value normalized to Xmax in SPKD (Speaker Distance)" },
   {&EDID_cl::ByteVal, NULL, offsetof(rmcd_t, spk_plpd), 1, 1, EF_BYTE|EF_INT, 0, 3, "DisplayY",
   "Y-coordinate value normalized to Ymax in SPKD (Speaker Distance)" },
   {&EDID_cl::ByteVal, NULL, offsetof(rmcd_t, spk_plpd), 2, 1, EF_BYTE|EF_INT, 0, 3, "DisplayZ",
   "Z-coordinate value normalized to Zmax in SPKD (Speaker Distance)" }
};

rcode rmcd_dspc_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_RMCD_DPC;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//RMCD: Room Configuration Data Block: base class
const char  cea_rmcd_cl::Name[] = "RMCD: Room Configuration Data Block";
const char  cea_rmcd_cl::Desc[] =
"Room Configuration Data Block (RMCD) describes playback environment, using room coordinate system.";

rcode cea_rmcd_cl::init(u8_t* inst, u32_t orflags) {
   rcode       retU;
   u32_t       dlen;
   edi_grp_cl* pgrp;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast<ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if (dlen != 11) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "RMCD");

   type_id   = ID_RMCD;

   RCD_SET_OK(retU);

   //create and init sub-groups: 4: DHDR, SPM, SPKD, DSPC
   subgroups.Alloc(4);

   //DHDR
   pgrp = new rmcd_hdr_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);
   //SPM
   pgrp = new rmcd_spm_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);
   //SPKD
   pgrp = new rmcd_spkd_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);
   //DSPC
   pgrp = new rmcd_dspc_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(inst, 0);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);

   retU = init_fields(&CEA_BlkHdr_fields[0], inst, CEA_BlkHdr_fcount, orflags);
   return retU;
}

//SLDB: Speaker Location Data Block (DBC_EXT_SLDB = 20)
//SLDB: SLOCD: Speaker Location Descriptor
const char  slocd_cl::Name[] = "SLOCD: Speaker Location Descriptor";
const char  slocd_cl::Desc[] = "Speaker Location Descriptor";

static const char SLDB_SPK_index[] =
"Speaker index 0..31, CTA-861-G: Table 34:\n"
"0x00 FL\t: Front Left\n"
"0x01 FR\t: Front Right\n"
"0x02 FC\t: Front Center\n"
"0x03 LFE1\t: LFE Low Frequency Effects 1 (subwoofer1)\n"
"0x04 BL\t: Back Left\n"
"0x05 BR\t: Back Right\n"
"0x06 FLC\t: Front Left of Center\n"
"0x07 FRC\t: Front Right of Center\n"
"0x08 BC\t: Back Center\n"
"0x09 LFE2\t: LFE Low Frequency Effects 2 (subwoofer2)\n"
"0x0A SiL\t: Side Left\n"
"0x0B SiR\t: Side Right\n"
"0x0C TpFL\t: Top Front Left\n"
"0x0D TpFR\t: Top Front Right\n"
"0x0E TpFC\t: Top Front Center\n"
"0x0F TpC\t: Top Center\n"
"0x10 TpBL\t: Top Back Left\n"
"0x11 TpBR\t: Top Back Right\n"
"0x12 TpSiL\t: Top Side Left\n"
"0x13 TpSiR\t: Top Side Right\n"
"0x14 TpBC\t: Top Back Center\n"
"0x15 BtFC\t: Bottom Front Center\n"
"0x16 BtFL\t: Bottom Front Left\n"
"0x17 BtFR\t: Bottom Front Right\n"
"0x18 FLW\t: Front Left Wide\n"
"0x19 FRW\t: Front Right Wide\n"
"0x1A LS\t: Left Surround\n"
"0x1B RS\t: Right Surround\n"
"0x1C reserved\n"
"0x1D reserved\n"
"0x1E reserved\n"
"0x1F reserved\n";

const u32_t       slocd_cl::fcount = 11;
const edi_field_t slocd_cl::fields[] = {
   //channel index, byte0
   {&EDID_cl::BitF8Val, NULL, offsetof(slocd_t, spk_id), 0, 5, EF_BFLD|EF_INT, 0, 0x1F, "Cahnnel_IDX",
   "Channel index 0..31" },
   {&EDID_cl::BitVal, NULL, offsetof(slocd_t, spk_id), 5, 1, EF_BIT, 0, 3, "Active",
   "1= channel active, 0= channel unused" },
   {&EDID_cl::BitVal, NULL, offsetof(slocd_t, spk_id), 6, 1, EF_BIT, 0, 3, "COORD",
   "1= CoordX/Y/Z fields are used, 0= CoordX/Y/Z fields are not present in SLDB" },
   {&EDID_cl::BitVal, NULL, offsetof(slocd_t, spk_id), 7, 1, EF_BIT, 0, 3, "resvd7",
   "reserved (0)" },
   //speaker index, byte1
   {&EDID_cl::BitF8Val, NULL, offsetof(slocd_t, spk_id), 0, 5, EF_BFLD|EF_INT, 0, 0x1B, "Speaker_ID",
    SLDB_SPK_index },
   {&EDID_cl::BitVal, NULL, offsetof(slocd_t, spk_id), 5, 1, EF_BIT, 0, 3, "resvd5",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(slocd_t, spk_id), 6, 1, EF_BIT, 0, 3, "resvd6",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(slocd_t, spk_id), 7, 1, EF_BIT, 0, 3, "resvd7",
   "reserved (0)" },
   //speaker position, bytes 2-4
   {&EDID_cl::ByteVal, NULL, offsetof(slocd_t, CoordX), 0, 1, EF_BYTE|EF_INT, 0, 3, "CoordX",
   "X-axis position value normalized to Xmax in RMCD: SPKD: Speaker Distance (?)" },
   {&EDID_cl::ByteVal, NULL, offsetof(slocd_t, CoordY), 1, 1, EF_BYTE|EF_INT, 0, 3, "CoordY",
   "Y-axis position value normalized to Ymax in RMCD: SPKD: Speaker Distance (?)" },
   {&EDID_cl::ByteVal, NULL, offsetof(slocd_t, CoordZ), 2, 1, EF_BYTE|EF_INT, 0, 3, "CoordZ",
   "Z-axis position value normalized to Zmax in RMCD: SPKD: Speaker Distance (?)" }
};

rcode slocd_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_SLDB_SLD;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//SLDB: Speaker Location Data Block: base class
const char  cea_sldb_cl::Name[] = "SLDB: Speaker Location Data Block";
const char  cea_sldb_cl::Desc[] = "Speaker Location Data Block";

rcode cea_sldb_cl::init(u8_t* inst, u32_t orflags) {
   rcode       retU;
   u32_t       dlen;
   u8_t       *p_inst;
   edi_grp_cl *pgrp;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast<ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   if (dlen < 6) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "SLDB");
   //dlen ext tag code + min. 1 Speaker Location Descriptor
   dlen -= 1; //ext tag code

   if ((dlen % sizeof(slocd_t)) != 0) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "SLDB");

   type_id  = ID_SLDB;

   dlen    /= sizeof(slocd_t);
   p_inst   = inst;
   p_inst  += sizeof(ethdr_t);

   for (u32_t dcnt=0; dcnt<dlen; ++dcnt) {
      //SLOCD
      pgrp = new slocd_cl;
      if (pgrp == NULL) RCD_RETURN_FAULT(retU);
      retU = pgrp->init(p_inst, 0);
      if (! RCD_IS_OK(retU)) return retU;
      pgrp->setAbsOffs(getAbsOffs());
      subgroups.Add(pgrp);

      p_inst += sizeof(slocd_t);
   }

   retU = init_fields(&CEA_BlkHdr_fields[0], inst, CEA_BlkHdr_fcount, orflags);
   return retU;
}

//IFDB: InfoFrame Data Block (DBC_EXT_IFDB = 32)
//IFDB: IFPD: InfoFrame Processing Descriptor Header
const char  ifdb_ifpdh_cl::Name[] = "IFPD: InfoFrame Processing Descriptor";
const char  ifdb_ifpdh_cl::Desc[] = "InfoFrame Processing Descriptor";
const u32_t ifdb_ifpdh_cl::fcount = 3;

const edi_field_t ifdb_ifpdh_cl::fields[] = {
   {&EDID_cl::BitF8Val, NULL, offsetof(ifdb_t, ifpdh), 0, 5, EF_BFLD|EF_INT|EF_RD, 0, 0x1F, "resvd04",
    "reserved (0)" },
   {&EDID_cl::BitF8Val, NULL, offsetof(ifdb_t, ifpdh), 5, 3, EF_BFLD|EF_INT, 0, 7, "blk_len",
    "Block length: num of bytes following the 'n_VSIFs' byte."
    "For CTA-861-G the payload length should be zero." },
   {&EDID_cl::ByteVal, NULL, offsetof(ifdb_t, ifpdh), 1, 1, EF_BYTE|EF_INT, 0, 0xFF, "n_VSIFs",
   "num of additional Vendor-Specific InfoFrames (VSIFs) that can be received simultaneously."
   "The number is encoded as (n_VSIFs - 1), so 0 means 1." }
};

rcode ifdb_ifpdh_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_IFDB_IPD;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//IFDB: SIFD: Short InfoFrame Descriptor, InfoFrame Type Code != 0x00, 0x01
const char  ifdb_sifd_cl::Name[] = "SIFD: Short InfoFrame Descriptor";
const char  ifdb_sifd_cl::Desc[] = "Short InfoFrame Descriptor";

const u32_t       ifdb_sifd_cl::blkhdr_fcnt = 2;
const edi_field_t ifdb_sifd_cl::blkhdr[] = {
   {&EDID_cl::BitF8Val, NULL, 0, 0, 5, EF_BFLD|EF_INT, 2, 0x1F, "ift_code",
    "InfoFrame Type Code, values 0x00, 0x01 reserved for other types of descriptors." },
   {&EDID_cl::BitF8Val, NULL, 0, 5, 3, EF_BFLD|EF_INT, 0, 7, "blk_len",
    "Block length: num of bytes following the header byte." }
};

rcode ifdb_sifd_cl::init(u8_t* inst, u32_t orflags) {
   rcode        retU;
   u32_t        g_len;
   edi_field_t *p_fld;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_IFDB_SIF;

   //block length
   g_len = reinterpret_cast <sifdh_t*> (inst)->blk_len;

   //pre-alloc buffer for array of fields: blkhdr_fcnt + g_len
   fields = (edi_field_t*) malloc( (g_len + blkhdr_fcnt) * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   p_fld   = fields;

   memcpy( (void*) p_fld, blkhdr, (blkhdr_fcnt * edi_fld_sz) );
   p_fld  += blkhdr_fcnt;
   fcount  = blkhdr_fcnt;
   fcount += g_len;

   //payload data interpreted as unknown
   insert_unk_byte(p_fld, g_len, sizeof(sifdh_t) );

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//IFDB: VSIFD: Short Vendor-Specific InfoFrame Descriptor, InfoFrame Type Code = 0x01
const char  ifdb_vsifd_cl::Name[] = "VSIFD: Short Vendor-Specific InfoFrame Descriptor";
const char  ifdb_vsifd_cl::Desc[] = "Short Vendor-Specific InfoFrame Descriptor";

const u32_t       ifdb_vsifd_cl::blkhdr_fcnt = 3;
const edi_field_t ifdb_vsifd_cl::blkhdr[] = {
   {&EDID_cl::BitF8Val, NULL, offsetof(svsifd_t, ifhdr), 0, 5, EF_BFLD|EF_INT, 2, 0x1F, "ift_code",
    "InfoFrame Type Code, values 0x00, 0x01 reserved for other types of descriptors." },
   {&EDID_cl::BitF8Val, NULL, offsetof(svsifd_t, ifhdr), 5, 3, EF_BFLD|EF_INT, 0, 7, "blk_len",
    "Block length: num of bytes following the header byte." },
   //IEEE OUI
   {&EDID_cl::ByteStr, NULL, offsetof(vs_vadb_t, ieee_id), 0, 3, EF_STR|EF_HEX|EF_LE|EF_RD, 0, 0xFFFFFF, "ieee_id",
   "IEEE OUI (Organizationally Unique Identifier)" }
};

rcode ifdb_vsifd_cl::init(u8_t* inst, u32_t orflags) {
   rcode        retU;
   u32_t        g_len;
   edi_field_t *p_fld;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_IFDB_VSD;

   //block length
   g_len = reinterpret_cast <svsifd_t*> (inst)->ifhdr.blk_len;

   //pre-alloc buffer for array of fields: blkhdr_fcnt + g_len
   fields = (edi_field_t*) malloc( (g_len + blkhdr_fcnt) * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   p_fld   = fields;

   memcpy( (void*) p_fld, blkhdr, (blkhdr_fcnt * edi_fld_sz) );
   p_fld  += blkhdr_fcnt;
   fcount  = blkhdr_fcnt;
   fcount += g_len;

   //payload data interpreted as unknown
   insert_unk_byte(p_fld, g_len, sizeof(svsifd_t) );

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//IFDB: InfoFrame Data Block: base class
const char  cea_ifdb_cl::Name[] = "IFDB: InfoFrame Data Block";
const char  cea_ifdb_cl::Desc[] = "InfoFrame Data Block";

rcode cea_ifdb_cl::init(u8_t* inst, u32_t orflags) {
   rcode        retU;
   u32_t        dlen;
   u32_t        g_len;
   u32_t        g_type;
   u32_t        d_offs;
   u8_t        *g_inst;
   edi_grp_cl  *pgrp;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_IFDB;

   RCD_SET_OK(retU);

   dlen  = reinterpret_cast <ifdb_t*> (inst)->ethdr.ehdr.hdr.tag.blk_len;
   if (dlen < 3) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "IFDB");
   //1 byte ext tag code + 2 bytes IFPDH

   dlen += 1; //DBC hdr size
   g_inst   = inst;

   //create and init sub-groups: 2: IFPDH + 1 short descriptor
   subgroups.Alloc(2);

   //IFPDH
   pgrp = new ifdb_ifpdh_cl;
   if (pgrp == NULL) RCD_RETURN_FAULT(retU);
   retU = pgrp->init(g_inst, orflags);
   if (! RCD_IS_OK(retU)) return retU;
   pgrp->setAbsOffs(getAbsOffs());
   subgroups.Add(pgrp);

   d_offs  = offsetof(ifdb_t, ifpdh);
   d_offs += sizeof(ifpdh_t);
   g_inst  = inst;
   g_inst += d_offs;

   while (d_offs < dlen) {

      g_type = reinterpret_cast <sifdh_t*> (g_inst)->ift_code;
      g_len  = reinterpret_cast <sifdh_t*> (g_inst)->blk_len;


      if (g_type == 0x00) RCD_RETURN_FAULT(retU);
      if (g_type == 0x01) {
         //Short Vendor-Specific InfoFrame Descriptor
         pgrp = new ifdb_vsifd_cl;
         if (pgrp == NULL) RCD_RETURN_FAULT(retU);
         retU = pgrp->init(g_inst, orflags);
         if (! RCD_IS_OK(retU)) return retU;
         pgrp->setAbsOffs(getAbsOffs());
         subgroups.Add(pgrp);

         d_offs += sizeof(svsifd_t); //header size

      } else {
         //Short InfoFrame Descriptor
         pgrp = new ifdb_sifd_cl;
         if (pgrp == NULL) RCD_RETURN_FAULT(retU);
         retU = pgrp->init(g_inst, orflags);
         if (! RCD_IS_OK(retU)) return retU;
         pgrp->setAbsOffs(getAbsOffs());
         subgroups.Add(pgrp);

         d_offs += sizeof(sifdh_t); //header size
      }

      d_offs += g_len; //payload size
      g_inst  = inst;
      g_inst += d_offs;
   }

   if (d_offs > dlen) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "IFDB");

   retU = init_fields(&CEA_BlkHdr_fields[0], inst, CEA_BlkHdr_fcount, orflags);
   return retU;
}

//UNK-ET: Unknown Data Block (Extended Tag Code)
const char  cea_unket_cl::Name[] = "UNK-ET: Unknown Data Block";
const char  cea_unket_cl::Desc[] = "Unknown Extended Tag Code";

rcode cea_unket_cl::init(u8_t* inst, u32_t orflags) {
   rcode        retU;
   u32_t        g_len;
   edi_field_t *p_fld;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   //unknown Extended Tag Code
   type_id = ID_CEA_UETC;

   //block length
   g_len   = reinterpret_cast <ethdr_t*> (inst)->ehdr.hdr.tag.blk_len;
   g_len  -= 1; //Extended Tag Code in the header

   fcount  = CEA_BlkHdr_fcount;

   //pre-alloc buffer for array of fields: hdr_fcnt + g_len
   fields = (edi_field_t*) malloc( (g_len + fcount) * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   p_fld   = fields;

   //DBC-EXT header
   memcpy( (void*) p_fld, CEA_BlkHdr_fields, (fcount * edi_fld_sz) );
   p_fld  += fcount;

   fcount += g_len;

   //payload data interpreted as unknown
   insert_unk_byte(p_fld, g_len, sizeof(ethdr_t) );

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

