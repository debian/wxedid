/***************************************************************
 * Name:      EDID_CEA_class.cpp
 * Purpose:   EDID CEA-861 extension
 * Author:    Tomasz Pawlak (tomasz.pawlak@wp.eu)
 * Created:   2014-05-08
 * Copyright: Tomasz Pawlak (C) 2014-2020
 * License:   GPLv3
 **************************************************************/

#include "debug.h"
#include "rcdunits.h"
#ifndef idCEA
   #error "CEA_class.cpp: missing unit ID"
#endif
#define RCD_UNIT idCEA
#include "returncode/rcode.h"

#include "wxedid_rcd_func.h"
#include "wxedid_rcd_scope_ptr.h"

RCD_AUTOGEN_DEFINE_UNIT

#include <stddef.h>

#define EDID_CEA_CLASS
#include "EDID_class.h"
#undef  EDID_CEA_CLASS

//CHD: CEA Header
const char  cea_hdr_cl::Name[] = "CHD: CEA-861 header";
const char  cea_hdr_cl::Desc[] = "CEA-861 header";
const u32_t cea_hdr_cl::fcount = 9;

const edi_field_t cea_hdr_cl::fields[] = {
   {&EDID_cl::ByteVal, NULL, offsetof(cea_hdr_t, ext_tag), 0, 1, EF_BYTE|EF_HEX|EF_RD, 0, 0xFF, "Extension tag",
   "Extension type: 0x02 for CEA-861" },
   {&EDID_cl::ByteVal, NULL, offsetof(cea_hdr_t, rev), 0, 1, EF_BYTE|EF_INT|EF_RD, 0, 0xFF, "Revision",
   "Extension revision" },
   {&EDID_cl::ByteVal, NULL, offsetof(cea_hdr_t, dtd_offs), 0, 1, EF_BYTE|EF_HEX|EF_RD, 0, 0xFF, "DTD offset",
   "Offset to first DTD, if dtd_offs=0 -> No DTD sections" },
   {&EDID_cl::BitF8Val, NULL, offsetof(cea_hdr_t, info_blk), 0, 4, EF_BFLD|EF_INT|EF_RD, 0, 0x0F, "num_dtd",
   "Byte3, bits 3-0: total number of the DTD sections in this extension." },
   {&EDID_cl::BitVal, NULL, offsetof(cea_hdr_t, info_blk), 4, 1, EF_BIT, 0, 1, "YCbCr 4:2:2",
   "1 = supported." },
   {&EDID_cl::BitVal, NULL, offsetof(cea_hdr_t, info_blk), 5, 1, EF_BIT, 0, 1, "YCbCr 4:4:4",
   "1 = supported." },
   {&EDID_cl::BitVal, NULL, offsetof(cea_hdr_t, info_blk), 6, 1, EF_BIT, 0, 1, "Basic Audio",
   "1 = supports basic audio." },
   {&EDID_cl::BitVal, NULL, offsetof(cea_hdr_t, info_blk), 7, 1, EF_BIT, 0, 1, "Underscan",
   "1 = supports underscan." },
   {&EDID_cl::ByteVal, NULL, 127, 0, 1, EF_BYTE|EF_HEX|EF_RD, 0, 0xFF, "checksum",
   "Block checksum: sum of all bytes mod 256 must equal zero." }
};

rcode cea_hdr_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;
   u32_t  ext_tag;
   u32_t  rev;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   ext_tag = reinterpret_cast <cea_hdr_t*> (inst)->ext_tag;
   rev     = reinterpret_cast <cea_hdr_t*> (inst)->rev;

   if (ext_tag != 0x02) { //CEA tag
      wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] CEA/CTA-861: bad header tag=0x%02X", ext_tag);
   }

   if ((rev > 3) || (rev < 1)) { //CEA revision
      wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] CEA/CTA-861: unknown revision=%u", rev);
   }

   type_id   = ID_CHD;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}


//CEA header field descriptions
extern const char CEA_BlkHdr_dscF0[];
extern const char CEA_BlkHdr_dscF1[];
extern const char CEA_BlkHdr_dscF2[];
extern const edi_field_t CEA_BlkHdr_fields[];
extern const u32_t CEA_BlkHdr_fcount;

extern const edi_field_t unknown_byte_fld;
extern void insert_unk_byte(edi_field_t *p_fld, u32_t len, u32_t s_offs);

const char CEA_BlkHdr_dscF0[] =
"Total number of bytes in the block excluding 1st header byte. "
"If Block Type Tag = 7 (extended type tag), then the header takes 2 bytes. "
"The 2nd header byte contains the extended tag value.";
const char CEA_BlkHdr_dscF1[] = "Block Type, Tag Code: \n"
"0= reserved\n"
"1= ADB: Audio Data Block\n"
"2= VDB: Video Data Block\n"
"3= VSD: Vendor Specific Data Block\n"
"4= SAB: Speaker Allocation Data Block\n"
"5= VDT: VESA Display Transfer Characteristic Data Block\n"
"6= reserved\n"
"7= EXT: Use Extended Tag\n";
const char CEA_BlkHdr_dscF2[] =
"Extended Tag Codes:\n"
"00= Video Capability Data Block\n"
"01= Vendor-Specific Video Data Block\n"
"02= VESA Display Device Data Block\n"
"03= VESA Video Timing Block Extension\n"
"04= ! Reserved for HDMI Video Data Block\n"
"05= Colorimetry Data Block\n"
"06= HDR Static Metadata Data Block\n"
"07= HDR Dynamic Metadata Data Block\n"
"08..12 ! Reserved for video-related blocks\n"
"13= Video Format Preference Data Block\n"
"14= YCBCR 4:2:0 Video Data Block\n"
"15= YCBCR 4:2:0 Capability Map Data Block\n"
"16= ! Reserved for CTA Miscellaneous Audio Fields\n"
"17= Vendor-Specific Audio Data Block\n"
"18= Reserved for HDMI Audio Data Block\n"
"19= Room Configuration Data Block\n"
"20= Speaker Location Data Block\n"
"21..31 ! Reserved for audio-related blocks\n"
"32= InfoFrame Data Block (includes one or more Short InfoFrame Descriptors)\n"
"33..255 ! Reserved";

const u32_t CEA_BlkHdr_fcount = 3;

//CEA Header fields
const edi_field_t CEA_BlkHdr_fields[] = {
   {&EDID_cl::BitF8Val, NULL, offsetof(ethdr_t, ehdr), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
    CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(ethdr_t, ehdr), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
    CEA_BlkHdr_dscF1 },
   //Extended Tag Code field is included conditionally, depending on Tag Code
   {&EDID_cl::ByteVal, NULL, offsetof(ethdr_t, ehdr)+1, 1, 1, EF_BYTE|EF_INT|EF_RD|EF_FGR, 0, 0xFF, "ext_tag",
    CEA_BlkHdr_dscF2 }
};

//unknown/invalid byte: (offset has to be modified accordingly)
const edi_field_t unknown_byte_fld =
   {&EDID_cl::ByteVal, NULL, 0, 0, 1, EF_BYTE|EF_HEX|EF_RD|EF_FGR, 0, 0xFF, "unknown", "Unknown data" };

//fallback: insert "unknown byte" field: display unknown data instead of error
void insert_unk_byte(edi_field_t *p_fld, u32_t len, u32_t s_offs) {
   for (u32_t cnt=0; cnt<len; ++cnt) {

      memcpy( (void*) p_fld, &unknown_byte_fld, sizeof(edi_field_t) );

      p_fld->offs = s_offs;

      s_offs ++ ;
      p_fld  ++ ;
   }
}


//ADB: Audio Data Block
//ADB: Audio Data Block : audio format codes : selector
const vname_map_t adb_audiofmt_map[] = {
   {0, "Reserved"},
   {1, "LPCM"},
   {2, "AC-3"},
   {3, "MPEG1 (Layers 1 and 2)"},
   {4, "MP3"},
   {5, "MPEG2"},
   {6, "AAC"},
   {7, "DTS"},
   {8, "ATRAC"},
   {9, "SACD"},
   {10, "DD+"},
   {11, "DTS-HD"},
   {12, "MLP/Dolby TrueHD"},
   {13, "DST Audio"},
   {14, "Microsoft WMA Pro"},
   {15, "ACE: Audio Coding Extension Type Code"}
};

const vmap_t ADB_audiofmt = {
   0, 16,
   adb_audiofmt_map
};

const char  cea_adb_cl::Name[] = "ADB: Audio Data Block";
const char  cea_adb_cl::Desc[] =
"Audio Data Block contains one or more 3-byte "
"Short Audio Descriptors (SADs). Each SAD details audio format, channel number, "
"and bitrate/resolution capabilities of the display";

rcode cea_adb_cl::init(u8_t* inst, u32_t orflags) {
   rcode   retU;
   u32_t   dlen;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <bhdr_t*> (inst)->tag.blk_len;
   if ( dlen < (sizeof(sad_t))     ) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "ADB");
   if ((dlen %  sizeof(sad_t)) != 0) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "ADB");

   dlen    /= sizeof(sad_t);
   type_id  = ID_ADB;

   RCD_SET_OK(retU);
   sad_t* pgrp_inst = reinterpret_cast <sad_t*> (inst+1);
   subgroups.Alloc(dlen);
   //create and init sub-groups: SAD: Short Audio Descriptor
   for (u32_t idx=0; idx<dlen; idx++) {
      edi_grp_cl* pgrp = new cea_sad_cl;
      if (pgrp == NULL) {
         RCD_SET_FAULT(retU);
         break;
      }
      retU = pgrp->init(reinterpret_cast <u8_t*> (pgrp_inst), 0);
      if (! RCD_IS_OK(retU)) break;
      pgrp->setRelOffs(reinterpret_cast <u8_t*> (pgrp_inst) - inst);
      pgrp->setAbsOffs(abs_offs + pgrp->getRelOffs() );
      subgroups.Add(pgrp);
      pgrp_inst += 1;
   }
   if (! RCD_IS_OK(retU)) {
      subgroups.Clear();
      return retU;
   }

   retU = init_fields(&CEA_BlkHdr_fields[0], inst, 2, orflags, Name, Desc);
   return retU;
}

//ADB: Audio Data Block ->
//SAD: Short Audio Descriptor
const char  cea_sad_cl::Name[] = "SAD: Short Audio Descriptor";
const char  cea_sad_cl::Desc[] =
"SAD describes audio format, channel number, "
"and sample bitrate/resolution capabilities of the display";

static const char freqSuppDsc[] = "SAD byte 1, bit flag =1: sampling frequency is supported.";
static const char sadAFC_Desc[] =
   "Audio Format Code (index):\n"
   "0  = reserved\n"
   "1  = LPCM (Linear Pulse Code Modulation)\n"
   "2  = AC-3\n"
   "3  = MPEG1 (Layers 1 and 2)\n"
   "4  = MP3\n"
   "5  = MPEG2\n"
   "6  = AAC\n"
   "7  = DTS\n"
   "8  = ATRAC\n"
   "9  = SACD\n"
   "10 = DD+\n"
   "11 = DTS-HD\n"
   "12 = MLP/Dolby TrueHD\n"
   "13 = DST Audio\n"
   "14 = Microsoft WMA Pro\n"
   "15 = Audio Coding Extension Type Code is used (ACE)";
//const u32_t cea_sad_cl::fcount = 16;

/* SAD (Short Audio Descriptor) uses different data layouts, depending
   on Audio Format Code (AFC) and Audio Coding Extension Type Code (ACE).
   All the possible data layouts are defined here, and the final SAD is
   constructed by cea_sad_cl::gen_data_layout(), which is also setting the
   cea_sad_cl::fcount variable.
*/

//SAD byte 0: AFC = 1...14
const u32_t       cea_sad_cl::byte0_afc1_14_fcnt = 3;
const edi_field_t cea_sad_cl::byte0_afc1_14[] {
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad0), 0, 3, EF_BFLD|EF_INT, 0, 8, "num_chn",
   "Bitfield of 3 bits: number of channels minus 1 -> f.e. 0 = 1 channel; 7 = 8 channels." },
   {&EDID_cl::BitF8Val, &ADB_audiofmt, offsetof(sad_t, sad0), 3, 4, EF_BFLD|EF_INT|EF_VS|EF_FGR, 0, 0x0F, "AFC",
    sadAFC_Desc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad0), 7, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" }
};
//SAD byte 0: AFC = 15 && ACE = 11,12 (MPEG-H 3D Audio, AC-4)
const u32_t       cea_sad_cl::byte0_afc15_ace11_12_fcnt = 3;
const edi_field_t cea_sad_cl::byte0_afc15_ace11_12[] {
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad0), 0, 3, EF_BFLD|EF_INT, 0, 8, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitF8Val, &ADB_audiofmt, offsetof(sad_t, sad0), 3, 4, EF_BFLD|EF_INT|EF_VS|EF_FGR, 0, 0x0F, "AFC",
    sadAFC_Desc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad0), 7, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" }
};
//SAD byte 0: AFC = 15 && ACE = 13 (L-PCM 3D Audio)
const u32_t    cea_sad_cl::byte0_afc15_ace13_fcnt = 2;
const edi_field_t cea_sad_cl::byte0_afc15_ace13[] {
   {&EDID_cl::SAD_LPCM_MC, NULL, offsetof(sad_t, sad0), 0, 3, EF_BFLD|EF_INT, 0, 32, "num_chn",
   "Set of 5 bits (scattered - not a bitfield), number of channels minus 1 - i.e. 0 -> 1 channel; "
   "7 -> 8 channels.\n"
   "Used only with L-PCM 3D Audio." },
   {&EDID_cl::BitF8Val, &ADB_audiofmt, offsetof(sad_t, sad0), 3, 4, EF_BFLD|EF_INT|EF_VS|EF_FGR, 0, 0x0F, "AFC",
    sadAFC_Desc },
};

//SAD byte 1: AFC = 1...14 || AFC = 15 && ACE = 11 (MPEG-H 3D Audio)
const u32_t    cea_sad_cl::byte1_afc1_14_ace11_fcnt = 8;
const edi_field_t cea_sad_cl::byte1_afc1_14_ace11[] = {
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 0, 1, EF_BIT, 0, 1, "sf_32kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 1, 1, EF_BIT, 0, 1, "sf_44.1kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 2, 1, EF_BIT, 0, 1, "sf_48kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 3, 1, EF_BIT, 0, 1, "sf_88.2kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 4, 1, EF_BIT, 0, 1, "sf_96kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 5, 1, EF_BIT, 0, 1, "sf_176.4kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 6, 1, EF_BIT, 0, 1, "sf_192kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 7, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" }
};
//SAD byte 1: AFC = 15 && ACE = 4,5,6,8,10
const u32_t    cea_sad_cl::byte1_afc15_ace456810_fcnt = 8;
const edi_field_t cea_sad_cl::byte1_afc15_ace456810[] = {
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 0, 1, EF_BIT, 0, 1, "sf_32kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 1, 1, EF_BIT, 0, 1, "sf_44.1kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 2, 1, EF_BIT, 0, 1, "sf_48kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 3, 1, EF_BIT, 0, 1, "sf_88.2kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 4, 1, EF_BIT, 0, 1, "sf_96kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 5, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 6, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 7, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" }
};
//SAD byte 1: //AFC = 15 && ACE = 12 (AC-4)
const u32_t    cea_sad_cl::byte1_afc15_ace12_fsmp_fcnt = 8;
const edi_field_t cea_sad_cl::byte1_afc15_ace12_fsmp[] = {
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 0, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 1, 1, EF_BIT, 0, 1, "sf_44.1kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 2, 1, EF_BIT, 0, 1, "sf_48kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 3, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 4, 1, EF_BIT, 0, 1, "sf_96kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 5, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 6, 1, EF_BIT, 0, 1, "sf_192kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 7, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" }
};
//SAD byte 1: AFC = 15 && ACE = 13 (L-PCM 3D Audio)
const u32_t    cea_sad_cl::byte1_afc15_ace13_fcnt = 8;
const edi_field_t cea_sad_cl::byte1_afc15_ace13[] = {
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 0, 1, EF_BIT, 0, 1, "sf_32kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 1, 1, EF_BIT, 0, 1, "sf_44.1kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 2, 1, EF_BIT, 0, 1, "sf_48kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 3, 1, EF_BIT, 0, 1, "sf_88.2kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 4, 1, EF_BIT, 0, 1, "sf_96kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 5, 1, EF_BIT, 0, 1, "sf_176.4kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 6, 1, EF_BIT, 0, 1, "sf_192kHz",
    freqSuppDsc },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad1), 7, 1, EF_BIT|EF_RD, 0, 1, "MC4",
   "This bit is part of 'num_chn' value for L-PCM 3D Audio, do not change it directly!." }
};

//SAD byte 2: AFC = 1: LPCM
const u32_t    cea_sad_cl::byte2_afc1_fcnt = 4;
const edi_field_t cea_sad_cl::byte2_afc1[] = {
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 0, 1, EF_BIT, 0, 1, "sample16b",
   "LPCM sample size in bits: 1=supported." },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 1, 1, EF_BIT, 0, 1, "sample20b",
   "LPCM sample size in bits: 1=supported." },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 2, 1, EF_BIT, 0, 1, "sample24b",
   "LPCM sample size in bits: 1=supported." },
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad2), 3, 5, EF_BFLD|EF_RD, 0, 0, "reserved",
   "reserved (0)" }
};
//SAD byte 2: AFC = 2...8 : bitrate
const u32_t    cea_sad_cl::byte2_afc2_8_fcnt = 1;
const edi_field_t cea_sad_cl::byte2_afc2_8[] = {
   {&EDID_cl::ByteVal, NULL, offsetof(sad_t, sad2), 0, 1, EF_BYTE|EF_INT, 0, 0xFF, "Bitrate",
   "Bitrate divided by 8 kbit/s" }
};
//SAD byte 2: AFC = 9...13: AFC dependent value
const u32_t    cea_sad_cl::byte2_afc9_13_fcnt = 1;
const edi_field_t cea_sad_cl::byte2_afc9_13[] = {
   {&EDID_cl::ByteVal, NULL, offsetof(sad_t, sad2), 0, 1, EF_BYTE|EF_HEX, 0, 0xFF, "value",
   "AFC dependent value" }
};
//SAD byte 2: AFC = 14: WMA-Pro
const u32_t    cea_sad_cl::byte2_afc14_fcnt = 2;
const edi_field_t cea_sad_cl::byte2_afc14[] = {
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad2), 0, 3, EF_BFLD|EF_RD, 0, 0, "profile",
   "WMA-Pro profile." },
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad2), 3, 5, EF_BFLD|EF_RD, 0, 0, "reserved",
   "reserved (0)" }
};
//SAD byte 2: AFC = 15, ACE = 4, 5, 6
const u32_t    cea_sad_cl::byte2_afc15_ace456_fcnt = 4;
const edi_field_t cea_sad_cl::byte2_afc15_ace456[] = {
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 0, 1, EF_BIT|EF_RD, 0, 1, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 1, 1, EF_BIT, 0, 1, "960_TL",
   "Total frame length 960 samples" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 2, 1, EF_BIT, 0, 1, "1024_TL",
   "Total frame length 1024 samples" },
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad2), 3, 5, EF_INT|EF_FGR, 1, 13, "ACE_TC",
   "Audio Coding Extension Type Code." }
};
//SAD byte 2: AFC = 15, ACE = 8 or 10
const u32_t    cea_sad_cl::byte2_afc15_ace8_10_fcnt = 4;
const edi_field_t cea_sad_cl::byte2_afc15_ace8_10[] = {
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 0, 1, EF_BIT, 0, 1, "MPS_L",
   "MPEG Surround" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 1, 1, EF_BIT, 0, 1, "960_TL",
   "Total frame length 960 samples" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 2, 1, EF_BIT, 0, 1, "1024_TL",
   "Total frame length 1024 samples" },
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad2), 3, 5, EF_INT|EF_FGR, 1, 13, "ACE_TC",
   "Audio Coding Extension Type Code." }
};
//SAD byte 2: AFC = 15, ACE = 11 (MPEG-H 3D Audio) or 12 (AC-4)
const u32_t    cea_sad_cl::byte2_afc15_ace11_12_fcnt = 2;
const edi_field_t cea_sad_cl::byte2_afc15_ace11_12[] = {
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad2), 0, 3, EF_BFLD, 0, 0, "AFC_dep_val",
   "Audio Format Code dependent value" },
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad2), 3, 5, EF_INT|EF_FGR, 1, 13, "ACE_TC",
   "Audio Coding Extension Type Code." }
};
//SAD byte 2: AFC = 15, ACE = 13 (L-PCM 3D Audio)
const u32_t    cea_sad_cl::byte2_afc15_ace13_fcnt = 4;
const edi_field_t cea_sad_cl::byte2_afc15_ace13[] = {
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 0, 1, EF_BIT, 0, 1, "s16bit",
   "16bit sample" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 1, 1, EF_BIT, 0, 1, "s20bit",
   "20bit sample" },
   {&EDID_cl::BitVal, NULL, offsetof(sad_t, sad2), 2, 1, EF_BIT, 0, 1, "s24bit",
   "24bit sample" },
   {&EDID_cl::BitF8Val, NULL, offsetof(sad_t, sad2), 3, 5, EF_INT|EF_FGR, 1, 13, "ACE_TC",
   "Audio Coding Extension Type Code." }
};

const u32_t    cea_sad_cl::max_fcnt = byte0_afc1_14_fcnt + byte1_afc1_14_ace11_fcnt + byte2_afc1_fcnt;

rcode cea_sad_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;
   rcode retU2;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_SAD;

   //pre-alloc buffer for array of fields
   fields = (edi_field_t*) malloc( max_fcnt * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   retU = gen_data_layout(inst);

   retU2 = init_fields(fields, inst, fcount, orflags, Name, Desc);

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode cea_sad_cl::ForcedGroupRefresh() {
   rcode retU;
   rcode retU2;

   //clear fields
   memset( (void*) fields, 0, ( max_fcnt * edi_fld_sz ) );

   //on error, "unknown data" layout is generated, init_fields is always called.
   retU  = gen_data_layout(instance);

   retU2 = init_fields(fields, instance, fcount, 0); //note: orflags are cleared.

   if (! RCD_IS_OK(retU)) return retU;
   return retU2;
}

rcode cea_sad_cl::gen_data_layout(u8_t* inst) {
   rcode        retU;
   edi_field_t *p_fld;
   int          AFC;
   int          ACE;

   p_fld = fields;

   AFC = reinterpret_cast <sad0_t*> (inst  )->afc1_14.audio_fmt;
   ACE = reinterpret_cast <sad2_t*> (inst+2)->afc15_ace.ace_456.ace_tc;

   { // SAD byte 0
      if (AFC < 1 ) {
         insert_unk_byte(p_fld, 3, 0);
         fcount = 3;
         RCD_RETURN_FAULT_MSG(retU, "[E] SAD: bad Audio Format Code (AFC)");
      }
      if (AFC < 15) { //AFC = 1...14
         memcpy( (void*) p_fld, byte0_afc1_14, (byte0_afc1_14_fcnt * edi_fld_sz) );
         p_fld  += byte0_afc1_14_fcnt;
         fcount  = byte0_afc1_14_fcnt;
      } else
      {  //AFC = 15 && ACE = 4-6,8,10: same as for AFC = 1...14
         if ((ACE >= 4 && 6 >= ACE) || (ACE = 8) || (ACE = 10)) {
            memcpy( (void*) p_fld, byte0_afc1_14, (byte0_afc1_14_fcnt * edi_fld_sz) );
            p_fld  += byte0_afc1_14_fcnt;
            fcount  = byte0_afc1_14_fcnt;

         } else
         //AFC = 15 && ACE = 11,12 (MPEG-H 3D Audio, AC-4)
         if ((ACE = 11) || (ACE = 12)) {
            memcpy( (void*) p_fld, byte0_afc15_ace11_12, (byte0_afc15_ace11_12_fcnt * edi_fld_sz) );
            p_fld  += byte0_afc15_ace11_12_fcnt;
            fcount  = byte0_afc15_ace11_12_fcnt;

         } else
         // AFC == 15, ACE = 13 (L-PCM 3D Audio)
         if (ACE == 13) {
            memcpy( (void*) p_fld, byte0_afc15_ace13, (byte0_afc15_ace13_fcnt * edi_fld_sz) );
            p_fld  += byte0_afc15_ace13_fcnt;
            fcount  = byte0_afc15_ace13_fcnt;
         } else {
         // AFC == 15, ACE > 13 || ACE = 9 || ACE = 7 || ACE < 4 : unknown
            insert_unk_byte(p_fld, 3, 0);
            fcount = 3;
            RCD_RETURN_FAULT_MSG(retU, "[E] SAD: bad Audio Coding Extension Type Code (ACE)");
         }
      }
   }
   { // SAD byte 1
      //AFC = 1...14 || AFC = 15 && ACE = 11 (MPEG-H 3D Audio)
      if ( (AFC < 15) || ( AFC == 15 && ACE == 11 ) ) {
         memcpy( (void*) p_fld, byte1_afc1_14_ace11, (byte1_afc1_14_ace11_fcnt * edi_fld_sz) );
         p_fld  += byte1_afc1_14_ace11_fcnt;
         fcount += byte1_afc1_14_ace11_fcnt;
      } else {
         //AFC == 15
         if ( (ACE >= 4 && 6 >= ACE) || ACE==8 || ACE==10 ) { //ACE = 4,5,6,8,10
            memcpy( (void*) p_fld, byte1_afc15_ace456810, (byte1_afc15_ace456810_fcnt * edi_fld_sz) );
            p_fld  += byte1_afc15_ace456810_fcnt;
            fcount += byte1_afc15_ace456810_fcnt;
         } else
         if ( ACE==12 ) { //ACE = 12 (AC-4)
            memcpy( (void*) p_fld, byte1_afc15_ace12_fsmp, (byte1_afc15_ace12_fsmp_fcnt * edi_fld_sz) );
            p_fld  += byte1_afc15_ace12_fsmp_fcnt;
            fcount += byte1_afc15_ace12_fsmp_fcnt;
         } else
         if ( ACE==13 ) { //ACE = 13 (L-PCM 3D Audio)
            memcpy( (void*) p_fld, byte1_afc15_ace13, (byte1_afc15_ace13_fcnt * edi_fld_sz) );
            p_fld  += byte1_afc15_ace13_fcnt;
            fcount += byte1_afc15_ace13_fcnt;
         } else {
         // AFC == 15, ACE > 13 || ACE = 9 || ACE = 7 || ACE < 4 :
         // insert "unknown byte" on error
            insert_unk_byte(p_fld, 2, 1);
            fcount += 2;
            RCD_RETURN_FAULT_MSG(retU, "[E] SAD: bad Audio Coding Extension Type Code (ACE)");
         }
      }
   }
   { // SAD byte 2
      if (AFC == 1 ) { //AFC = 1: LPCM
         memcpy( (void*) p_fld, byte2_afc1, (byte2_afc1_fcnt * edi_fld_sz) );
         p_fld  += byte2_afc1_fcnt;
         fcount += byte2_afc1_fcnt;
         RCD_RETURN_OK(retU);
      } else
      if (AFC >= 2 && 8 >= AFC ) { //AFC = 2...8 : bitrate
         memcpy( (void*) p_fld, byte2_afc2_8, (byte2_afc2_8_fcnt * edi_fld_sz) );
         p_fld  += byte2_afc2_8_fcnt;
         fcount += byte2_afc2_8_fcnt;
         RCD_RETURN_OK(retU);
      } else
      if (AFC >= 9 && 13 >= AFC ) { //AFC = 9...13: AFC dependent value
         memcpy( (void*) p_fld, byte2_afc9_13, (byte2_afc9_13_fcnt * edi_fld_sz) );
         p_fld  += byte2_afc9_13_fcnt;
         fcount += byte2_afc9_13_fcnt;
         RCD_RETURN_OK(retU);
      } else
      if (AFC == 14) { //AFC = 14: WMA-Pro
         memcpy( (void*) p_fld, byte2_afc14, (byte2_afc14_fcnt * edi_fld_sz) );
         p_fld  += byte2_afc14_fcnt;
         fcount += byte2_afc14_fcnt;
         RCD_RETURN_OK(retU);
      } else
      if (AFC == 15) {
         if ( ACE >= 4 && 6 >= ACE ) { //AFC = 15, ACE = 4, 5, 6
            memcpy( (void*) p_fld, byte2_afc15_ace456, (byte2_afc15_ace456_fcnt * edi_fld_sz) );
            p_fld  += byte2_afc15_ace456_fcnt;
            fcount += byte2_afc15_ace456_fcnt;
            RCD_RETURN_OK(retU);
         } else
         if ( ACE==8 || ACE==10 ) { //AFC = 15, ACE = 8 or 10
            memcpy( (void*) p_fld, byte2_afc15_ace8_10, (byte2_afc15_ace8_10_fcnt * edi_fld_sz) );
            p_fld  += byte2_afc15_ace8_10_fcnt;
            fcount += byte2_afc15_ace8_10_fcnt;
            RCD_RETURN_OK(retU);
         } else
         if ( ACE==11 || ACE==12 ) { //AFC = 15, ACE = 11 (MPEG-H 3D Audio) or 12 (AC-4)
            memcpy( (void*) p_fld, byte2_afc15_ace11_12, (byte2_afc15_ace11_12_fcnt * edi_fld_sz) );
            p_fld  += byte2_afc15_ace11_12_fcnt;
            fcount += byte2_afc15_ace11_12_fcnt;
            RCD_RETURN_OK(retU);
         } else
         if ( ACE==13 ) { //AFC = 15, ACE = 13 (L-PCM 3D Audio)
            memcpy( (void*) p_fld, byte2_afc15_ace13, (byte2_afc15_ace13_fcnt * edi_fld_sz) );
            p_fld  += byte2_afc15_ace13_fcnt;
            fcount += byte2_afc15_ace13_fcnt;
            RCD_RETURN_OK(retU);
         }
         // AFC == 15, ACE > 13 || ACE = 9 || ACE = 7 || ACE < 4 : unknown
      }
   }
   //unknown byte format:
   insert_unk_byte(p_fld, 1, 2);
   fcount += 1;
   RCD_RETURN_FAULT_MSG(retU, "[E] SAD: bad Audio Coding Extension Type Code (ACE)");
}

//ADB: Audio Data Block: handlers
//ADB:SAD byte0 & 1, AFC = 15 && ACE = 13 (L-PCM 3D Audio): number of channels, bits MC4..MC0
rcode EDID_cl::SAD_LPCM_MC(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode    retU;
   u8_t *inst;

   inst = getValPtr(p_field);
   if (inst == NULL) {
      RCD_RETURN_FAULT(retU);
   }

   if (op == OP_READ) {
      uint  tmpv;
      tmpv   = ((sad0_t*)  inst   )->afc15_ace13.MC3;
      ival   = ((sad1_t*) (inst+1))->fsmp_afc15_ace13.MC4;
      ival <<= 1;
      ival  |= tmpv; //bits MC4,MC3
      tmpv   = inst[0] & 0x07; //bits MC2 .. MC0
      ival <<= 3;
      ival  |= tmpv; //bits MC4 .. MC0

      sval << ival;
      RCD_SET_OK(retU);
   } else {
      ulong  utmp;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         retU = getStrUint(sval, 10, p_field->field.minv, p_field->field.maxv, utmp);
         if (! RCD_IS_OK(retU)) return retU;
      } else if (op == OP_WRINT) {
         utmp = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }
      {
         uint  tmpv;
         utmp   &= 0xFF;

         tmpv    = utmp >> 3; //bit MC3
         tmpv  <<= 7;
         ival    = inst[0]; //SAD byte 0
         ival   &= 0x78;    //clr M3, MC2-0
         ival   |= tmpv;    //bit MC3

         tmpv    = utmp & 0x07; //bits MC2 .. MC0
         ival   |= tmpv;
         inst[0] = ival; //write bits M3, MC2-0

         tmpv    = utmp >> 4; //bit MC4
         ((sad1_t*) inst+1)->fsmp_afc15_ace13.MC4 = tmpv;
      }
   }
   return retU;
}

//VDB: Video Data Block : video modes

#include "svd_vidfmt.h"

//VDB: Video Data Block
const char  cea_vdb_cl::Name[] = "VDB: Video Data Block";
const char  cea_vdb_cl::Desc[] =
"Video Data Block contains one or more 1-byte "
"Short Video Descriptors (SVD). Each SVD contains index number from supported resolutions set "
"defined in CEA/EIA standard, and a \"native\" resolution flag";

rcode cea_vdb_cl::init(u8_t* inst, u32_t orflags) {
   rcode   retU;
   u32_t   dlen;
   u8_t   *pgrp_inst;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <bhdr_t*> (inst)->tag.blk_len;
   if (dlen < 1) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "VDB");

   type_id   = ID_VDB;

   RCD_SET_OK(retU);
   subgroups.Alloc(dlen);

   pgrp_inst = (inst+1);
   //create and init sub-groups: SVD: Short Video Descriptor
   for (u32_t idx=0; idx<dlen; idx++) {
      edi_grp_cl* pgrp = new cea_svd_cl;
      if (pgrp == NULL) {
         RCD_SET_FAULT(retU); break;
      }
      retU = pgrp->init(pgrp_inst, 0);
      if (! RCD_IS_OK(retU)) break;

      pgrp->setRelOffs(pgrp_inst - inst);
      pgrp->setAbsOffs(abs_offs + (pgrp_inst - inst) );

      subgroups.Add(pgrp);
      pgrp_inst += 1;
   }
   if (! RCD_IS_OK(retU)) {
      subgroups.Clear();
      return retU;
   }

   retU = init_fields(&CEA_BlkHdr_fields[0], inst, 2, orflags, Name, Desc);
   return retU;
}

//VDB: Video Data Block ->
//SVD: Short Video Descriptor
const char  cea_svd_cl::Name[] = "SVD: Short Video Descriptor";
const char  cea_svd_cl::Desc[] =
"Short Video Descriptor: "
"contains index number of supported resolutions set "
"defined in CEA/CTA-861 standard, and a \"native\" resolution flag";

const u32_t       cea_svd_cl::fcount = 2;
const edi_field_t cea_svd_cl::fields[] = {
   {&EDID_cl::ByteVal, &CEA_vidm_map, 0, 0, 1, EF_BYTE|EF_INT|EF_VS|EF_FGR, 0, 0xFF, "VIC",
   "Video ID Code: an index referencing a table of standard resolutions/timings defined in CEA/CTA-861." },
   {&EDID_cl::BitVal, NULL, 0, 7, 1, EF_BIT|EF_RD|EF_FGR, 0, 0, "Native",
   "Before CEA-861F the bit7 means a \"native\" mode\n"
   "For CEA-861F and above, VIC value takes 8bits, interpretation:\n"
   "0\t\t: reserved\n"
   "1-64\t\t: 7bit VIC, forced NOT native mode (for backward compatibility)\n"
   "65-127\t: 8bit VIC, \"native\" bit forced to 0\n"
   "129-192\t: 7bit VIC, \"native\" bit forced to 1 (all those VICs are native)\n"
   "193-253\t: 8bit VIC, no \"native\" bit\n"
   "254,255\t: reserved" }
};

rcode cea_svd_cl::init(u8_t* inst, u32_t orflags) {
   rcode retU;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   type_id   = ID_SVD;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//VSD: Vendor Specific Data Block: handlers
rcode EDID_cl::VSD_ltncy(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode  retU;
   u8_t  *inst;

   inst = getValPtr(p_field);
   if (inst == NULL) {
      RCD_RETURN_FAULT(retU);
   }

   if (op == OP_READ) {
      ival = (inst[0] - 1) << 1; //(val-1)*2
      sval << ival;
      RCD_SET_OK(retU);
   } else {
      ulong       tmpv;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         retU = getStrUint(sval, 10, p_field->field.minv, p_field->field.maxv, tmpv);
         if (! RCD_IS_OK(retU)) return retU;
      } else if (op == OP_WRINT) {
         tmpv = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }

      tmpv = (tmpv >> 1)+1;
      inst[0] = (tmpv & 0xFF);
   }
   return retU;
}

rcode EDID_cl::VSD_MaxTMDS(u32_t op, wxString& sval, u32_t& ival, edi_dynfld_t* p_field) {
   rcode  retU;
   u8_t  *inst;

   inst = getValPtr(p_field);
   if (inst == NULL) {
      RCD_RETURN_FAULT(retU);
   }

   if (op == OP_READ) {
      ival = (inst[0] * 5);
      sval << ival;
      RCD_SET_OK(retU);
   } else {
      ulong       tmpv;
      RCD_SET_FAULT(retU);

      if (op == OP_WRSTR) {
         retU = getStrUint(sval, 10, p_field->field.minv, p_field->field.maxv, tmpv);
         if (! RCD_IS_OK(retU)) return retU;
      } else if (op == OP_WRINT) {
         tmpv = ival;
         RCD_SET_OK(retU);
      } else {
         RCD_RETURN_FAULT(retU); //wrong op code
      }

      tmpv = (tmpv / 5);
      inst[0] = (tmpv & 0xFF);
   }
   return retU;
}

//VSD: Vendor Specific Data Block
const char  cea_vsd_cl::Name[] = "VSD: Vendor Specific Data Block";
const char  cea_vsd_cl::Desc[] =
"Vendor Specific Data Block is required to contain the "
"following fields: \n1. Vendor's IEEE-id 24-bit LE registration number (For HDMI, it is always "
"00-0C-03 HDMI Licensing, LLC).\n2. Source Physical Address (16bit LE). The source physical "
"address provides the CEC physical address for upstream CEC devices.\n"
"The rest of fields can be anything the vendor considers worthy of inclusion in this VSD. "
"HDMI 1.3a specifies some requirements for those data and this is what wxEDID uses as reference.";

const u32_t       cea_vsd_cl::fcount = 20;
const edi_field_t cea_vsd_cl::fields[] = {
   {&EDID_cl::BitF8Val, NULL, offsetof(vsd_t, tag), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
   CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(vsd_t, tag), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
   CEA_BlkHdr_dscF1 },
   {&EDID_cl::ByteStr, NULL, offsetof(vsd_t, ieee_id), 0, 3, EF_STR|EF_HEX|EF_LE|EF_RD, 0, 0xFFFFFF, "ieee_id",
   "IEEE Registration Id LE, 00-0C-03 for HDMI Licensing, LLC." },
   {&EDID_cl::ByteStr, NULL, offsetof(vsd_t, src_phy), 0, 2, EF_STR|EF_HEX|EF_LE|EF_RD, 0, 0xFFFF, "src phy",
   "Source Physical Address (section 8.7 of HDMI 1.3a)." },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, sink_feat), 1, 1, EF_BIT, 0, 1, "DVI_dual",
   "DVI Dual Link Operation support." },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, sink_feat), 1, 1, EF_BIT, 0, 1, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, sink_feat), 2, 1, EF_BIT, 0, 1, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, sink_feat), 3, 1, EF_BIT, 0, 1, "DC_Y444",
   "4:4:4 support in deep color modes." },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, sink_feat), 4, 1, EF_BIT, 0, 1, "DC_30bit",
   "10-bit-per-channel deep color support." },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, sink_feat), 5, 1, EF_BIT, 0, 1, "DC_36bit",
   "12-bit-per-channel deep color support." },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, sink_feat), 6, 1, EF_BIT, 0, 1, "DC_48bit",
   "16-bit-per-channel deep color support." },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, sink_feat), 7, 1, EF_BIT, 0, 1, "AI",
   "Supports_AI (needs info from ACP or ISRC packets)." },
   {&EDID_cl::VSD_MaxTMDS, NULL, offsetof(vsd_t, max_tmds), 0, 1, EF_INT|EF_MHZ, 0, 0xFF, "Max_TMDS",
   "Optional: If non-zero: Max_TMDS_Frequency / 5mhz." },
   {&EDID_cl::BitF8Val, NULL, offsetof(vsd_t, ltncy_hdr), 0, 6, EF_BFLD, 0, 0, "reserved",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, ltncy_hdr), 6, 1, EF_BIT, 0, 0, "i_latency",
   "If set, 4 interlaced latency fields are present, must be 0 if latency_f (bit 7) is 0" },
   {&EDID_cl::BitVal, NULL, offsetof(vsd_t, ltncy_hdr), 7, 1, EF_BIT, 0, 0, "latency_f",
   "If set, latency fields are present" },
   {&EDID_cl::VSD_ltncy, NULL, offsetof(vsd_t, vid_lat), 0, 1, EF_INT|EF_MLS, 0, 251, "Video Latency",
   "Optional: Video Latency value=1+ms/2 with a max of 251 meaning 500ms." },
   {&EDID_cl::VSD_ltncy, NULL, offsetof(vsd_t, aud_lat), 0, 1, EF_INT|EF_MLS, 0, 251, "Audio Latency",
   "Optional: Audio Latency value=1+ms/2 with a max of 251 meaning 500ms." },
   {&EDID_cl::VSD_ltncy, NULL, offsetof(vsd_t, vid_ilat), 0, 1, EF_INT|EF_MLS, 0, 251, "Video iLatency",
   "Optional: Interlaced Video Latency value=1+ms/2 with a max of 251 meaning 500ms." },
   {&EDID_cl::VSD_ltncy, NULL, offsetof(vsd_t, aud_ilat), 0, 1, EF_INT|EF_MLS, 0, 251, "Audio iLatency",
   "Optional: Interlaced Audio Latency value=1+ms/2 with a max of 251 meaning 500ms." }
};

rcode cea_vsd_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;
   u32_t  dlen;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <bhdr_t*> (inst)->tag.blk_len;
   if (dlen < 4) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "VSD");
   //3 bytes IEEE OUI + min 1 byte payload

   type_id   = ID_VSD;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//SAB: Speaker Allocation Data Block
//NOTE: the structure of SAB is basically a SPM: Speaker Presence Mask
//      The Desc and fields are shared with RMCD: Room Configuration Data Block: SPM

extern const char SPM_Desc[];
       const char SPM_Desc[] =
"Speaker Presence Mask (3 bytes).\n"
"Contains information about which speakers are present in the display device.\n"
"Byte1:\n"
"FL_FR\t: Front Left/Right\n"
"LFE1\t\t: LFE Low Frequency Effects 1 (subwoofer1)\n"
"FC\t\t: Front Center\n"
"BL_BR\t: Back Left/Right\n"
"BC\t\t: Back Center\n"
"FLC_FRC\t: Front Left/Right of Center\n"
"RLC_RRC\t: Rear Left/Right of Center\n"
"FLW_FRW\t: Front Left/Right Wide\n"
"Byte2:\n"
"TpFL_TpFR\t: Top Front Left/Right\n"
"TpC\t\t: Top Center\n"
"TpFC\t\t: Top Front Center\n"
"LS_RS\t: Left/Right Surround\n"
"LFE2\t\t: LFE Low Frequency Effects 2 (subwoofer2)\n"
"TpBC\t: Top Back Center\n"
"SiL_SiR\t: Side Left/Right\n"
"TpSiL_TpSiR: Top Side Left/Right\n"
"Byte3:\n"
"TpBL_TpBR: Top Back Left/Right\n"
"BtFC\t\t: Bottom Front Center\n"
"BtFL_BtFR\t: Bottom Front Left/Right\n"
"TpLS_TpRS: Top Left/Right ? Surround ? - a bug in CTA-861-G ?\n"
"bit4-7\t: reserved\n";

const char  cea_sab_cl::Name[] = "SAB: Speaker Allocation Block";
const u32_t cea_sab_cl::fcount = 26;

//NOTE: The SPM fields definitions are shared with RMCD: Room Configuration Data Block: SPM
extern const edi_field_t SAB_SPM_fields[];
       const edi_field_t SAB_SPM_fields[] = {
   //SAB header
   {&EDID_cl::BitF8Val, NULL, offsetof(sab_t, bhdr), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
    CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(sab_t, bhdr), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
    CEA_BlkHdr_dscF1 },
   //SAB byte 0
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm0), 0, 1, EF_BIT, 0, 1, "FL_FR",
   "Front Left/Right " },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm0), 1, 1, EF_BIT, 0, 1, "LFE1",
   "LFE Low Frequency Effects 1 (subwoofer1)" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm0), 2, 1, EF_BIT, 0, 1, "FC",
   "Front Center" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm0), 3, 1, EF_BIT, 0, 1, "BL_BR",
   "Back Left/Right" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm0), 4, 1, EF_BIT, 0, 1, "BC",
   "Back Center" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm0), 5, 1, EF_BIT, 0, 1, "FLC_FRC",
   "Front Left/Right of Center" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm0), 6, 1, EF_BIT, 0, 1, "RLC_RRC",
   "Rear (Back) Left/Right of Center" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm0), 7, 1, EF_BIT, 0, 1, "FLW_FRW",
   "Front Left/Right Wide" },
   //SAB byte 1
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 0, 1, EF_BIT, 0, 1, "TpFL_TpFR",
   "Top Front Left/Right" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 1, 1, EF_BIT, 0, 1, "TpC",
   "Top Center" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 2, 1, EF_BIT, 0, 1, "TpFC",
   "Top Front Center" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 3, 1, EF_BIT, 0, 1, "LS_RS",
   "Left/Right Surround" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 4, 1, EF_BIT, 0, 1, "LFE2",
   "LFE Low Frequency Effects 2 (subwoofer2)" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 5, 1, EF_BIT, 0, 1, "TpBC",
   "Top Back Center" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 6, 1, EF_BIT, 0, 1, "SiL_SiR",
   "Side Left/Right" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 7, 1, EF_BIT, 0, 1, "TpSiL_TpSiR",
   "Top Side Left/Right" },
   //SAB byte 2
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 0, 1, EF_BIT, 0, 1, "TpBL_TpBR",
   "Top Back Left/Right" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 1, 1, EF_BIT, 0, 1, "BtFC",
   "Bottom Front Center" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 2, 1, EF_BIT, 0, 1, "BtFL_BtFR",
   "Bottom Front Left/Right" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 3, 1, EF_BIT, 0, 1, "TpLS_TpRS",
   "Top Left/Right ? Surround ? - a bug in CTA-861-G ?" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 4, 1, EF_BIT|EF_RD, 0, 1, "resvd4",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 5, 1, EF_BIT|EF_RD, 0, 1, "resvd5",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 6, 1, EF_BIT|EF_RD, 0, 1, "resvd6",
   "reserved (0)" },
   {&EDID_cl::BitVal, NULL, offsetof(sab_t, spm1), 7, 1, EF_BIT|EF_RD, 0, 1, "resvd7",
   "reserved (0)" },
};

rcode cea_sab_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;
   u32_t  dlen;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <bhdr_t*> (inst)->tag.blk_len;
   if ( dlen < 3)       wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "SAB");
   if ((dlen % 3) != 0) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "SAB");

   fields    = SAB_SPM_fields;
   Desc      = SPM_Desc;

   type_id   = ID_VSD;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//VDTC: VESA Display Transfer Characteristic Data Block (gamma)
const char  cea_vdtc_cl::Name[] = "VDTC: VESA Display Transfer Characteristic";
const char  cea_vdtc_cl::Desc[] =
"Gamma value for the display device.\n"
"The format is exactly the same as in EDID block 0, byte 23";

const u32_t       cea_vdtc_cl::fcount = 3;
const edi_field_t cea_vdtc_cl::fields[] = {
   //DBC header
   {&EDID_cl::BitF8Val, NULL, offsetof(sab_t, bhdr), 0, 5, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x1F, "blk_len",
    CEA_BlkHdr_dscF0 },
   {&EDID_cl::BitF8Val, NULL, offsetof(sab_t, bhdr), 5, 3, EF_BFLD|EF_INT|EF_RD|EF_FGR, 0, 0x07, "tag_code",
    CEA_BlkHdr_dscF1 },
   {&EDID_cl::Gamma, NULL, offsetof(vtc_t, gamma), 0, 1, EF_FLT|EF_NI, 0, 255, "gamma",
   "Byte value = (gamma*100)-100 (range 1.00–3.54)" }
};

rcode cea_vdtc_cl::init(u8_t* inst, u32_t orflags) {
   rcode  retU;
   u32_t  dlen;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   dlen = reinterpret_cast <bhdr_t*> (inst)->tag.blk_len;
   if ( dlen != 1) wxedid_RCD_RETURN_FAULT_VMSG(retU, "[E] %s: bad block length", "VDTC");

   type_id   = ID_VDTC;

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

//UNK-TC: Unknown Data Block (Tag Code)
const char  cea_unktc_cl::Name[] = "UNK-TC: Unknown Data Block";
const char  cea_unktc_cl::Desc[] = "Unknown Tag Code";

rcode cea_unktc_cl::init(u8_t* inst, u32_t orflags) {
   rcode        retU;
   u32_t        g_len;
   edi_field_t *p_fld;

   if (inst == NULL) RCD_RETURN_FAULT(retU);

   //unknown Tag Code
   type_id = ID_CEA_UTC;

   //block length
   g_len   = reinterpret_cast <bhdr_t*> (inst)->tag.blk_len;

   fcount  = CEA_BlkHdr_fcount;
   fcount -= 1; //no extended tag

   //pre-alloc buffer for array of fields: hdr_fcnt + g_len
   fields = (edi_field_t*) malloc( (g_len + fcount) * edi_fld_sz );
   if (NULL == fields) RCD_RETURN_FAULT(retU);

   p_fld   = fields;

   memcpy( (void*) p_fld, CEA_BlkHdr_fields, (fcount * edi_fld_sz) );
   p_fld  += fcount;

   fcount += g_len;

   //payload data interpreted as unknown
   insert_unk_byte(p_fld, g_len, sizeof(bhdr_t) );

   retU = init_fields(&fields[0], inst, fcount, orflags, Name, Desc);
   return retU;
}

