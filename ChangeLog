#Change log for wxEDID

NOTE#1: configure script automatically uses wxWidgets v3.1.x+, if found.
NOTE#2: wxAUI in wxWidgets 3.1.x built against GTK3 is broken, but the application is usable.

TODO: add "invalid/unknown" block/field flag, display invalid data on red background.

TODO: cea_sad_cl::byte2_afc2_8: add function for handling values of bitrate in 8kHz units

TODO: HDR Dynamic Metadata parser in hdrd_mtd_cl::gen_data_layout().

TODO: Constructors for changing block type: in particular, some DBC blocks requires to change more
      than one value at once to get a valid block type of different kind.


____ BUGS:
BUG: (CTA-861-D..G specifications) CEA-DBC:
     Extended Tag Code=3: VESA Video Timing Block Extension (VTB-EXT):
     CTA-861-F/G references VTB-EXT Standard, Release A, November 24, 2003.
     The VTB-EXT extension block is defined as a 128 byte structure, what exceeds the range allowed
     by the DBC Extended Tag header (30 bytes).
     None of the documents is addressing this issue.
     wxEDID will report error for Extended Tag Code=3.
____


2020.07.12
   < released v0.0.21 >

2020.07.10
   - Fixed: (BUG): BlockTree: root node was not showing EDID file name if the file path was provided
            as an cmd line argument (both binary and --text).

2020.06.12
   - Fixed: (BUG) cea_hdrs_cl::init(): Incorrect min size for "HDR Static Metadata Data Block"
            (TagCode=7, ExtTagCode=6): the min. block size is 3, not 6 bytes.
            Fields for bytes 4..6 are marked as unused, depending on the block size.
   - Fixed: (BUG) cea_hdrs_cl: incorrect bitfield definitions for reserved fields: "ET6-7" & "SM1-7"
   - Fixed: Wrong evt ID for importing HEX-text EDID from cmd line arg: should be id_mnu_imphex, not id_mnu_exphex.

2020.06.05
   - Fixed: (Regression) if config file doen't exist, the config.b_dtd_keep_aspect is set to OFF,
            but by default it should be set to ON.

2020.06.04
   < released v0.0.20 >
   - Added: Loading/Importing EDID files from path passed as cmd line argument.
   - Added: Save settings: last used directory + last opened file, dtd_keep_aspect_ratio

2020.06.03
   - Fixed: (BUG) cea_y42c_cl::gen_data_layout() never tested: buggy code, memory corruption.
            Program crash if EDID data contains "YCBCR 4:2:0 Capability Map Data Block"
            (TagCode=7, ExtTagCode=15)
   - Change: lower-case name for the final executable: wxEDID -> wxedid.

2020.01.02
   < released v0.0.19 >
   - Fixed: (BUG::old) evt_blktree_sel(): the cell editor must be closed before switching to newly selected
            data block. Otherwise the cell editor can be "orphaned" and its window stays opened until
            another cell editor is activated. This problem was visible only when the cell editor was activated
            from Value Selector Menu event and if evt_blktree_sel() was invoked with opened cell editor.
   - Fixed: (BUG::regression) EDID_cl::ProdWk(): wxString::operator<<() requires type casting to <int>
            when appending a byte -> otherwise <u8_t> is interpreted as <char>
   - Fixed: (BUG::regression) menu Options::"Ignore EDID errors" must be always enabled, because
            otherwise the option will be unavailable until a valid EDID structure is loaded.
   - Added: Menu Help::Flags & Types: info about flags displayed in the block data grid.

2019.12.30
   - Update: rcode v1.0.3, added support for rcd_autogen v1.3.
            Updated build scripts and codeblocks project.
   - Fixed: (BUG) SLDB: Speaker Location Data Block must handle multiple Speaker Location Descriptors (SLOCD)
                  instances - but only 1st SLOCD was processed.
   - Added: CEA-DBC: basic checks of data block lengths.

2019.12.29
   - Fixed: Menu Options::"Ignore EDID Errors": now most of the errors can be ignored, including
            errors in CEA/CTA-861 extension block
   - Added: (CEA) UNK-ET: Unknown Data Block (Extended Tag Code)
   - Added: (CEA) UNK-TC: Unknown Data Block (Tag Code)
   - Added: flags: EF_BYTE, EF_DM (decimeters unit for RMCD: Room Configuration Data Block).
            Updated all field descriptors.
   - Change: All EDID data classes: .Name and .Desc fields: changed type from wxString to char[].

2019.12.28
   - Added: IFDB: InfoFrame Data Block

2019.12.23
   - Fixed: removed unused function EDID_cl::del_groups()

2019.12.22
   - Added: CEA-SLDB: Speaker Location Data Block
   - Added: CEA-RMCD: Room Configuration Data Block
   - Added: CEA-Y42C: YCBCR 4:2:0 Capability Map Data Block
   - Added: CEA-Y42V: YCBCR 4:2:0 Video Data Block
   - Added: CEA-VFPD: Video Format Preference Data Block

2019.12.21
   - Added: CEA-HDRD: HDR Dynamic Metadata Data Block
   - Added: CEA-HDRS: HDR Static Metadata Data Block
   - Added: CEA-CLDB: Colorimetry Data Block

2019.12.20
   - Change: CEA-VDDD: VESA Display Device Data Block: split into 5 subgroups.

2019.12.19
   - Added: CEA-VDDD: VESA Display Device Data Block (VESA DDDB Standard, Version 1; September 25, 2006).

2019.12.18
   - Added: CEA-VSAD: Vendor-Specific Audio Data Block.
   - Added: CEA-VSVD: Vendor-Specific Video Data Block.
   - Added: CEA-VCDB: Video Capability Data Block.
   - Added: Support for CEA-DBC Extended Tag Codes.
   - Added: CEA-VDTC: VESA Display Transfer Characteristic data block (gamma).
            The data format is the same as in EDID block 0, byte 23.

2019.12.17
   - Fixed: VID: analog input: vsync field (bit) description. (should be analog, not digital).
   - Added: VID: variable data layout and a value selector menu for analog/digital input type.
   - Added: SVD: Video ID Codes (VICs) 64..219 added to the value selector menu.
   - Added: SVD: support for 8-bit VIC mode (no native bit).
   - Added: SAB: updated/added speaker presence bit definitions for (formerly reserved) bytes 2 & 3 (offset 1 & 2).

2019.12.16
   - Fixed: (BUG::regression::unknown_date) value selector menu is not triggering EVT_GRID_CELL_CHANGED.
   - Fixed: EDID_cl::BitVal(), BitF8Val(), ByteVal(): strict checking of write opcode value (return error if incorrect)
   - Added: cea_sad_cl::gen_data_layout(): support for variable data layouts in ADB::SAD.
            The Block Data grid is automatically updated when a value with EF_FGR flag set is changed
            (forced group refresh).
   - Change: Added def_types.h: short definitions of common int types.

2019.12.14
   - Added: SAD: variable data layout: full support for CEA/CTA-861-G
   - Fixed: #include <wx/arrimpl.cpp> before each WX_DEFINE_OBJARRAY.
            Not really needed, but recommended in wxWidgets docs.

2019.07.23
   - Fixed: DTD_Constructor: txc_vtotal: (gtk2) BG color should be greyed, not white (not editable).
   - Fixed: dtd_screen_cl: removed unused wxRect rcDC, introduced in v0.0.18

2019.07.22
   < released v0.0.18 >
   - Fixed: gcc v8.x:
            wxEvtHandler::Connect()/Disconnect(): Warning of type mismatch for event handler functions casted using
            "wxObjectEventFunction" {aka ‘void (wxEvtHandler::*)(wxEvent&)’}, due to argument type mismatch.
            Warnings silenced by disabling GCC diagnostic "-Wcast-function-type".
   - Fixed: Removed unused "Hex View" menu item.
   - Added: Menu Options: "DTD preview: keep aspect ratio": keep aspect ratio of the DTD preview screen.
   - Added: DTD_Constructor: active screen area size is now displayed on the DTD screen preview.
            H/V borders are excluded from active area.
   - Fixed: (BUG) configure script: symbols stripping in release build didn't work.

2019.07.21
   - Cleanup: All the constructs {RCD_SET_FAULT(); return <rcode>;} replaced with RCD_RETURN_FAULT().
   - Cleanup: All the constructs {RCD_SET_OK(); return <rcode>;} replaced with RCD_RETURN_OK().
   - Cleanup: dtd_screen_cl::calc_coords use single precision float type for calculations.

2019.07.20
   - Fixed: fixed few help texts for Info Panel: VSD, SAB, SAD.num_ch,

2018.12.22
   < released v0.0.17 >
   - Fixed: (BUG::wxSmith) Menu "Quit" and "About" handlers were connected dynamically in the frame
            constructor, even though they were already present in the static event table.
   - Change: Increased default Log Window size from 400x300 to 500x400.

2018.12.20
   - Fixed: Options menu: all menu items except "Log window" should stay disabled until edid data
            is loaded/imported.
   - Change: UpdateEDItree() renamed to UpdateEDID_tree()
   - Change: All panels & controls are now using default system font instead of fixed one.

2018.12.19
   - Update: new versions of event table macros:
            wxDECLARE_EVENT_TABLE, wxBEGIN_EVENT_TABLE, wxEND_EVENT_TABLE.

2018.12.01
   < released v0.0.16 >
   - Change: Info about packages required to compile the project moved to file INSTALL,
            README file removed.
   - Change: Code::Blocks project files are now included in the dist package.
   - Fixed: DTD panel: dtd_screen: background was erased twice on resize event-> now the refreshing
            is performed in a single call to the paint event.
2018.11.30
   - Change: DTD panel: all the controls are now configured to use default min sizes - this is rather
             a disadventage, because f.e. the default controls in gtk3 are ridiculously big.
             However, the minimal App frame size is now re-calculated dynamically, basing on the
             min. size reported by the DTD sizer - so this is a more flexible solution.

2018.11.29
   - Added: support for wxWidgets v3.1.x : *experimental*
   - Change: Dropped support for old wxWidgets versions (<3.0.0).
   - Change: Changed proportins of AUI panes.
   - Fixed: wxWidgets v3.1.x wxGrid::SetCellValue(wxString&, int, int) is deprecated, new
            version is wxGrid::SetCellValue(int, int, wxString&)
   - Fixed: wxWidgets v3.1.x: wxEVT_GRID_CELL_CHANGE is no longer supported -> renamed to
            wxEVT_GRID_CELL_CHANGED
   - Fixed: wxEDID_Frame::evt_frame_size() missing call to sizer->Layout() -> quick resizing
            of the frame could cause incorrect placement of child windows on the DTD panel.
   - Update: returncode.h v0.8.11.

2018.11.28
   - Fixed: (BUG::old): DTD_Ctor_WriteInt(): value change event log: missing field names.
            only the first letter of field name was printed due to missing conversion from
            ASCII to wxString.
   - Fixed: (BUG::old): EDID_class.cpp: wxString AltDesc wasn't really initializing the alternative
            desriptors' "Desc" fields. Now the AltDesc is just a char string, and the "Desc" fields
            are properly initialized by calling wxString::FromUTF8(AltDesc).
   - Fixed: DTD_Ctor_WriteInt(), WriteField(): value change event log: print the whole message string
            in a single call to guilog::DoLog() - eliminates printing of multiple timestamps per event.
   - Fixed: Corrected few textual descriptions of EDID fields.
   - Cleanup: removed line continuation marks, as they are not really needed for any modern compiler.

2018.01.07
   - Update: returncode.h v0.8.9.

2017.11.27
   < released v0.0.15 >
   - Fixed: (BUG) RCD_RETURN_FALSE() returns RCD_TRUE! (unknown origin of regression!)
            No influence on the behaviour of the program code - just an update.

2017.10.30
   < released v0.0.14 >
   - Update: guilog.h v0.2
   - Fixed: Info panel: BG & FG colors were theme-dependant, what could make the panel look
            "ugly" or even completely unreadable. Now the FG is forced to black and the BG is
            white.
   - Added: README: info about packages required to compile the project (Debian-based distros)

2017.09.18
   - Update: returncode.h v0.8.6

2017.09.10
   - Fixed: Info string: incorrect value for VBOX descriptor type: should be 0x10, not 0x0A.

2017.09.08
   - Update: returncode.h v0.8.5

2017.09.05
   < released v0.0.13 >
   - Fixed: (GCC 6.x): silenced some warnings (false-positives) regarding "possibly
            unitialized variables" in EDID_class.cpp
   - Fixed: (GCC 6.x, C11 mode): wxEDID_Main.h: C11 requires a space between literal
            and string macro [-Wliteral-suffix].
   - Update: returncode.h v0.8.3

2016.06.17
   < released v0.0.12 >
   - Change: Entire wxEDID GUI switched to wxAUI.
   - Change: all menu events are now statically compiled-in.
   - Update: (wxWidgets 3.x) new version of wxwin.m4: wxAUI needs updated macros
             for linking.

2016.06.11
   < released v0.0.11 >
   - TEST:  compilation against wxWidgets 3.x: works (wxWidgets 3.0.2)
   - Update: returncode.h v0.8.0
   - Fixed: Constant font size for all windows.
   - Fixed: (BUG): uninitialized rcode returned in DTD_Ctor_read_all() in case when
            DTD_Ctor_read_field() fails.
   - Fixed: (wxWidgets 3.x): assertion failure: wxFlexGridSizer fgs_dtd_bottom:
            incorrect number of rows.

2015.09.09
   < released v0.0.10 >
   - Update: ancient returncode.h (v0.3.5) updated to v0.6.9.
   - Fixed: v0.0.9 version was still displaying v0.0.8 in GUI::About
   - Fixed: added -fno-exceptions to default build flags: the project is not using
            exceptions: waste of resources.
   - Fixed: build script was improperly setting the build flags.

2015.05.07
   < released v0.0.9 >
   - Fixed: (BUG) (thanks to *gilles-b*):
            EDID_class.cpp:EDID_cl::MfcId -> clearing of reserved bit after swapping
            mfc_id damages the mfc id string.

2014.06.12
   < released v0.0.8 >
   - Fixed: cea_adb_cl::init() all SAD instances are referencing data from SAD instance 0:
            missing data pointer incrementation.
   - Fixed: DTD_Ctor_Recalc() returns uninitialized rcode.
   - Fixed: Reparse(): force number of valid edid blocks if "Ignore EDID Errors" is enabled.
            This allows to export faulty blocks, which are otherwise dropped.

2014.06.11
   < released v0.0.7 >
   - Added: CEA-861 support (as first extension block)
   - Added: Value selector menu: some CEA-861 fields contains indexes, not actual values.
   - Fixed: import/export/load/save functions are now aware of extensions.
   - Added: Menu Options: Ignore EDID Errors -> allows to ignore minor EDID erros and to view the
            data anyway.
   - Added: Menu Options: Recalc Checksum
   - Fixed: DTD "virtual" screen is resized on Frame size change.
   - Cleanup: some field handlers replaced with generic funcions.

2014.04.23
   < released v0.0.6:
   - Added: X11 ModeLine viewer on DTD constructor panel (quick patch)

2014.04.19
   < released v0.0.5 >
   - Added: DTD constructor panel.
   - Change: field handlers can now use uint var as alternative i/o value.
   - Fixed: Disable save/export/save_as menu items until EDID buffer is loaded.
   - Fixed: ParseGroups() didn't checked group init() rcode.
   - Fixed: Bad conversion of mfc_id to PNP_ID (on write, misuse of temporary pointer)
   - Fixed: EDID checksum field handler replaced with generic ByteVal(), which is more type-safe
   - Fixed: EDID prod_id field handler: require "0x" prefix for EF_HEX type field.
   - Fixed: descriptions and order of some fields: some descrptions were still buggy and fields
            order was not necesarily readable.
   - Cleanup: removed unused fn rdUByte() and wrUByte()

2014.04.09
   < released v0.0.4 >
   - Fixed: memory leak in edi_grp_cl - missing destructor, edi_dynfld_t fields
            were not deleted on exit/reparse;
   - Fixed: some fields descriptions were inaccurate or buggy.
   - Added: AST: Additional Standard Timings Descriptor support (it was missing by mistake)

2014.04.08
   - Initial release v0.0.3. Code still needs cleanups and
     there are many features left to implement, but the base
     editor is tested and working.

2014.03.18
   - Project started: GUI code & layout, EDID definitions
