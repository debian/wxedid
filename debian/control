Source: wxedid
Section: utils
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libwxgtk3.2-dev,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/wxedid.git -b debian/master
Vcs-Browser: https://salsa.debian.org/debian/wxedid
Homepage: https://sourceforge.net/projects/wxedid/

Package: wxedid
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Description: Graphical editor for monitor resolution and timing data (EDID)
 wxEDID is a wxWidgets based editor for Extended Display Identification
 Data (EDID).
 .
 It operates on offline files that can be flashed to a monitor using the
 technique described here: https://wiki.debian.org/RepairEDID.
 .
 This tool can modify the base EDID v1.3+ structure and the CEA/CTA-861-G,
 as the first extension block. The program also provides a DTD constructor
 to aid with the selection of suitable timings.
 .
 The tool can import and export EDID data in binary blob and in hexadecimal
 formats.
